﻿Public Class clsGetUserInfo
    Private lUserID As Long
    Private sFName As String
    Private sLName As String

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Property FName As String
        Get
            FName = sFName
        End Get
        Set(value As String)
            sFName = value
        End Set
    End Property

    Public Property LName As String
        Get
            LName = sLName
        End Get
        Set(value As String)
            sLName = value
        End Set
    End Property

    Public Sub GetUserInfo()
        Dim SQL As String
        Dim clU As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & lUserID
        clU.OpenDB(SQL, sCON)
        If clU.RowCount > 0 Then
            clU.GetRow()
            sFName = clU.Fields("fname")
            sLName = clU.Fields("lname")
        End If
    End Sub
End Class
