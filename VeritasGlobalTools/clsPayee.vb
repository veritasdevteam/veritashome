﻿
Public Class clsPayee
    Private lContractID As Long
    Private lPayeeID As Long
    Private SQL As String
    Private clC As New clsDBO
    Private clD As New clsDBO
    Private clP As New clsDBO
    Private sCompanyName As String
    Private sFName As String
    Private sLName As String
    Private sAddr1 As String
    Private sAddr2 As String
    Private sCity As String
    Private sState As String
    Private sZip As String
    Private sPhone As String

    Public Property Addr1 As String
        Get
            Addr1 = sAddr1
        End Get
        Set(value As String)
            sAddr1 = value
        End Set
    End Property

    Public Property Addr2 As String
        Get
            Addr2 = sAddr2
        End Get
        Set(value As String)
            sAddr2 = value
        End Set
    End Property

    Public Property City As String
        Get
            City = sCity
        End Get
        Set(value As String)
            sCity = value
        End Set
    End Property

    Public Property CompanyName As String
        Get
            CompanyName = sCompanyName
        End Get
        Set(value As String)
            sCompanyName = value
        End Set
    End Property

    Public Property ContractID As Long
        Get
            ContractID = lContractID
        End Get
        Set(value As Long)
            lContractID = value
        End Set
    End Property

    Public Property FName As String
        Get
            FName = sFName
        End Get
        Set(value As String)
            sFName = value
        End Set
    End Property

    Public Property LName As String
        Get
            LName = sLName
        End Get
        Set(value As String)
            sLName = value
        End Set
    End Property

    Public Property PayeeID As Long
        Get
            PayeeID = lPayeeID
        End Get
        Set(value As Long)
            lPayeeID = value
        End Set
    End Property

    Public Property Phone As String
        Get
            Phone = sPhone
        End Get
        Set(value As String)
            sPhone = value
        End Set
    End Property

    Public Property State As String
        Get
            State = sState
        End Get
        Set(value As String)
            sState = value
        End Set
    End Property

    Public Property Zip As String
        Get
            Zip = sZip
        End Get
        Set(value As String)
            sZip = value
        End Set
    End Property

    Public Sub AddCustomerToPayee()
        OpenContract()
        If clC.RowCount > 0 Then
            SQL = "select * from payee "
            SQL = SQL + "where contractid = " & clC.Fields("contractid")
            clP.OpenDB(SQL, sCON)
            If clP.RowCount = 0 Then
                clP.NewRow()
            Else
                clP.GetRow()
            End If
            clP.Fields("payeetype") = "Customer"
            clP.Fields("companyname") = ""
            clP.Fields("fname") = clC.Fields("fname")
            clP.Fields("lname") = clC.Fields("lname")
            clP.Fields("addr1") = clC.Fields("addr1")
            clP.Fields("addr2") = clC.Fields("addr2")
            clP.Fields("city") = clC.Fields("city")
            clP.Fields("state") = clC.Fields("state")
            clP.Fields("zip") = clC.Fields("zip")
            clP.Fields("phone") = clC.Fields("phone")
            clP.Fields("contractid") = clC.Fields("contractid")
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
            SQL = "select * from payee "
            SQL = SQL + "where contractid = " & clC.Fields("contractid")
            clP.OpenDB(SQL, sCON)
            If clP.RowCount > 0 Then
                clP.GetRow()
                lPayeeID = clP.Fields("payeeid")
            End If
        End If
    End Sub

    Public Sub AddDealerToPayee()
        OpenContract()
        If clC.RowCount > 0 Then
            OpenDealer()
            If clD.RowCount > 0 Then
                SQL = "select * from payee "
                SQL = SQL + "where dealerid = " & clC.Fields("dealerid")
                clP.OpenDB(SQL, sCON)
                If clP.RowCount = 0 Then
                    clP.NewRow()
                Else
                    clP.GetRow()
                End If
                clP.Fields("payeetype") = "Dealer"
                clP.Fields("companyname") = clD.Fields("dealername")
                clP.Fields("addr1") = clD.Fields("addr1")
                clP.Fields("addr2") = clD.Fields("addr2")
                clP.Fields("city") = clD.Fields("city")
                clP.Fields("state") = clD.Fields("state")
                clP.Fields("zip") = clD.Fields("zip")
                clP.Fields("phone") = clD.Fields("phone")
                clP.Fields("dealerid") = clD.Fields("dealerid")
                If clP.RowCount = 0 Then
                    clP.AddRow()
                End If
                clP.SaveDB()
                SQL = "select * from payee "
                SQL = SQL + "where dealerid = " & clD.Fields("dealerid")
                clP.OpenDB(SQL, sCON)
                If clP.RowCount > 0 Then
                    clP.GetRow()
                    lPayeeID = clP.Fields("payeeid")
                End If
            End If
        End If
    End Sub

    Public Sub AddOtherToPayee()
        SQL = "select * from payee "
        SQL = SQL + "where payeetype = 'Other' "
        If sCompanyName.Length > 0 Then
            SQL = SQL + "and companyname = '" & sCompanyName & "' "
            GoTo MoveHere
        End If
        If sFName.Length > 0 And sLName.Length > 0 Then
            SQL = SQL + "and fname = '" & sFName & "' "
            SQL = SQL + "and lname = '" & sLName & "' "
            GoTo MoveHere
        End If
        Exit Sub
MoveHere:
        clP.OpenDB(SQL, sCON)
        If clP.RowCount > 0 Then
            clP.GetRow()
        Else
            clP.NewRow()
        End If
        clP.Fields("payeetype") = "Other"
        clP.Fields("companyname") = sCompanyName
        clP.Fields("fname") = sFName
        clP.Fields("lname") = sLName
        clP.Fields("addr1") = sAddr1
        clP.Fields("addr2") = sAddr2
        clP.Fields("city") = sCity
        clP.Fields("state") = sState
        clP.Fields("zip") = sZip
        clP.Fields("phone") = sPhone
        If clP.RowCount = 0 Then
            clP.AddRow()
        End If
        clP.SaveDB()
        SQL = "select * from payee "
        SQL = SQL + "where payeetype = 'Other' "
        If sCompanyName.Length > 0 Then
            SQL = SQL + "and companyname = '" & sCompanyName & "' "
        End If
        If sFName.Length > 0 And sLName.Length > 0 Then
            SQL = SQL + "and fname = '" & sFName & "' "
            SQL = SQL + "and lname = '" & sLName & "' "
        End If
        clP.OpenDB(SQL, sCON)
        If clP.RowCount > 0 Then
            clP.GetRow()
            lPayeeID = clP.Fields("payeeid")
        End If

    End Sub

    Public Sub OpenPayeeInfo()
        SQL = "select * from payee "
        SQL = SQL + "where payeeid = " & lPayeeID
        clP.OpenDB(SQL, sCON)
        If clP.RowCount > 0 Then
            clP.GetRow()
            sCompanyName = clP.Fields("companyname")
            sFName = clP.Fields("fname")
            sLName = clP.Fields("lname")
            sAddr1 = clP.Fields("addr1")
            sAddr2 = clP.Fields("addr2")
            sCity = clP.Fields("city")
            sState = clP.Fields("state")
            sZip = clP.Fields("zip")
            sPhone = clP.Fields("phone")
        End If
    End Sub

    Private Sub OpenContract()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
        End If
    End Sub

    Private Sub OpenDealer()
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & clC.Fields("dealerid")
        clD.OpenDB(SQL, sCON)
        If clD.RowCount > 0 Then
            clD.GetRow()
        End If
    End Sub
End Class
