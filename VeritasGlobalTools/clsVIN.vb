﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Net
Imports System.Web
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Public Class clsVIN
	Private sVIN As String
	Private sContract As String
	Public responseText As String
	Private lVINID As Long
	Private ds As New DataSet
	Private SQL As String

	Public Property VIN As String
		Get
			VIN = sVIN
		End Get
		Set(value As String)
			sVIN = value
		End Set
	End Property

	Public Property Contract As String
		Get
			Contract = sContract
		End Get
		Set(value As String)
			sContract = value
		End Set
	End Property

	Public Sub ProcessVIN()
		Try
			Dim xmlD As New Xml.XmlDocument
			Dim xmlNR As Xml.XmlNodeReader
			If CheckVIN() Then
				Exit Sub
			End If
			xmlD.LoadXml(Decode)
			xmlNR = New Xml.XmlNodeReader(xmlD)
			ds.ReadXml(xmlNR)

			If ds.Tables(3).Rows(0).Item(0).ToString.Length > 0 Then
				Exit Sub
			End If


			CheckBasicData()
			CheckEngine()
			CheckTransmission()
			CheckWarranty()
		Catch ex As Exception

		End Try
	End Sub

	Public Sub CheckWarranty()
		Dim clW As New clsDBO
		Dim dt As DataTable
		Dim dr As DataRow
		Try
			dt = ds.Tables(13)
			For cnt = 0 To dt.Rows.Count - 1
				dr = dt.Rows(cnt)
				SQL = "select * from vin.dbo.warranty "
				SQL = SQL + "where vinid = " & lVINID & " "
				SQL = SQL + "and WarrantyType = '" & dr("type") & "' "
				clW.OpenDB(SQL, sCON)
				If clW.RowCount > 0 Then
					GoTo MoveNext
				Else
					clW.NewRow()
				End If
				clW.Fields("vinid") = lVINID
				clW.Fields("WarrantyType") = dr("type")
				clW.Fields("Months") = dr("months")
				clW.Fields("Miles") = dr("miles")
				clW.Fields("Name") = dr("name")
				If clW.RowCount = 0 Then
					clW.AddRow()
				End If
				clW.SaveDB()
MoveNext:
			Next
		Catch ex As Exception

		End Try
	End Sub

	Public Sub CheckTransmission()
		Dim clT As New clsDBO
		Dim dt As DataTable
		Dim dr As DataRow
		Try
			dt = ds.Tables(11)
			dr = dt.Rows(0)
			SQL = "select * from vin.dbo.transmission "
			SQL = SQL + "where vinid = " & lVINID
			clT.OpenDB(SQL, sCON)
			If clT.RowCount > 0 Then
				clT.GetRow()
			Else
				clT.NewRow()
			End If
			clT.Fields("vinid") = lVINID
			clT.Fields("Availability") = dr("availability")
			clT.Fields("TransmissionType") = dr("type")
			clT.Fields("DetailType") = dr("detail_type")
			clT.Fields("Gears") = dr("gears")
			clT.Fields("OrderCode") = dr("order_code")
			If dr("fleet").ToString.ToLower = "y" Then
				clT.Fields("fleet") = True
			End If
			clT.Fields("Name") = dr("name")
			clT.Fields("Brand") = dr("brand")
			clT.Fields("MarketingName") = dr("marketing_name")
			If clT.RowCount = 0 Then
				clT.AddRow()
			End If
			clT.SaveDB()

		Catch ex As Exception

		End Try
	End Sub

	Public Sub CheckEngine()
		Dim clE As New clsDBO
		Dim dt As DataTable
		Dim dr As DataRow
		Try
			dt = ds.Tables(9)
			dr = dt.Rows(0)
			SQL = "select * from vin.dbo.engine "
			SQL = SQL + "where vinid = " & lVINID
			clE.OpenDB(SQL, sCON)
			If clE.RowCount > 0 Then
				clE.GetRow()
			Else
				clE.NewRow()
			End If
			clE.Fields("vinid") = lVINID
			clE.Fields("availability") = dr("availability")
			clE.Fields("aspiration") = dr("ice_aspiration")
			clE.Fields("blocktype") = dr("ice_block_type")
			clE.Fields("bore") = dr("ice_bore")
			clE.Fields("CamType") = dr("ice_cam_type")
			clE.Fields("Compression") = dr("ice_compression")
			clE.Fields("Cyclinders") = dr("ice_cylinders")
			clE.Fields("Displacement") = dr("ice_displacement")
			clE.Fields("ElectricMotorConfig") = dr("electric_motor_configuration")
			clE.Fields("ElectricMaxHP") = dr("electric_max_hp")
			clE.Fields("ElectricMaxKW") = dr("electric_max_kw")
			clE.Fields("ElectricMaxTorque") = dr("electric_max_torque")
			clE.Fields("EngineType") = dr("engine_type")
			clE.Fields("FuelInduction") = dr("ice_fuel_induction")
			clE.Fields("FuelQuality") = dr("fuel_quality")
			clE.Fields("FuelType") = dr("fuel_type")
			If dr("fleet").ToString.ToLower = "y" Then
				clE.Fields("fleet") = True
			End If
			clE.Fields("GeneratorDesc") = dr("generator_description")
			clE.Fields("GeneratorMaxHP") = dr("generator_max_hp")
			clE.Fields("MaxHP") = dr("ice_max_hp")
			clE.Fields("MaxHPAtRPM") = dr("ice_max_hp_at")
			clE.Fields("MaxPayload") = dr("max_payload")
			clE.Fields("MaxTorque") = dr("ice_max_torque")
			clE.Fields("MaxTorqueAtRPM") = dr("ice_max_torque_at")
			clE.Fields("OilCapacity") = dr("oil_capacity")
			clE.Fields("OrderCode") = dr("order_code")
			clE.Fields("RedLine") = dr("redline")
			clE.Fields("Stroke") = dr("ice_stroke")
			clE.Fields("TotalMaxHP") = dr("total_max_hp")
			clE.Fields("TotalMaxHPAtRPM") = dr("total_max_hp_at")
			clE.Fields("TotalMaxTorque") = dr("total_max_torque")
			clE.Fields("TotalMaxTorqueAtRPM") = dr("total_max_torque_at")
			clE.Fields("ValveTiming") = dr("ice_valve_timing")
			clE.Fields("Valves") = dr("ice_valves")
			clE.Fields("Name") = dr("name")
			clE.Fields("Brand") = dr("brand")
			clE.Fields("MarketingName") = dr("marketing_name")
			If clE.RowCount = 0 Then
				clE.AddRow()
			End If
			clE.SaveDB()

		Catch ex As Exception

		End Try
	End Sub

	Public Sub CheckBasicData()
		Dim clBD As New clsDBO
		Dim dt As DataTable
		Dim dr As DataRow
		Try
			SQL = "select * from vin.dbo.basicdata "
			SQL = SQL + "where vinid = " & lVINID

			clBD.OpenDB(SQL, sCON)
			If clBD.RowCount > 0 Then
				clBD.GetRow()
			Else
				clBD.NewRow()
			End If
			clBD.Fields("vinid") = lVINID
			dt = ds.Tables(7)
			dr = dt.Rows(0)
			clBD.Fields("year") = dr("year")
			clBD.Fields("make") = dr("make")
			clBD.Fields("model") = dr("model")
			clBD.Fields("trim") = dr("trim")
			clBD.Fields("vehicletype") = dr("vehicle_type")
			clBD.Fields("bodytype") = dr("body_type")
			clBD.Fields("bodysubtype") = dr("body_subtype")
			clBD.Fields("oembodystyle") = dr("oem_body_style")
			clBD.Fields("door") = dr("doors")
			clBD.Fields("oemdoor") = dr("oem_doors")
			clBD.Fields("modelnumber") = dr("model_number")
			clBD.Fields("packagecode") = dr("package_code")
			clBD.Fields("packagesummary") = dr("package_summary")
			clBD.Fields("rearaxle") = dr("rear_axle")
			clBD.Fields("drivetype") = dr("drive_type")
			clBD.Fields("brakesystem") = dr("brake_system")
			clBD.Fields("restrainttype") = dr("restraint_type")
			clBD.Fields("countrymfr") = dr(18)
			clBD.Fields("plant") = dr("plant")
			If dr("glider").ToString.ToLower = "y" Then
				clBD.Fields("glider") = True
			End If
			clBD.Fields("chassistype") = dr("chassis_type")
			If clBD.RowCount = 0 Then
				clBD.AddRow()
			End If
			clBD.SaveDB()

		Catch ex As Exception

		End Try
	End Sub



	Private Function CheckVIN() As Boolean
		Dim clV As New clsDBO
		CheckVIN = True
		Try
			SQL = "select * from vin.dbo.vin "
			SQL = SQL + "where vin = '" & sVIN.Substring(0, 11) & "' "
			clV.OpenDB(SQL, sCON)
			If clV.RowCount > 0 Then
				Exit Function
			End If
			clV.NewRow()
			clV.Fields("vin") = sVIN.Substring(0, 11)
			clV.Fields("createdate") = Now
			clV.AddRow()
			clV.SaveDB()
			clV.OpenDB(SQL, sCON)
			If clV.RowCount > 0 Then
				clV.GetRow()
				lVINID = clV.Fields("vinid")
			End If
			CheckVIN = False
		Catch ex As Exception

		End Try
	End Function
	Public Function ValidateServerCertificate(sender As Object, certificate As X509Certificate, chain As X509Chain, sslPolicyErrors As SslPolicyErrors) As Boolean
		ValidateServerCertificate = True
	End Function

	Public Function Decode() As String
		Try
			Dim url As String = "https://api.dataonesoftware.com/webservices/vindecoder/decode"
			Dim postData As String = "client_id=19047&authorization_code=e806f6cca0026cfab447f424bfb69d859a1e4f62&decoder_query="
			postData += "" &
			"<decoder_query>" &
				"<decoder_settings>" &
				"<display>full</display>" &
					"<styles>on</styles>" &
					"<style_data_packs>" &
						"<basic_data>on</basic_data>" &
						"<pricing>off</pricing>" &
						"<engines>on</engines>" &
						"<transmissions>on</transmissions>" &
						"<specifications>on</specifications>" &
						"<installed_equipment>of</installed_equipment>" &
						"<optional_equipment>off</optional_equipment>" &
						"<generic_optional_equipment>off</generic_optional_equipment>" &
						"<colors>off</colors>" &
						"<warranties>on</warranties>" &
						"<fuel_efficiency>off</fuel_efficiency>" &
						"<green_scores>off</green_scores>" &
						"<crash_test>off</crash_test>" &
						"<recalls>off</recalls>" &
						"<service_bulletins>off</service_bulletins>" &
					"</style_data_packs>" &
					"<common_data>off</common_data>" &
					"<common_data_packs>" &
						"<basic_data>on</basic_data>" &
						"<pricing>on</pricing>" &
						"<engines>on</engines>" &
						"<transmissions>on</transmissions>" &
						"<specifications>on</specifications>" &
						"<installed_equipment>off</installed_equipment>" &
						"<generic_optional_equipment>off</generic_optional_equipment>" &
					"</common_data_packs>" &
				"</decoder_settings>" &
				"<query_requests>" &
					"<query_request identifier=""" & sContract & """>" &
						"<vin>" & sVIN & "</vin>" &
						"<year/>" &
						"<make/>" &
						"<model/>" &
						"<trim/>" &
						"<model_number/>" &
						"<package_code/>" &
						"<drive_type/>" &
						"<vehicle_type/>" &
						"<body_type/>" &
						"<doors/>" &
						"<bedlength/>" &
						"<wheelbase/>" &
						"<msrp/>" &
						"<invoice_price/>" &
						"<engine description="""">" &
							"<block_type/>" &
							"<cylinders/>" &
							"<displacement/>" &
							"<fuel_type/>" &
						"</engine>" &
						"<transmission description="""">" &
							"<trans_type/>" &
							"<trans_speeds/>" &
						"</transmission>" &
						"<optional_equipment_codes/>" &
						"<installed_equipment_descriptions/>" &
						"<interior_color description="""">" &
							"<color_code/>" &
						"</interior_color>" &
						"<exterior_color description="""">" &
							"<color_code/>" &
						"</exterior_color>" &
					"</query_request>" &
				"</query_requests>" &
			"</decoder_query>"

			Dim request As HttpWebRequest = WebRequest.Create(url)
			request.Method = "POST"
			request.ContentType = "application/x-www-form-urlencoded"
			request.ContentLength = postData.Length

			ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateServerCertificate)
			Dim writeStream As Stream = request.GetRequestStream()

			' Encode the string to be posted
			Dim encoding As UTF8Encoding = New UTF8Encoding()
			Dim bytes() As Byte = encoding.GetBytes(postData)
			writeStream.Write(bytes, 0, bytes.Length)
			writeStream.Close()

			' Get Response
			Dim response As HttpWebResponse = request.GetResponse()
			Dim reader As StreamReader = New StreamReader(response.GetResponseStream())

			' String is returned containing the data returned by the response
			Decode = reader.ReadToEnd()

		Catch ex As Exception

		End Try
		' Set the post data and URL

	End Function
End Class
