﻿Public Class clsAutoNationACH
    Private sCON As String
    Private lClaimID As Long
    Private sDealerNo As String
    Private sDateApproved As String
    Private sPaidDate As String
    Private dPaidAmt As Double
    Private sConfirmNumber As String
    Private SQL As String
    Private clCD As New clsDBO
    Private lUserID As Long

    Public Property ConnectiionString As String
        Get
            ConnectiionString = sCON
        End Get
        Set(value As String)
            sCON = value
        End Set
    End Property

    Public Property ClaimID As Long
        Get
            ClaimID = lClaimID
        End Get
        Set(value As Long)
            lClaimID = value
        End Set
    End Property

    Public Property ConfirmNumber As String
        Get
            ConfirmNumber = sConfirmNumber
        End Get
        Set(value As String)
            sConfirmNumber = value
        End Set
    End Property

    Public Property DealerNo As String
        Get
            DealerNo = sDealerNo
        End Get
        Set(value As String)
            sDealerNo = value
        End Set
    End Property

    Public Property DateApproved As String
        Get
            DateApproved = sDateApproved
        End Get
        Set(value As String)
            sDateApproved = value
        End Set
    End Property

    Public Property PaidDate As String
        Get
            PaidDate = sPaidDate
        End Get
        Set(value As String)
            sPaidDate = value
        End Set
    End Property

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Sub ProcessClaim()
        UpdateANACH()
        SQL = "select * from claimdetail cd "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and dateapprove = '" & sDateApproved & "' "
        SQL = SQL + "and claimdetailstatus = 'Approved' "
        SQL = SQL + "and ClaimPayeeID in ( "
        SQL = SQL + "select claimpayeeid from ClaimPayee cp "
        SQL = SQL + "inner join ServiceCenter sc "
        SQL = SQL + "on sc.ServiceCenterNo = cp.PayeeNo "
        SQL = SQL + "and sc.dealerno = '" & sDealerNo & "') "
        clCD.OpenDB(SQL, sCON)
        If clCD.RowCount > 0 Then
            For cnt = 0 To clCD.RowCount - 1
                clCD.GetRowNo(cnt)
                UpdatePaid()
            Next
        End If
        Dim clC As New clsClaim
        clC.ClaimID = lClaimID
        clC.DatePaid = sPaidDate
        clC.CON = sCON
        clC.UserID = lUserID
        clC.ProcessClaimStatus()
    End Sub

    Private Sub UpdateANACH()
        Dim clR As New clsDBO
        SQL = "select * from claimautonationach "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and dateapprove = '" & sDateApproved & "' "
        SQL = SQL + "and dealerno = '" & sDealerNo & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("datepaid") = sPaidDate
            clR.Fields("ConfirmNumber") = sConfirmNumber
            clR.SaveDB()
        End If
    End Sub

    Private Sub UpdatePaid()
        Dim clR As New clsDBO
        SQL = "update claimdetail "
        SQL = SQL + "set datepaid = '" & sPaidDate & "', "
        SQL = SQL + "claimdetailstatus = 'Paid', "
        SQL = SQL + "paidamt = totalamt, "
        SQL = SQL + "paidby = " & lUserID & ", "
        SQL = SQL + "moddate = '" & Today & "', "
        SQL = SQL + "modby = " & lUserID & " "
        SQL = SQL + "where claimdetailid = " & clCD.Fields("claimdetailid")
        clR.RunSQL(SQL, sCON)
    End Sub
End Class
