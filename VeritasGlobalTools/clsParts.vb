﻿Imports System.Net.Mail
Imports System.Net
Imports System.Text

Public Class clsParts
    Private sPartNo As String
    Private sPartName As String
    Private sVIN As String
    Private lClaimID As Long
    Private sClaimNo As String
    Private sBody As String
    Private lServiceCenterID

    Public Property ClaimID As Long
        Get
            ClaimID = lClaimID
        End Get
        Set(value As Long)
            lClaimID = value
        End Set
    End Property

    Public Property PartNo As String
        Get
            PartNo = sPartNo
        End Get
        Set(value As String)
            sPartNo = value
        End Set
    End Property

    Public Property PartName As String
        Get
            PartName = sPartName
        End Get
        Set(value As String)
            sPartName = value
        End Set
    End Property

    Public Property VIN As String
        Get
            VIN = sVIN
        End Get
        Set(value As String)
            sVIN = value
        End Set
    End Property

    Public Sub SendPartRequest()
        openclaim
        Dim client As New SmtpClient("smtp.office365.com", 587)
        client.UseDefaultCredentials = False
        client.Credentials = New System.Net.NetworkCredential(sSendEMail, sSendEMailPassword)
        client.EnableSsl = True
        Dim mail As New MailMessage
        mail.To.Add("steve@hannaworld.com")
        mail.To.Add("srhanna@veritasglobal.com")
        mail.From = New MailAddress(sSendEMail)
        mail.Subject = "Claim No: " & sClaimNo
        mail.IsBodyHtml = True
        sBody = "<p>Can you provide pricing on Part No: " & sPartNo & " Part Desc: " & sPartName & "</p>"
        sBody = sBody & "<p><b>Vehicle Info</b></br>"
        sBody = sBody & "VIN: " & sVIN & "</br>"
        GetVINInfo()
        sBody = sBody & "</p>"
        sBody = sBody & "<p><b>Repair Shop</b></br> "
        GetServiceCenter()
        sBody = sBody & "</p>"
        mail.Body = sBody
        client.Send(mail)
    End Sub

    Private Sub GetServiceCenter()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & lServiceCenterID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sBody = sBody & clR.Fields("servicecentername") & "</br>"
            sBody = sBody & clR.Fields("addr1") & "</br>"
            If clR.Fields("addr2").Length > 0 Then
                sBody = sBody & clR.Fields("addr2") & "</br>"
            End If
            sBody = sBody & clR.Fields("city") & " " & clR.Fields("state") & " " & clR.Fields("zip") & "</br>"
            sBody = sBody & clR.Fields("phone")
        End If
    End Sub

    Private Sub GetVINInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clB As New clsDBO
        Dim clE As New clsDBO
        SQL = "select * from vin.dbo.vin "
        SQL = SQL + "where vin = '" & sVIN.Substring(0, 11) & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from vin.dbo.basicdata "
            SQL = SQL + "where vinid = " & clR.Fields("vinid")
            clB.OpenDB(SQL, sCON)
            If clB.RowCount > 0 Then
                clB.GetRow()
                sBody = sBody & "Vehicle Type: " & clB.Fields("vehicletype") & "</br>"
                sBody = sBody & "Year: " & clB.Fields("year") & " "
                sBody = sBody & "Make: " & clB.Fields("make") & " "
                sBody = sBody & "Model: " & clB.Fields("model") & " "
                sBody = sBody & "Trim: " & clB.Fields("Trim") & "</br>"
                sBody = sBody & "Drive Type: " & clB.Fields("drivetype") & " "
            End If
            SQL = "select * from vin.dbo.engine "
            SQL = SQL + "where vinid = " & clR.Fields("vinid")
            clE.OpenDB(SQL, sCON)
            If clE.RowCount > 0 Then
                clE.GetRow()
                sBody = sBody & "Aspiration: " & clE.Fields("aspiration") & "</br> "
                sBody = sBody & "Fuel: " & clE.Fields("fueltype") & " "
                sBody = sBody & "Valve: " & clE.Fields("valves") & " "
                sBody = sBody & "Engine: " & clE.Fields("name")
            End If
        End If

    End Sub

    Private Sub OpenClaim()
        sClaimNo = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & lClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sClaimNo = clR.Fields("claimno")
            lServiceCenterID = clR.Fields("servicecenterid")
        End If
    End Sub
End Class
