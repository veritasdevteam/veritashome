﻿Public Class clsMoxyAddressChange
    Private SQL As String
    Private lContractID As Long
    Private clC As New clsDBO
    Private sCustomerID As String
    Public Sub UpdateMoxy(xContractID As Long)
        lContractID = xContractID
        opencontract
    End Sub

    Private Sub OpenContract()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            sCustomerID = ""
            GetCustomerID()
            addaddresschange
        End If
    End Sub

    Private Sub AddAddressChange()
        Dim clM As New clsDBO
        If clC.Fields("contractno").Substring(0, 3) = "VEP" Or clC.Fields("contractno").Substring(0, 3) = "REP" Then
            SQL = "select * from VeritasMoxy.dbo.epaddresschange "
            SQL = SQL + "where epaddresschangeid = 0 "
        Else
            SQL = "select * from VeritasMoxy.dbo.moxyaddresschange "
            SQL = SQL + "where moxyaddresschangeid = 0 "
        End If
        clM.OpenDB(SQL, sCON)
        If clM.RowCount = 0 Then
            clM.NewRow()
            clM.Fields("customerid") = sCustomerID
            clM.Fields("fname") = clC.Fields("fname")
            clM.Fields("lname") = clC.Fields("lname")
            clM.Fields("addr1") = clC.Fields("addr1")
            clM.Fields("addr2") = clC.Fields("addr2")
            clM.Fields("city") = clC.Fields("city")
            clM.Fields("state") = clC.Fields("state")
            clM.Fields("zip") = clC.Fields("zip")
            clM.Fields("phone") = clC.Fields("phone")
            clM.Fields("vgcontractid") = clC.Fields("contractid")
            clM.Fields("changedate") = Now
            clM.AddRow()
            clM.SaveDB()
        End If
    End Sub

    Private Sub GetCustomerID()
        Dim clM As New clsDBO
        If clC.Fields("contractno").Substring(0, 3) = "VEP" Or clC.Fields("contractno").Substring(0, 3) = "REP" Then
            SQL = "select * from VeritasMoxy.dbo.epcontract "
        Else
            SQL = "select * from VeritasMoxy.dbo.moxycontract "
        End If
        SQL = SQL + "where contractno = '" & clC.Fields("contractno") & "' "
        clM.OpenDB(SQL, sCON)
        If clM.RowCount > 0 Then
            clM.GetRow()
            sCustomerID = clM.Fields("CustomerID")
        End If
    End Sub
End Class
