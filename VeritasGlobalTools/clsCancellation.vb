﻿
Public Class clsCancellation
    Private lContractID As Long
    Private sCancelDate As String
    Private dClaims As Double
    Private dDealerAmt As Double
    Private dAdminAmt As Double
    Private dCustomer As Double
    Private clC As New clsDBO
    Private clD As New clsDBO
    Private clCSR As New clsDBO
    Private clCC As New clsDBO
    Private clP As New clsDBO
    Private SQL As String
    Private lUsedDays As Long
    Private dCancelPer As Double
    Private dCancelFee As Double
    Private dCancelTermPer As Double
    Private sFormAddress As String
    Private sFormAddressOut As String
    Private sHTTPAddress As String
    Private bQuote As Boolean
    Private sCancelInfo As String
    Private dGrossCustomer As Double
    Private dGrossDealer As Double
    Private dGrossAdmin As Double
    Private bFLCustomer As Boolean
    Private bFLAdmin As Boolean

    Public Property ContractID As Long
        Get
            ContractID = lContractID
        End Get
        Set(value As Long)
            lContractID = value
        End Set
    End Property

    Public Property CancelDate As String
        Get
            CancelDate = sCancelDate
        End Get
        Set(value As String)
            sCancelDate = value
        End Set
    End Property

    Public ReadOnly Property CancelFee As Double
        Get
            CancelFee = dCancelFee
        End Get
    End Property

    Public ReadOnly Property CancelInfo As String
        Get
            CancelInfo = sCancelInfo
        End Get
    End Property

    Public ReadOnly Property CancelTermPer As Double
        Get
            CancelTermPer = dCancelTermPer
        End Get
    End Property

    Public ReadOnly Property CancelPer As Double
        Get
            CancelPer = dCancelPer
        End Get
    End Property

    Public ReadOnly Property Claims As Double
        Get
            Claims = dClaims
        End Get
    End Property

    Public ReadOnly Property DealerAmt As Double
        Get
            DealerAmt = dDealerAmt
        End Get
    End Property

    Public ReadOnly Property AdminAmt As Double
        Get
            AdminAmt = dAdminAmt
        End Get
    End Property

    Public Property FormAddress As String
        Get
            FormAddress = sFormAddress
        End Get
        Set(value As String)
            sFormAddress = value
        End Set
    End Property

    Public Property HTTPAddress As String
        Get
            HTTPAddress = sHTTPAddress
        End Get
        Set(value As String)
            sHTTPAddress = value
        End Set
    End Property

    Public Property FLCustomer As Boolean
        Get
            FLCustomer = bFLCustomer
        End Get
        Set(value As Boolean)
            bFLCustomer = value
        End Set
    End Property

    Public Property FLAdmin As Boolean
        Get
            FLAdmin = bFLAdmin
        End Get
        Set(value As Boolean)
            bFLAdmin = value
        End Set
    End Property

    Public Property FormAddressOut As String
        Get
            FormAddressOut = sFormAddressOut
        End Get
        Set(value As String)
            sFormAddressOut = value
        End Set
    End Property

    Public Property Quote As Boolean
        Get
            Quote = bQuote
        End Get
        Set(value As Boolean)
            bQuote = value
        End Set
    End Property

    Public ReadOnly Property CustomerCost As Double
        Get
            CustomerCost = dCustomer
        End Get
    End Property

    Public ReadOnly Property GrossDealer As Double
        Get
            GrossDealer = dGrossDealer
        End Get
    End Property

    Public ReadOnly Property GrossAdmin As Double
        Get
            GrossAdmin = dGrossAdmin
        End Get
    End Property

    Public ReadOnly Property GrossCustomer As Double
        Get
            GrossCustomer = dGrossCustomer
        End Get
    End Property

    Private Sub GetCancelTable()
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & lContractID
        clCC.OpenDB(SQL, sCON)
    End Sub

    Private Sub OpenContract()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
    End Sub

    Public Sub CalculateCancellation()

        sCancelInfo = ""
        GetContractInfo()
        If clC.RowCount > 0 Then
            clC.GetRow()
            GetDealerInfo()
            If clD.RowCount > 0 Then
                clD.GetRow()
                GetCancelStateRules()
                If clCSR.RowCount > 0 Then
                    clCSR.GetRow()
                    CalcCancel()
                End If
            End If
        End If
    End Sub

    Private Sub CalcCancel()
        dAdminAmt = clC.Fields("moxydealercost")
        dDealerAmt = clC.Fields("customercost") - clC.Fields("MoxyDealerCost")

        If clCSR.Fields("noclaimdeduct") = True Then
            dClaims = 0
        Else
            dClaims = CalcClaims()
        End If
        If clC.Fields("lienholder").ToLower = "paylink" Or clC.Fields("lienholder").ToLower = "mepco" Or clC.Fields("lienholder").ToLower = "omnisure" Then
            dClaims = 0
        End If
        If dClaims = 86 Or dClaims = 136 Or dClaims = 172 Or dClaims = 222 Or dClaims = 258 Then
            dClaims = 0
        End If
        lUsedDays = DateDiff(DateInterval.Day, CDate(Format(CDate(clC.Fields("saledate")), "M/d/yyyy")), CDate(sCancelDate))

        If lUsedDays <= CLng(clCSR.Fields("maxdays")) Then
            dCancelFee = 0
            If clCSR.Fields("noclaimdeduct") = False Then
                dAdminAmt = dAdminAmt - dClaims
                If dAdminAmt < 0 Then
                    dDealerAmt = dDealerAmt - dAdminAmt
                    dAdminAmt = 0
                End If
                If dDealerAmt < 0 Then
                    dDealerAmt = 0
                End If
            End If
            dGrossCustomer = dDealerAmt + dAdminAmt
            dGrossAdmin = dAdminAmt
            dGrossDealer = dDealerAmt
            dCancelTermPer = 1
            dCancelPer = 1
            dCustomer = dGrossCustomer
            Exit Sub
        End If

        CalcCancelPer()
        dDealerAmt = dDealerAmt * dCancelPer
        dAdminAmt = dAdminAmt * dCancelPer
        dGrossCustomer = dDealerAmt + dAdminAmt
        dGrossAdmin = dAdminAmt
        dGrossDealer = dDealerAmt

        CalcCancelFee()
        If dCancelPer = 1 Then
            dCancelFee = 0
        End If
        If clC.Fields("state") = "FL" Then
            If bFLAdmin Then
                dCancelFee = 0
            End If
        End If
        dAdminAmt = dAdminAmt - dCancelFee
        If clCSR.Fields("noclaimdeduct") = False Then
            dAdminAmt = dAdminAmt - dClaims
            If dAdminAmt < 0 Then
                dDealerAmt = dDealerAmt + dAdminAmt
                dAdminAmt = 0
            End If
            If dDealerAmt < 0 Then
                dDealerAmt = 0
            End If
        End If
        dCustomer = dAdminAmt + dDealerAmt
        If clCSR.Fields("regulated") = True Then
            If clD.Fields("dealerstatusid") = 5 Then
                dAdminAmt = dAdminAmt + dDealerAmt
                dCustomer = dAdminAmt
            End If
        End If
        If dAdminAmt < 0 Then
            dAdminAmt = 0
        End If
        If dDealerAmt < 0 Then
            dDealerAmt = 0
        End If
    End Sub

    Private Sub CalcCancelFee()
        Dim dCancelFeeTemp As Double
        If clCSR.Fields("cancelpercent") = 0 Then
            dCancelFee = clCSR.Fields("cancelfee")
            Exit Sub
        End If
        dCancelFeeTemp = clC.Fields("customercost") * clCSR.Fields("cancelpercent")
        If dCancelFeeTemp < clCSR.Fields("cancelfee") And dCancelFeeTemp > 0 Then
            dCancelFee = dCancelFeeTemp
        Else
            dCancelFee = clCSR.Fields("cancelfee")
        End If
    End Sub

    Private Sub CalcCancelPer()
        Dim dCanTime As Double
        Dim lTermDays As Double

        lTermDays = DateDiff(DateInterval.Day, CDate(Format(CDate(clC.Fields("saledate")), "M/d/yyy")), CDate(clC.Fields("expdate")))
        dCanTime = 1 - (lUsedDays / CDbl(lTermDays))
        If dCanTime < 0 Then
            dCanTime = 0
            sCancelInfo = "Cancel Date is larger than Exp. Date"
        End If
        dCancelTermPer = dCanTime
        dCancelPer = dCancelTermPer
    End Sub

    Private Function CalcClaims() As Double
        Dim clCL As New clsDBO
        CalcClaims = 0
        SQL = "select sum(cd.paidamt) as PaidAmt from claim cl "
        SQL = SQL + "inner join claimdetail cd on cl.ClaimID = cd.claimid "
        SQL = SQL + "inner join contract c on c.contractid = cl.ContractID "
        SQL = SQL + "where contractno = '" & clC.Fields("contractno") & "' "
        SQL = SQL + "and cl.Status = 'Paid' "
        SQL = SQL + "and cd.JobNo <> 'A03' "
        clCL.OpenDB(SQL, sCON)
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            If clCL.Fields("paidamt").Length > 0 Then
                CalcClaims = clCL.Fields("paidamt")
            End If
        End If
    End Function

    Private Sub GetCancelStateRules()
        SQL = "select * from cancelstaterules "
        SQL = SQL + "where state = '" & clC.Fields("state") & "' "
        clCSR.OpenDB(SQL, sCON)
    End Sub

    Private Sub GetDealerInfo()
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & clC.Fields("dealerid")
        clD.OpenDB(SQL, sCON)
    End Sub

    Private Sub GetContractInfo()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
    End Sub

End Class
