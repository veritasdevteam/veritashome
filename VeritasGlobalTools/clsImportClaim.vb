﻿Imports System
Imports System.Net.Mail
Imports System.Net
Imports System.Text

Public Class clsImportClaim
    Private lVeritasClaimID As Long
    Private lAssignedToID As Long
    Private lUserID As Long
    Private clVC As New clsDBO
    Private SQL As String
    Private sNewClaimNo As String
    Private lClaimID As Long
    Private lClaimPayeeID As Long
    Private dDeductAmt As Double

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Property AssignedToID As Long
        Get
            AssignedToID = lAssignedToID
        End Get
        Set(value As Long)
            lAssignedToID = value
        End Set
    End Property

    Public Property VeritasClaimID As Long
        Get
            VeritasClaimID = lVeritasClaimID
        End Get
        Set(value As Long)
            lVeritasClaimID = value
        End Set
    End Property

    Public Sub ProcessClaim()
        SQL = "select * from veritasclaims.dbo.claim "
        SQL = SQL + "where claimid = " & lVeritasClaimID
        clVC.OpenDB(SQL, sCON)
        If clVC.RowCount > 0 Then
            clVC.GetRow()
            GetClaimNo()
            PlaceEmailFax()
            CheckClaimPayee()
            InsertClaimDetailDeduct()
            InsertClaimDetailPart()
            InsertClaimDetailLabor()
            Place3Cs()
            CloseClaim()
            SendEMail()
            sendconfirm
        End If
    End Sub

    Private Sub Place3Cs()
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 1 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = lClaimID
            clR.Fields("claimnotetypeid") = 1
            clR.Fields("note") = clVC.Fields("complaint")
            clR.Fields("credate") = Today
            clR.Fields("creby") = lUserID
            clR.Fields("moddate") = Today
            clR.Fields("modby") = lUserID
            clR.Fields("scsclaimnoteid") = 0
            clR.Fields("notetext") = clVC.Fields("complaint")
            clR.AddRow()
            clR.SaveDB()
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 2 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = lClaimID
            clR.Fields("claimnotetypeid") = 2
            clR.Fields("note") = clVC.Fields("correction")
            clR.Fields("credate") = Today
            clR.Fields("creby") = lUserID
            clR.Fields("moddate") = Today
            clR.Fields("modby") = lUserID
            clR.Fields("scsclaimnoteid") = 0
            clR.Fields("notetext") = clVC.Fields("correction")
            clR.AddRow()
            clR.SaveDB()
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 3 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = lClaimID
            clR.Fields("claimnotetypeid") = 3
            clR.Fields("note") = clVC.Fields("cause")
            clR.Fields("credate") = Today
            clR.Fields("creby") = lUserID
            clR.Fields("moddate") = Today
            clR.Fields("modby") = lUserID
            clR.Fields("scsclaimnoteid") = 0
            clR.Fields("notetext") = clVC.Fields("cause")
            clR.AddRow()
            clR.SaveDB()
        End If

    End Sub

    Private Sub PlaceEmailFax()
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & clVC.Fields("servicecenterid")
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clVC.Fields("email").Length > 0 Then
                clR.Fields("email") = clVC.Fields("email")
            End If
            If clVC.Fields("fax").Length > 0 Then
                clVC.Fields("fax") = clVC.Fields("fax").Replace("-", "")
                clVC.Fields("fax") = clVC.Fields("fax").Replace(" ", "")
                clVC.Fields("fax") = clVC.Fields("fax").Replace("(", "")
                clVC.Fields("fax") = clVC.Fields("fax").Replace(")", "")
                clVC.Fields("fax") = clVC.Fields("fax").Replace(".", "")
                clR.Fields("fax") = clVC.Fields("fax")
            End If
            clR.SaveDB()
        End If
    End Sub

    Private Sub SendEMail()
        Dim clR As New clsDBO
        Dim sEMail As String
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & lAssignedToID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            Exit Sub
        Else
            clR.GetRow()
            sEMail = clR.Fields("email")
        End If
        Dim client As New SmtpClient("smtp.office365.com", 587)
        client.UseDefaultCredentials = False
        client.Credentials = New System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3")
        client.EnableSsl = True
        Dim mail As New MailMessage
        mail.To.Add(sEMail)
        mail.From = New MailAddress("info@veritasglobal.com")
        mail.Bcc.Add("steve@hannaworld.com")
        mail.Subject = "New Claim assigned to you."
        mail.IsBodyHtml = True
        mail.Body = sNewClaimNo & " has been assigned to from the Veritas Global Claim."
        client.Send(mail)

    End Sub

    Private Sub SendConfirm()
        Dim clR As New clsDBO
        Dim sAdvisor As String
        Dim sEMail As String
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & lAssignedToID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount = 0 Then
            Exit Sub
        Else
            clR.GetRow()
            sAdvisor = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
        SQL = "select * from VeritasClaims.dbo.Claim "
        SQL = SQL + "where claimno = '" & sNewClaimNo & "' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sEMail = clR.Fields("email")
        Else
            Exit Sub
        End If
        If sEMail.Length = 0 Then
            Exit Sub
        End If
        Dim client As New SmtpClient("smtp.office365.com", 587)
        client.UseDefaultCredentials = False
        client.Credentials = New System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3")
        client.EnableSsl = True
        Dim mail As New MailMessage
        mail.To.Add(sEMail)
        mail.From = New MailAddress("info@veritasglobal.com")
        mail.Bcc.Add("steve@hannaworld.com")
        mail.Subject = "Your New Claim " & sNewClaimNo
        mail.IsBodyHtml = True
        Dim sBody As String
        sBody = "Your claim has been assigned Claim No: " & sNewClaimNo & ".</br></br>"
        sBody = sBody + "Claim Adjust has been assigned to " & sAdvisor & ".</br></br>"
        sBody = sBody + "Thank you for contacting Veritas Global Protection."
        mail.Body = sBody
        client.Send(mail)

    End Sub


    Private Sub CloseClaim()
        Dim clR As New clsDBO
        SQL = "update veritasclaims.dbo.claim "
        SQL = SQL + "set processclaimdate = '" & Today & "', "
        SQL = SQL + "processclaimby = " & lUserID & " "
        SQL = SQL + "where claimid = " & clVC.Fields("claimid")
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub InsertClaimDetailLabor()
        Dim clCL As New clsDBO
        Dim clR As New clsDBO
        SQL = "select * from veritasclaims.dbo.claimlabor "
        SQL = SQL + "where claimid = " & clVC.Fields("claimid")
        clCL.OpenDB(SQL, sCON)
        If clCL.RowCount > 0 Then
            For cnt = 0 To clCL.RowCount - 1
                clCL.GetRowNo(cnt)
                If clCL.Fields("tax").Length = 0 Then
                    clCL.Fields("tax") = 0
                End If
                SQL = "insert into claimdetail "
                SQL = SQL + "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc) "
                SQL = SQL + "values (" & lClaimID & ", "
                SQL = SQL + "'Labor', 'J01', "
                SQL = SQL + clCL.Fields("qty") & ","
                SQL = SQL + clCL.Fields("cost") & ","
                If clCL.Fields("cost").Length > 0 And clCL.Fields("qty").Length > 0 Then
                    SQL = SQL + CStr(CDbl(clCL.Fields("qty")) * CDbl(clCL.Fields("cost"))) & ","
                Else
                    SQL = SQL + "0,"
                End If
                SQL = SQL + clCL.Fields("tax") & ","
                If clCL.Fields("cost").Length > 0 And clCL.Fields("qty").Length > 0 Then
                    SQL = SQL + CStr(CDbl(clCL.Fields("qty")) * CDbl(clCL.Fields("cost")) * (CDbl(clCL.Fields("tax")) / 100)) & ","
                Else
                    SQL = SQL + "0,"
                End If

                SQL = SQL + clCL.Fields("totalamt") & ","
                SQL = SQL + "'Requested', "
                SQL = SQL & lUserID & ","
                SQL = SQL + "'" & Today & "', "
                SQL = SQL + "'" & Today & "', "
                SQL = SQL & lUserID & ","
                SQL = SQL & lClaimPayeeID & ","
                SQL = SQL + "'" & clCL.Fields("labordesc") & "')"
                clR.RunSQL(SQL, sCON)
            Next
        End If
    End Sub

    Private Sub InsertClaimDetailPart()
        Dim clCP As New clsDBO
        Dim clR As New clsDBO
        SQL = "select * from veritasclaims.dbo.claimpart "
        SQL = SQL + "where claimid = " & clVC.Fields("claimid")
        clCP.OpenDB(SQL, sCON)
        If clCP.RowCount > 0 Then
            For cnt = 0 To clCP.RowCount - 1
                clCP.GetRowNo(cnt)
                SQL = "insert into claimdetail "
                SQL = SQL + "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc, PartNo) "
                SQL = SQL + "values (" & lClaimID & ", "
                SQL = SQL + "'Part', 'J01', "
                SQL = SQL + clCP.Fields("qty") & ","
                SQL = SQL + clCP.Fields("cost") & ","
                SQL = SQL + CStr(CDbl(clCP.Fields("qty")) * CDbl(clCP.Fields("cost")))
                SQL = SQL + clCP.Fields("tax") & ","
                SQL = SQL + CStr(CDbl(clCP.Fields("qty")) * CDbl(clCP.Fields("cost")) * (CDbl(clCP.Fields("tax")) / 100)) & ","
                SQL = SQL + clCP.Fields("totalamt") & ","
                SQL = SQL + CStr(dDeductAmt) & ",'Requested', "
                SQL = SQL & lUserID & ","
                SQL = SQL + "'" & Today & "', "
                SQL = SQL + "'" & Today & "', "
                SQL = SQL & lUserID & ","
                SQL = SQL & lClaimPayeeID & ","
                SQL = SQL + "'" & clCP.Fields("partdesc") & "',"
                SQL = SQL + "'" & clCP.Fields("partno") & "')"
                clR.RunSQL(SQL, sCON)
            Next
        End If
    End Sub

    Private Sub InsertClaimDetailDeduct()
        Dim clR As New clsDBO
        dDeductAmt = 0
        GetDeduct()
        dDeductAmt = dDeductAmt * -1
        SQL = "insert into claimdetail "
        SQL = SQL + "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc) "
        SQL = SQL + "values (" & lClaimID & ", "
        SQL = SQL + "'Deductible', 'A01', 1, "
        SQL = SQL + CStr(dDeductAmt) & ", "
        SQL = SQL + CStr(dDeductAmt) & ", "
        SQL = SQL + "0, 0, "
        SQL = SQL + CStr(dDeductAmt) & ",'Requested', "
        SQL = SQL + CStr(lUserID) & ","
        SQL = SQL + "'" & Today & "', "
        SQL = SQL + "'" & Today & "', "
        SQL = SQL + CStr(lUserID) & ","
        SQL = SQL + CStr(lClaimPayeeID) & ",'')"
        clR.RunSQL(SQL, sCON)
    End Sub

    Private Sub GetDeduct()
        Dim clC As New clsDBO
        Dim clD As New clsDBO
        SQL = "select deductid from contract "
        SQL = SQL + "where contractid = " & clVC.Fields("contractid")
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "select * from deductible "
            SQL = SQL + "where deductid = " & clC.Fields("deductid")
            clD.OpenDB(SQL, sCON)
            If clD.RowCount > 0 Then
                clD.GetRow()
                dDeductAmt = clD.Fields("deductamt")
            End If
        End If
    End Sub

    Private Sub CheckClaimPayee()
        Dim clSC As New clsDBO
        Dim clCP As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & clVC.Fields("servicecenterid")
        clSC.OpenDB(SQL, sCON)
        If clSC.RowCount > 0 Then
            clSC.GetRow()
MoveHere:
            SQL = "select * from claimpayee "
            SQL = SQL + "where payeeno = '" & clSC.Fields("servicecenterno") & "' "
            clCP.OpenDB(SQL, sCON)
            If clCP.RowCount > 0 Then
                clCP.GetRow()
                lClaimPayeeID = clCP.Fields("claimpayeeid")
            Else
                clCP.NewRow()
                clCP.Fields("payeeno") = clSC.Fields("servicecenterno")
                clCP.Fields("payeename") = clSC.Fields("servicecentername")
                clCP.Fields("addr1") = clSC.Fields("addr1")
                clCP.Fields("addr2") = clSC.Fields("addr2")
                clCP.Fields("city") = clSC.Fields("city")
                clCP.Fields("state") = clSC.Fields("state")
                clCP.Fields("zip") = clSC.Fields("zip")
                clCP.Fields("phone") = clSC.Fields("phone")
                clCP.AddRow()
                clCP.SaveDB()
                GoTo MoveHere
            End If
        End If
    End Sub

    Private Sub GetClaimNo()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim sClaimNo As String
        Dim lClaimNo As Long = 0
MoveHere:
        sClaimNo = ""
        SQL = "select max(claimno) as claimno from claim "
        SQL = SQL + "where claimno like 'C1%' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            sClaimNo = clC.Fields("claimno")
            sClaimNo = sClaimNo.Replace("C1", "")
            lClaimNo = sClaimNo
            lClaimNo = lClaimNo + 1
            sClaimNo = Format(lClaimNo, "100000000")
            sClaimNo = "C" & sClaimNo
        End If
        If sClaimNo.Length > 0 Then
            SQL = "select * from claim "
            SQL = SQL + "where claimno = '" & sClaimNo & "' "
            clC.OpenDB(SQL, sCON)
            If clC.RowCount > 0 Then
                GoTo MoveHere
            Else
                clC.NewRow()
                clC.Fields("claimno") = sClaimNo
                clC.Fields("status") = "Open"
                If clVC.Fields("contractid").Length > 0 Then
                    clC.Fields("contractid") = clVC.Fields("contractid")

                Else
                    clC.Fields("contractid") = 0
                End If
                clC.Fields("lossdate") = Today
                clC.Fields("claimactivityid") = 1
                clC.Fields("servicecenterid") = clVC.Fields("servicecenterid")
                clC.Fields("sccontactinfo") = clVC.Fields("contactname") & " " & clVC.Fields("contactphone") & " " & clVC.Fields("cell")
                If clVC.Fields("odometer").Length > 0 Then
                    clC.Fields("lossmile") = clVC.Fields("odometer")
                End If
                clC.Fields("ronumber") = clVC.Fields("ronumber")
                clC.Fields("creby") = lUserID
                clC.Fields("assignedto") = lAssignedToID
                clC.Fields("Modby") = lUserID
                clC.Fields("credate") = Today
                clC.Fields("moddate") = Today
                clC.Fields("addnew") = True
                clC.AddRow()
                clC.SaveDB()
            End If
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimno = '" & sClaimNo & "' "
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            sNewClaimNo = clC.Fields("claimno")
            lClaimID = clC.Fields("claimid")
        End If

        SQL = "update veritasclaims.dbo.claim "
        SQL = SQL + "set claimno = '" & sNewClaimNo & "' "
        SQL = SQL + "where claimid = " & clVC.Fields("claimid")
        clC.RunSQL(SQL, sCON)
        SQL = "insert into claimopenhistory "
        SQL = SQL + "(claimid, opendate, openby) "
        SQL = SQL + "values (" & clC.Fields("claimid") & ",'"
        SQL = SQL + Today & "',"
        SQL = SQL + clC.Fields("assignedto") & ")"
        clVC.RunSQL(SQL, sCON)

    End Sub


End Class
