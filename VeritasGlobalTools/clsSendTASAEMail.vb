﻿Imports System.Net.Mail
Imports System.Net

Public Class clsSendTASAEMail
    Private lClaimDetailID As Long
    Private lUserID As Long
    Private lClaimID As Long
    Private sTicketNo As String
    Private clUI As New clsDBO
    Private SQL As String
    Private clC As New clsDBO
    Private clCD As New clsDBO
    Private clCL As New clsDBO
    Private clPT As New clsDBO
    Private clD As New clsDBO
    Private clSC As New clsDBO
    Private sComplaint As String
    Private sCorrective As String
    Private sCause As String
    Private bTruPic As Boolean
    Private sLink As String
    Private bInspection As Boolean
    Private sAdvisorName As String

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Property TicketNo As String
        Get
            TicketNo = sTicketNo
        End Get
        Set(value As String)
            sTicketNo = value
        End Set
    End Property

    Public Property ClaimDetailID As Long
        Get
            ClaimDetailID = lClaimDetailID
        End Get
        Set(value As Long)
            lClaimDetailID = value
        End Set
    End Property

    Public Property ClaimID As Long
        Get
            ClaimID = lClaimID
        End Get
        Set(value As Long)
            lClaimID = value
        End Set
    End Property

    Public Sub SendEMail()
        GetUserName()
        SendTo(clUI.Fields("email"))
        SendTo("steve@hannaworld.com")
        SendTo("tylerrockey@tasaky.com")
        SendTo("travisscott@tasaky.com")
        SendTo("jakemuck@tasaky.com")
        SendTo("zpeters@CLAIMSPROCESSINGDEPT.COM")
        SendTo("mhansen@CLAIMSPROCESSINGDEPT.COM")

    End Sub

    Private Sub SendTo(xEMail As String)
        Dim sBody As String

        Try
            GetUserName()
            GetClaim()
            GetContract()
            GetDealer()
            GetPlanType()
            GetClaimDetail()
            GetServiceCenter()
            GetComplaint()
            GetCause()
            GetCorrective()
            GetTruPic()
            GetInspection()
            Dim client As New SmtpClient("smtp.office365.com", 587)
            client.UseDefaultCredentials = False
            client.Credentials = New System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3")
            client.EnableSsl = True

            Dim mail As New MailMessage
            mail.To.Add(xEMail)
            mail.From = New MailAddress("info@veritasglobal.com")
            mail.Subject = clCL.Fields("claimno") & ""
            mail.IsBodyHtml = True
            sBody = "Ticket No: " & sTicketNo & "</br>"
            sBody = sBody & "Contract Coverage: " & clPT.Fields("plantype") & "</br>"
            sBody = sBody & "Claim Number: " & clCL.Fields("claimno") & "</br>"
            sBody = sBody & "Contract No: " & clC.Fields("contractno") & "</br>"
            sBody = sBody & "Customer Name: " & clC.Fields("fname") & " " & clC.Fields("lname") & "</br>"
            sBody = sBody & "Vehicle Make and Model: " & clC.Fields("make") & " " & clC.Fields("model") & "</br>"
            sBody = sBody & "Date Sold: " & clC.Fields("saledate") & "</br>"
            sBody = sBody & "Date of Claim: " & clCL.Fields("credate") & "</br>"
            sBody = sBody & "Days and Miles into Contract: " & DateDiff(DateInterval.Day, CDate(clC.Fields("saledate")), CDate(clCL.Fields("lossdate"))) & " / "
            sBody = sBody & clCL.Fields("lossmile") - clC.Fields("effmile") & "</br>"
            sBody = sBody & "Aftersale (y/n): "
            If clC.Fields("contractno").Contains("VEP") Then
                sBody = sBody & "Y" & "</br>"
            Else
                If clC.Fields("contractno").Contains("REP") Then
                    sBody = sBody & "Y" & "</br>"
                Else
                    sBody = sBody & "N" & "</br>"
                End If
            End If
            sBody = sBody + "Selling Dealer: " & clD.Fields("dealername") & "</br>"
            sBody = sBody + "Repair Facility: " & clSC.Fields("servicecentername") & "</br>"
            If clSC.Fields("dealerno").Length > 0 Then
                If clSC.Fields("dealerno").Substring(0, 1) = "2" Then
                    sBody = sBody + "RF in AN Network (y/n): Y " & "</br>"
                Else
                    sBody = sBody + "RF in AN Network (y/n): N " & "</br>"
                End If
            Else
                sBody = sBody + "RF in AN Network (y/n): N " & "</br>"
            End If
            sBody = sBody + "RF in Prime Network (y/n): N" & "</br>"
            sBody = sBody + "RF in Cooper Network (y/n): N" & "</br>"
            sBody = sBody + "Claims on Contract: " & CountClaim() - 1 & "</br>"
            sBody = sBody + "Claims Total Paid: " & Format(GetClaimPaidAmt, "#,##0.00") & "</br>"
            sBody = sBody + "Estimate: " & Format(GetEstimate, "#,##0.00") & "</br>"
            sBody = sBody + "Failures: " & clCD.Fields("ClaimDesc") & "</br>"
            sBody = sBody + "Tech finds: " & "</br>"
            sBody = sBody + sComplaint & "</br>" & "</br>"
            sBody = sBody + "Summary:" & "</br>"
            sBody = sBody + sCause & "</br>" & "</br>"
            sBody = sBody + "Detail:" & "</br>"
            sBody = sBody + sCorrective & "</br>" & "</br>"
            If bTruPic Then
                sBody = sBody + "Inspection: Tru Pic" & "</br>"
                sBody = sBody + sLink & "</br>" & "</br>"
            ElseIf bInspection Then
                sBody = sBody + "Inspection: WIS Inspection" & "</br>"
                sBody = sBody + sLink & "</br>" & "</br>"
            Else
                sBody = sBody + "Inspection: No Inspection or Pending Inspection" & "</br>" & "</br>"
            End If
            sBody = sBody + "Please advise" & "</br>"

            sBody = sBody + "<table width=""100%"" cellspacing=""0"" cellpadding=""0"">"
            sBody = sBody + "<tr>"
            sBody = sBody + "<td>"
            sBody = sBody + "<table cellspacing=""6"" cellpadding=""6"">"
            sBody = sBody + "<tr>"
            sBody = sBody + "<td style=""border-radius: 2px;"" bgcolor=""#1eabe2"">"
            sBody = sBody + "<a href="
            sBody = sBody + """https://www.veritasgenerator.com/ticket/response.aspx?ticketno="
            sBody = sBody + sTicketNo + "&Response=Approve&email=" & xEMail
            sBody = sBody + """ target=""_blank"" style=""padding: 8px 12px; border: 1px solid #1eabe2;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;"">"
            sBody = sBody + "Approve"
            sBody = sBody + "</a>"
            sBody = sBody + "</td>"
            sBody = sBody + "<td style=""border-radius: 2px;"" bgcolor=""#1eabe2"">"
            sBody = sBody + "<a href=""https://www.veritasgenerator.com/ticket/response.aspx?ticketno="
            sBody = sBody + sTicketNo + "&Response=Deny&email=" & xEMail
            sBody = sBody + """ target=""_blank"" style=""padding: 8px 12px; border: 1px solid #1eabe2;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;"">"
            sBody = sBody + "Deny"
            sBody = sBody + "</a>"
            sBody = sBody + "</td>"
            sBody = sBody + "</tr>"
            sBody = sBody + "</table>"
            sBody = sBody + "</td>"
            sBody = sBody + "</tr>"
            sBody = sBody + "</table>"
            sBody = sBody + "<b>" & sAdvisorName & "</b>" & "</br>"
            sBody = sBody + "Claims Adjuster" & "</br>"
            sBody = sBody + "VERITAS GLOBAL PROTECTION" & "</br>"

            mail.Body = sBody
            client.Send(mail)
        Catch ex As Exception
            Dim s As String
            s = ex.Message
        End Try

    End Sub

    Private Sub GetInspection()
        bInspection = False
        sLink = ""
        Dim clR As New clsDBO
        SQL = "select * from claiminspection "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and not detailsurl is null "
        SQL = SQL + "order by requestdate desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            bInspection = True
            sLink = clR.Fields("detailsurl")
        End If
    End Sub

    Private Sub GetTruPic()
        bTruPic = False
        sLink = ""
        Dim clR As New clsDBO
        SQL = "select * from claimdocument "
        SQL = SQL + "where (DocumentName = 'truepic' "
        SQL = SQL + "or DocumentName = 'trupic') "
        SQL = SQL + "and claimid = " & lClaimID & " "
        SQL = SQL + "order by credate desc "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            bTruPic = True
            sLink = clR.Fields("documentlink")
            sLink = sLink.Replace("~", "https://www.veritasgenerator.com")
        End If
    End Sub

    Private Sub GetCorrective()
        sCorrective = ""
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 2 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sCorrective = clR.Fields("note")
        End If
    End Sub

    Private Sub GetCause()
        sCause = ""
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 3 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sCause = clR.Fields("note")
        End If
    End Sub

    Private Sub GetComplaint()
        sComplaint = ""
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimnotetypeid = 1 "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            sComplaint = clR.Fields("note")
        End If
    End Sub

    Private Function GetEstimate() As Double
        GetEstimate = 0
        Dim clR As New clsDBO
        SQL = "select sum(reqamt) as Amt from claimdetail cd "
        SQL = SQL + "inner join claim cl on cl.claimid = cd.claimid "
        SQL = SQL + "where cl.claimid = " & lClaimID
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()

            If clR.Fields("amt").Length > 0 Then
                GetEstimate = clR.Fields("amt")
            End If
        End If
    End Function

    Private Function GetClaimPaidAmt() As Double
        GetClaimPaidAmt = 0
        Dim clR As New clsDBO
        SQL = "select sum(totalamt) as Amt from claimdetail cd "
        SQL = SQL + "inner join claim cl on cl.claimid = cd.claimid "
        SQL = SQL + "where contractid = " & clC.Fields("contractid") & " "
        SQL = SQL + "and claimdetailstatus = 'Paid' "
        clR.OpenDB(SQL, sCON)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                GetClaimPaidAmt = clR.Fields("amt")
            End If
        End If
    End Function

    Private Function CountClaim() As Long
        Dim clR As New clsDBO
        CountClaim = 0
        SQL = "select * from claim "
        SQL = SQL + "where contractid = " & clC.Fields("contractid")
        clR.OpenDB(SQL, sCON)
        CountClaim = clR.RowCount
    End Function

    Private Sub GetServiceCenter()
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & clCL.Fields("servicecenterid")
        clSC.OpenDB(SQL, sCON)
        If clSC.RowCount > 0 Then
            clSC.GetRow()

        End If
    End Sub

    Private Sub GetDealer()
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & clC.Fields("dealerid")
        clD.OpenDB(SQL, sCON)
        If clD.RowCount > 0 Then
            clD.GetRow()
        End If
    End Sub

    Private Sub GetPlanType()
        SQL = "select * from plantype "
        SQL = SQL + "where plantypeid = " & clC.Fields("plantypeid")
        clPT.OpenDB(SQL, sCON)
        If clPT.RowCount > 0 Then
            clPT.GetRow()
        End If
    End Sub

    Private Sub GetContract()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & clCL.Fields("contractid")
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
        End If
    End Sub

    Private Sub GetClaimDetail()
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & lClaimDetailID
        clCD.OpenDB(SQL, sCON)
        If clCD.RowCount > 0 Then
            clCD.GetRow()
        End If
    End Sub

    Private Sub GetUserName()
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & lUserID & " "
        clUI.OpenDB(SQL, sCON)
        If clUI.RowCount > 0 Then
            clUI.GetRow()
            sAdvisorName = clUI.Fields("fname") & " " & clUI.Fields("lname")
        End If
    End Sub

    Private Sub GetClaim()
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & lClaimID
        clCL.OpenDB(SQL, sCON)
        If clCL.RowCount > 0 Then
            clCL.GetRow()

        End If
    End Sub

End Class
