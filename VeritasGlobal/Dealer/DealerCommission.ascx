﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DealerCommission.ascx.vb" Inherits="VeritasGlobal.DealerCommission" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlCommission" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddCommission" runat="server" Text="Add Commission" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgDealerCommission" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="CommissionHeaderID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CommissionHeaderID" ReadOnly="true" Visible="false" UniqueName="CommissionHeaderID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ProgramName" UniqueName="ProgramName" HeaderText="Program Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CommissionDesc" UniqueName="CommissionDesc" HeaderText="Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypename" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amount" DataFormatString="{0:###.00}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="StartDate" UniqueName="StartDate" HeaderText="Start Date" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EndDate" UniqueName="EndDate" HeaderText="End Date" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource1"
                            ProviderName="System.Data.SqlClient" 
                            SelectCommand="select CommissionHeaderID, CommissionDesc, RateTypeName, ProgramName, AgentNo, agentname, class, Amt, startdate, enddate 
                            from CommissionHeader ch
                                inner join RateType rt on ch.ratetypeid = rt.ratetypeid 
                                inner join Program P on p.programid = ch.programid
                                left join agents A on a.agentid = ch.agentid 
                            where dealerid = @dealerid " runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell  ID="tcGuard">
                            <telerik:RadGrid ID="rgGuard" runat="server" AutoGenerateColumns="false"
                                AllowSorting="true" AllowPaging="true"  DataSourceID="dsGuard">
                                <MasterTableView AutoGenerateColumns="false" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ProgramName" UniqueName="ProgramName" HeaderText="Program Name"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Class1" DataFormatString="{0:c}" HeaderText="1"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsGuard"
                            ProviderName="System.Data.SqlClient" 
                            SelectCommand="select programname from  Program P " 
                                runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlCommissionDetail" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Commission Desc:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCommDesc" Width="200" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Bucket:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtRateType" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnSearchRateType" runat="server" Text="Search Bucket" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Program:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadComboBox ID="cboProgram" DataSourceID="dsProgram" DataTextField="ProgramName" DataValueField="ProgramID" AutoPostBack="true" runat="server"></telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsProgram"
                                        ProviderName="System.Data.SqlClient" SelectCommand="select programid, programname from Program order by programid " runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Start Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpStartDate" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Agent:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAgent" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnAgent" runat="server" Text="Search Agent" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        &nbsp
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        &nbsp
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Amt
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtAmount" runat="server"></telerik:RadNumericTextBox>
                                        <asp:CheckBox ID="chkNoChargeBack" Text="No Charge Back" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        End Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpEndDate" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Create Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Create by:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Modify Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblModDate" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Modify By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblModBy" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trNCB">
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtNCB" TextMode="Number" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnDeleteCommission" runat="server" Text="Delete" />
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCommission" runat="server" Text="Save" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCommission" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlConfirmDelete" BorderStyle="Double">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Program:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblProgramConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Bucket:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblRateTypeConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Amount:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblAmtConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ForeColor="Red">
                            Do you wish to delete?
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnYesConfirm" runat="server" Text="Yes" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnNoConfirm" runat="server" Text="No" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddRateType" runat="server" Text="Add Bucket" BackColor="#1eabe2" BorderColor="#1eabe2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgRateType" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsRateType">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="RateTypeID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="RateTypeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CategoryName" UniqueName="RateCategory" HeaderText="Bucket Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsRateType"
                            ProviderName="System.Data.SqlClient" SelectCommand="select ratetypeid, ratetypeName, categoryname from ratetype rt inner join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid where ratetypeid <> 29 " 
                            runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnCancelRateType" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlAddRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateTypeAdd" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Bucket Category
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboRateCategoryAdd" DataSourceID="dsRateCategory" DataTextField="CategoryName" DataValueField="RateCategoryID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsRateCategory"
                            ProviderName="System.Data.SqlClient" SelectCommand="select RateCategoryid, categoryname from ratecategory order by categoryname " runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCategory" runat="server" Text="Save" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCategory" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchAgent" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgAgent" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsAgent">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="AgentID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="AgentID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DBA" UniqueName="DBA" HeaderText="DBA" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsAgent"
                            ProviderName="System.Data.SqlClient" SelectCommand="select agentid, agentno, agentname, DBA, city, state from agents" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnClearAgentInfo" runat="server" Text="Clear Agent Info" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchSubAgent" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgSubAgent" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsSubAgent">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="SubAgentID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="SubAgentID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SubAgentNo" UniqueName="SubAgentNo" HeaderText="Sub Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SubAgentName" UniqueName="SubAgentName" HeaderText="Sub Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsSubAgent"
                            ProviderName="System.Data.SqlClient" SelectCommand="select subagentid, subagentno, subagentname, city, state from subagents" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnClearSubAgentInfo" runat="server" Text="Clear Sub Agent Info" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfAgentID" runat="server" />
<asp:HiddenField ID="hfSubAgentID" runat="server" />
<asp:HiddenField ID="hfRateTypeID" runat="server" />
<asp:HiddenField ID="hfCommissionHeaderID" runat="server" />

