﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ClaimGapPayment

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnlError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlError As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlList As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnAddPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddPayment As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgClaimPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgClaimPayment As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsClaimPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsClaimPayment As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''btnHiddenCP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenCP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboPaymentType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPaymentType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dsPaymentType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsPaymentType As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtPayeeNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSeekPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSeekPayee As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtPayeeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayeeName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPaymentAuth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPaymentAuth As Global.Telerik.Web.UI.RadNumericTextBox

    '''<summary>
    '''rdpAuthDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdpAuthDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''rdpPaidDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdpPaidDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''txtBankLoad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBankLoad As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLast6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLast6 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnDetailClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDetailClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnDetailUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDetailUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSeekPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSeekPayee As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnAddClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddClaimPayee As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgClaimPayee As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsPayee As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''btnHiddenCP2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenCP2 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlAddClaimPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAddClaimPayee As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtGAPPayeeNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGAPPayeeNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtGapPayeeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGapPayeeName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddr1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddr1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddr2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddr2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboState As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''dsStates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsStates As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txtZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZip As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPhone As Global.Telerik.Web.UI.RadMaskedTextBox

    '''<summary>
    '''txtEMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEMail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnHiddenACP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenACP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnPayeeCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPayeeCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnPayeeSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPayeeSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfUserID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfClaimPaymentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfClaimPaymentID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfPayeeID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfPayeeID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfAuthDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfAuthDate As Global.System.Web.UI.WebControls.HiddenField
End Class
