﻿Imports System.Configuration.ConfigurationManager

Public Class Claim3C
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        GetServerInfo()
        If Not IsPostBack Then
            FillNotes()
            If CheckLock() Then
                btnUpdateNotes.Visible = False
            Else
                btnUpdateNotes.Visible = True
            End If
        End If

    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnUpdateNotes.Enabled = False
            End If
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillNotes()
        GetComplaint()
        GetCause()
        GetCorrect()
    End Sub

    Private Sub GetCorrect()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimnoteid, note from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 2 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfCorrectNoteID.Value = clR.Fields("claimnoteid")
            txtCorrect.Text = clR.Fields("note")
        End If
    End Sub

    Private Sub GetCause()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimnoteid, note from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 3 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfCauseNoteID.Value = clR.Fields("claimnoteid")
            txtCause.Text = clR.Fields("note")
        End If
    End Sub

    Private Sub GetComplaint()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimnoteid, note from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 1 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfComplaintNoteID.Value = clR.Fields("claimnoteid")
            txtComplaint.Text = clR.Fields("note")
        End If
    End Sub

    Private Sub btnUpdateNotes_Click(sender As Object, e As EventArgs) Handles btnUpdateNotes.Click
        SaveComplaint()
        SaveCause()
        SaveCorrect()
        FillNotes()
    End Sub

    Private Sub SaveCorrect()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfCorrectNoteID.Value.Length = 0 Then
            hfCorrectNoteID.Value = 0
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = " & hfCorrectNoteID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 2
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
        Else
            clR.GetRow()
        End If
        clR.Fields("note") = txtCorrect.Text
        clR.Fields("moddate") = Today
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("scsclaimnoteid") = 0
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub SaveCause()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfCauseNoteID.Value.Length = 0 Then
            hfCauseNoteID.Value = 0
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = " & hfCauseNoteID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 3
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
        Else
            clR.GetRow()
        End If
        clR.Fields("note") = txtCause.Text
        clR.Fields("moddate") = Today
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("scsclaimnoteid") = 0
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()

    End Sub

    Private Sub SaveComplaint()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfComplaintNoteID.Value.Length = 0 Then
            hfComplaintNoteID.Value = 0
        End If

        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = " & hfComplaintNoteID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 1
            clR.Fields("credate") = Today
            clR.Fields("creby") = hfUserID.Value
        Else
            clR.GetRow()
        End If
        clR.Fields("note") = txtComplaint.Text
        clR.Fields("moddate") = Today
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("scsclaimnoteid") = 0
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

End Class