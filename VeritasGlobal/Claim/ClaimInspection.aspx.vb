﻿Imports System.Configuration.ConfigurationManager
Imports DocumentFormat.OpenXml.ExtendedProperties
Imports DocumentFormat.OpenXml.Office2013.Excel
Imports Telerik.Web.UI

Public Class ClaimInspection1
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")

        If Not IsPostBack Then
            GetServerInfo()
            pnlDetail.Visible = False
            pnlList.Visible = True
            FillGrid()
            If CheckLock() Then
                btnSave.Visible = False
                btnAddInspection.Visible = False
            Else
                btnSave.Visible = True
                btnAddInspection.Visible = True
            End If
            ReadOnlyButtons()
            lblError2.Visible = False
            pnlTasaEmail.Visible = False
            pnlTASAConfirm.Visible = False
            pnlANReason.Visible = False
            checkview
        End If

    End Sub

    Private Sub CheckView()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claiminspection "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and viewdate is null "
        SQL = SQL + "and not resulturl is null "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            SQL = "update claiminspection "
            SQL = SQL + "set viewdate = '" & Today & "', "
            SQL = SQL + "viewby = " & hfUserID.Value & " "
            SQL = SQL + "where claimid = " & hfClaimID.Value & " "
            SQL = SQL + "and viewdate is null "
            SQL = SQL + "and not resulturl is null "
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnSave.Enabled = False
                btnAddInspection.Enabled = False
            End If
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (lockuserid = " & hfUserID.Value & " "
        SQL = SQL + "or lockuserid is null) "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = True
        Else
            CheckLock = False
        End If
    End Function

    Private Sub FillGrid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claiminspectionid, inspectno, inspectnote, fname + ' ' + lname as RequestBy, requestdate, resulturl, imagesurl, detailsurl "
        SQL = SQL + "from claiminspection ci "
        SQL = SQL + "left join userinfo ui on ci.requestby = ui.userid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        rgClaimInspect.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgClaimInspect.Rebind()
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub LockButtons()
        btnAddCarFax.Enabled = False
        btnAddInspection.Enabled = False
        btnSave.Enabled = False
    End Sub

    Private Sub UnlockButtons()
        btnAddInspection.Enabled = True
        btnAddCarFax.Enabled = True
        btnSave.Enabled = True
    End Sub

    Private Sub btnAddInspection_Click(sender As Object, e As EventArgs) Handles btnAddInspection.Click
        If hfUserID.Value <> "17" And hfUserID.Value <> "18" And hfUserID.Value <> "19" And hfUserID.Value <> "20" And hfUserID.Value <> "12" And hfUserID.Value <> "1241" And hfUserID.Value <> "1227" Then
            If CheckANDealer() And CheckANRF() And CheckAgeUnder() Then
                pnlTASAConfirm.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If
            If CheckANRF() And CheckClaimAmt() And CheckAge() Then
                pnlANReason.Visible = True
                pnlList.Visible = False
                txtInspectReasonNote.Content = ""
                Exit Sub
            End If
            If CheckANDealer() And CheckANRF() And CheckClaimAmtOver() Then
                pnlTASAConfirm.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If
        End If

        pnlDetail.Visible = True
        pnlList.Visible = False
        btnSave.Visible = True
        hfAdd.Value = "True"
        FillInspectType()
        lblError.Visible = False
        GetInspectNo()
        trInspectResult.Visible = False
    End Sub

    Private Function CheckClaimAmtOver()
        CheckClaimAmtOver = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(totalamt) as AMT from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'Authorized' "
        SQL = SQL + "or claimdetailstatus = 'Approved' "
        SQL = SQL + "or claimdetailstatus = 'Paid') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If clR.Fields("amt") > 1500 Then
                    CheckClaimAmtOver = True
                End If
            End If
        End If

    End Function

    Private Function CheckAgeUnder() As Boolean
        CheckAgeUnder = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lDays As Long
        SQL = "select credate from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lDays = DateDiff(DateInterval.Day, CDate(clR.Fields("credate")), Today)
            If lDays < 60 Then
                CheckAgeUnder = True
            End If
        End If
    End Function

    Private Function CheckAge() As Boolean
        CheckAge = False
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lDays As Long
        SQL = "select credate from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lDays = DateDiff(DateInterval.Day, CDate(clR.Fields("credate")), Today)
            If lDays > 60 Then
                CheckAge = True
            End If
        End If
    End Function

    Private Function CheckClaimAmt() As Boolean
        CheckClaimAmt = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(totalamt) as AMT from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'Authorized' "
        SQL = SQL + "or claimdetailstatus = 'Approved' "
        SQL = SQL + "or claimdetailstatus = 'Paid') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                If clR.Fields("amt") < 1500 Then
                    CheckClaimAmt = True
                End If
            End If
        End If
    End Function

    Private Function CheckANRF() As Boolean
        CheckANRF = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        SQL = SQL + "and servicecenterid in ("
        SQL = SQL + "select servicecenterid from servicecenter "
        SQL = SQL + "where dealerno like '2%') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckANRF = True
        End If
    End Function

    Private Function CheckANDealer() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckANDealer = False
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        SQL = SQL + "and contractid in ("
        SQL = SQL + "select contractid from contract where dealerid in ("
        SQL = SQL + "select dealerid from dealer where dealerno like '2%')) "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckANDealer = True
        End If
    End Function

    Private Sub GetInspectNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select count(*) as cnt from claiminspection "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtInspectNo.Text = CLng(clR.Fields("cnt")) + 1
        Else
            txtInspectNo.Text = 1
        End If

    End Sub

    Private Sub FillInspectType()
        cboInspectType.Items.Add("")
        cboInspectType.Items.Add("Inspection Only")
        cboInspectType.Items.Add("Inspection and Oil")
        cboInspectType.Items.Add("No Inspection")
        cboInspectType.Items.Add("Oil Only")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim lInspectTry As Long
        lblError.Visible = False
        UpdateClaimInspect()
        UpdateClaimDetail()
        UpdateClaim()
        lInspectTry = 0
        If hfAdd.Value = "True" Then
            If Not chkTruePic.Checked Then
                If cboInspectType.Text <> "" Then
                    If cboInspectType.Text <> "No Inspect" Then
                        If Not AppSettings("connstring").ToLower.Contains("test") Then
MoveHere:
                            lInspectTry = lInspectTry + 1
                            Dim p As New ProcessStartInfo
                            p.FileName = "C:\ProcessProgram\WIS\WISInspect.exe"
                            p.Arguments = "-claimid " & hfClaimID.Value & " -inspectno " & txtInspectNo.Text & " -inspecttype Inspection"
                            p.WindowStyle = ProcessWindowStyle.Hidden
                            Dim pr As New Process
                            pr.StartInfo = p
                            pr.Start()
                            pr.WaitForExit()

                            'Process.Start("C:\ProcessProgram\WIS\WISInspect.exe", "-claimid " & hfClaimID.Value & " -inspectno " & txtInspectNo.Text & " -inspecttype Inspection")
                            If Not CheckInspect() Then
                                If lInspectTry = 3 Then
                                    placeError()
                                    Exit Sub
                                End If
                                GoTo MoveHere
                            End If
                        End If
                    End If
                End If
            End If
        End If
        FillGrid()
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub placeError()
        lblError.Visible = True
        lblError.Text = "Could not transmit to WIS for Inspection. Please try again."
    End Sub


    Private Function CheckInspect() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckInspect = False
        SQL = "select * from claiminspection "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and inspectno = " & txtInspectNo.Text & " "
        SQL = SQL + "and truepic = 0 "
        SQL = SQL + "and not inspectionid is null "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckInspect = True
        End If
    End Function

    Private Sub UpdateClaimDetail()
        Dim SQL As String
        Dim clCD As New clsDBO
        Dim sAmt As Double = 0
        If cboInspectType.Text = "No Inspection" Then
            Exit Sub
        End If
        If cboInspectType.Text = "Inspection Only" Then
            sAmt = 86
        End If
        If cboInspectType.Text = "Inspection and Oil" Then
            sAmt = 136
        End If
        If cboInspectType.Text = "Oil Only" Then
            sAmt = 50
        End If
        If chkTruePic.Checked Then
            sAmt = 25
        End If
        If sAmt > 0 Then
            SQL = "select * from claimdetail "
            SQL = SQL + "where jobno = 'A02' "
            SQL = SQL + "and qty = " & txtInspectNo.Text & " "
            SQL = SQL + "and claimid = " & hfClaimID.Value
            clCD.OpenDB(SQL, AppSettings("connstring"))
            If clCD.RowCount > 0 Then
                clCD.GetRow()
            Else
                clCD.NewRow()
            End If
            clCD.Fields("claimid") = hfClaimID.Value
            clCD.Fields("ClaimDetailType") = "Inspect"
            clCD.Fields("jobno") = "A02"
            clCD.Fields("qty") = txtInspectNo.Text
            clCD.Fields("ratetypeid") = 1
            clCD.Fields("reqqty") = 1
            clCD.Fields("reqamt") = sAmt
            clCD.Fields("reqcost") = sAmt
            clCD.Fields("LossCode") = "IFPend"
            clCD.Fields("AuthAmt") = 0
            clCD.Fields("taxamt") = 0
            clCD.Fields("paidamt") = 0
            clCD.Fields("claimdetailstatus") = "Requested"
            If chkTruePic.Checked Then
                clCD.Fields("claimpayeeid") = 10459
            Else
                clCD.Fields("claimpayeeid") = 5771
            End If
            If clCD.Fields("authamt").Length > 0 Then
                If clCD.Fields("authamt") > 0 Then
                    clCD.Fields("authamt") = sAmt
                End If
            End If
            clCD.Fields("moddate") = Today
            clCD.Fields("modby") = hfUserID.Value
            If clCD.RowCount = 0 Then
                clCD.Fields("credate") = Today
                clCD.Fields("creby") = hfUserID.Value
                clCD.AddRow()
            End If
            clCD.SaveDB()
        End If
    End Sub

    'Private Sub CheckCarFax()
    '    Dim SQL As String
    '    Dim clR As New clsDBO
    '    SQL = "select * From claimdetail "
    '    SQL = SQL + "where claimid = " & hfClaimID.Value & " "
    '    SQL = SQL + "and jobno = 'A03' "
    '    clR.OpenDB(SQL, AppSettings("connstring"))
    '    If clR.RowCount > 0 Then
    '        chkCarfax.Checked = True
    '    End If
    'End Sub

    Private Sub UpdateClaimInspect()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim sInspectType As String = ""
        If cboInspectType.Text = "" Then
            lblError.Text = "Please select an Inspection Type."
            lblError.Visible = True
            Exit Sub
        End If
        If cboInspectType.Text = "Inspection Only" Then
            sInspectType = "Inspection"
        End If
        If cboInspectType.Text = "Inspection and Oil" Then
            sInspectType = "InspectionOil"
        End If
        If cboInspectType.Text = "Oil Only" Then
            sInspectType = "Oil"
        End If
        If cboInspectType.Text = "No Inspection" Then
            sInspectType = "NoInspect"
        End If
        SQL = "select * from claiminspection "
        SQL = SQL + "where inspectno = " & txtInspectNo.Text & " "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("truepic") = chkTruePic.Checked
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("inspectno") = txtInspectNo.Text
        clR.Fields("InspectType") = sInspectType
        clR.Fields("InspectNote") = txtInspectNote.Text
        clR.Fields("resulturl") = txtInspectResult.Text
        clR.Fields("truepic") = chkTruePic.Checked
        If clR.RowCount = 0 Then
            clR.Fields("requestby") = hfUserID.Value
            clR.Fields("requestdate") = Now
            clR.AddRow()
        End If
        clR.SaveDB()

    End Sub

    Private Sub UpdateClaim()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set inspect = 1 "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clC.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnAddNoInspect_Click(sender As Object, e As EventArgs) Handles btnAddNoInspect.Click
        hfAdd.Value = "False"
        pnlDetail.Visible = True
        pnlList.Visible = False
        btnSave.Visible = True
        FillInspectType2()
        lblError.Visible = False
        txtInspectNo.Text = 0
        trInspectResult.Visible = False
    End Sub

    Private Sub FillInspectType2()
        cboInspectType.Items.Clear()
        cboInspectType.Items.Add("No Inspection")
    End Sub

    Private Sub btnAddCarFax_Click(sender As Object, e As EventArgs) Handles btnAddCarFax.Click
        hfAdd.Value = "False"
        AddClaimDetail()
        AddInspection()
        FillGrid()
    End Sub

    Private Sub AddInspection()
        Dim SQL As String
        Dim clCI As New clsDBO
        SQL = "select * from claiminspection "
        SQL = SQL + "where claiminspectionid = 0 "
        clCI.OpenDB(SQL, AppSettings("connstring"))
        If clCI.RowCount = 0 Then
            clCI.NewRow()
            clCI.Fields("claimid") = hfClaimID.Value
            clCI.Fields("inspectno") = 0
            clCI.Fields("inspectionid") = 0
            clCI.Fields("inspectnote") = "CarFax"
            clCI.Fields("requestby") = hfUserID.Value
            clCI.Fields("requestdate") = Today
            clCI.AddRow()
            clCI.SaveDB()
        End If
    End Sub

    Private Sub AddClaimDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim sContractNo As String
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = 'a03' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimdetailtype") = "Other"
            clR.Fields("JobNo") = "A03"
            clR.Fields("ratetypeid") = 1
            clR.Fields("qty") = 1
            clR.Fields("cost") = 13
            clR.Fields("losscode") = "ZCARFAX"
            clR.Fields("reqamt") = 13
            clR.Fields("authamt") = 13
            clR.Fields("taxamt") = 0
            clR.Fields("totalamt") = 13
            clR.Fields("dateauth") = Now
            clR.Fields("authby") = hfUserID.Value
            clR.Fields("credate") = Now
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("moddate") = Now
            clR.Fields("modby") = hfUserID.Value
            clR.Fields("claimpayeeid") = 4882
            clR.Fields("taxamt") = 0
            clR.Fields("paidamt") = 0
            clR.Fields("claimdetailstatus") = "Authorized"
            clR.AddRow()
            clR.SaveDB()
            SQL = "select * from usermessage "
            SQL = SQL + "where idx = 0 "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                sContractNo = GetContractNo()
                clR.NewRow()
                clR.Fields("toid") = hfUserID.Value
                clR.Fields("fromid") = 1187
                clR.Fields("header") = "Carfax - " & sContractNo
                clR.Fields("message") = "Attach carfax to the contract documents for " & sContractNo
                clR.Fields("messagedate") = Now
                clR.AddRow()
                clR.SaveDB()
            End If
        End If
    End Sub

    Private Sub rgClaimInspect_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimInspect.SelectedIndexChanged
        hfAdd.Value = "False"
        pnlDetail.Visible = True
        pnlList.Visible = False
        btnSave.Visible = True
        FillDetail()
    End Sub

    Private Sub FillDetail()
        FillInspectType()
        Dim SQL As String
        Dim clCI As New clsDBO
        SQL = "select * from claiminspection "
        SQL = SQL + "where claiminspectionid = " & rgClaimInspect.SelectedValue
        clCI.OpenDB(SQL, AppSettings("connstring"))
        If clCI.RowCount > 0 Then
            clCI.GetRow()
            If clCI.Fields("inspecttype") = "NoInspect" Then
                cboInspectType.Text = "No Inspection"
            End If
            If clCI.Fields("inspecttype") = "Inspection" Then
                cboInspectType.Text = "Inspection Only"
            End If
            txtInspectNo.Text = clCI.Fields("inspectno")
            txtInspectNote.Text = clCI.Fields("inspectnote")
            txtInspectResult.Text = clCI.Fields("resulturl")
            chkTruePic.Checked = clCI.Fields("truepic")
        End If
    End Sub


    Private Function GetContractNo() As String
        GetContractNo = ""
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select contractno from contract c "
        SQL = SQL + "inner join claim cl on cl.contractid = c.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            GetContractNo = clC.Fields("contractno")
        End If
    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnReasonNoteSave_Click(sender As Object, e As EventArgs) Handles btnReasonNoteSave.Click
        Dim clR As New clsDBO
        Dim SQL As String
        If txtInspectReasonNote.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 5
            clR.Fields("note") = txtInspectReasonNote.Content
            clR.Fields("NoteText") = txtInspectReasonNote.Text
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("credate") = Today
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
            clR.AddRow()
            clR.SaveDB()
        End If
        pnlDetail.Visible = True
        pnlList.Visible = False
        pnlANReason.Visible = False
        btnSave.Visible = True
        hfAdd.Value = "True"
        FillInspectType()
        GetInspectNo()
        trInspectResult.Visible = False
        lblError2.Visible = False
    End Sub

    Private Sub btnTASANo_Click(sender As Object, e As EventArgs) Handles btnTASANo.Click
        pnlTASAConfirm.Visible = False
        pnlList.Visible = True
        lblError2.Text = "Management Override required."
        lblError2.Visible = True
    End Sub

    Private Sub btnTASAYes_Click(sender As Object, e As EventArgs) Handles btnTASAYes.Click
        lblError2.Visible = False
        pnlTasaEmail.Visible = True
        txtNote.Content = ""
        pnlTASAConfirm.Visible = False
    End Sub

    Private Sub btnSaveNote_Click(sender As Object, e As EventArgs) Handles btnSaveNote.Click
        Dim clR As New clsDBO
        Dim SQL As String
        If txtNote.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimnoteid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("claimnotetypeid") = 5
            clR.Fields("note") = txtNote.Content
            clR.Fields("NoteText") = txtNote.Text
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("credate") = Today
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
            clR.AddRow()
            clR.SaveDB()
        End If
        pnlDetail.Visible = True
        pnlList.Visible = False
        pnlTasaEmail.Visible = False
        btnSave.Visible = True
        hfAdd.Value = "True"
        FillInspectType()
        GetInspectNo()
        trInspectResult.Visible = False
    End Sub

    Private Sub btnCancelNote_Click(sender As Object, e As EventArgs) Handles btnCancelNote.Click
        pnlDetail.Visible = True
        pnlList.Visible = False
        pnlTasaEmail.Visible = False
        btnSave.Visible = True
        hfAdd.Value = "True"
        FillInspectType()
        GetInspectNo()
        trInspectResult.Visible = False
    End Sub

End Class