﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI.Calendar
Imports System.Security.Claims
Imports System.EnterpriseServices.Internal
Imports DocumentFormat.OpenXml.Drawing
Imports System.Drawing
Imports Telerik.Windows.Documents.Flow.FormatProviders.Rtf.Exceptions
Imports AjaxControlToolkit
Imports DocumentFormat.OpenXml.Presentation

Public Class Claim1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsActivity.ConnectionString = AppSettings("connstring")
        dsServiceCenter.ConnectionString = AppSettings("connstring")
        dsClaimStatus.ConnectionString = AppSettings("connstring")
        dsContract.ConnectionString = AppSettings("connstring")
        dsStates.ConnectionString = AppSettings("connstring")
        dsLossCode.ConnectionString = AppSettings("connstring")
        dsAssignedTo.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            btnChangeAssignedTo.Visible = False
            GetServerInfo()
            tsClaim.Tabs(5).Visible = False
            txtAssignedTo.Enabled = False
            pvSurcharges.Selected = True
            'pvDealer.Selected = True
            hfClaimID.Value = Request.QueryString("claimid")
            pvNotes.ContentUrl = "~/claim/ClaimNotes.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pv3C.ContentUrl = "~/claim/Claim3C.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            'pvDealer.ContentUrl = "~/claim/ClaimDealer.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvDocuments.ContentUrl = "~/claim/ClaimDocument.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvInspection.ContentUrl = "~/claim/ClaimInspection.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvParts.ContentUrl = "~/claim/ClaimPart.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvJob.ContentUrl = "~/claim/ClaimJobs.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvPayment.ContentUrl = "~/claim/ClaimPayment.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvAlert.ContentUrl = "~/claim/ClaimAlert.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvHistory.ContentUrl = "~/claim/ClaimHistory.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvHCC.ContentUrl = "~/claim/ClaimHCC.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvSurcharges.ContentUrl = "~/claim/ClaimContractSurcharge.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvContractNote.ContentUrl = "~/claim/ContractNote.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value
            If hfUserID.Value = 0 Then
                Response.Redirect("~/Default.aspx")
            End If
            If hfClaimID.Value = "" Then
                hfClaimID.Value = "0"
            End If
            If hfClaimID.Value = "0" Then
                ClearScreen()
            Else
                FillScreen()
            End If
            pnlSeekSC.Visible = False
            pnlSeekContract.Visible = False
            pnlContractConfirm.Visible = False
            pnlSeekLossCode.Visible = False
            pnlLossCode.Visible = False
            pnlAddSC.Visible = False
            pnlAssignedToChange.Visible = False
            tbLock.Visible = False
            If Not CheckLock() Then
                hfClaimLocked.Value = False
                LockClaim()
            Else
                ShowLockedClaim()
            End If
            CheckNewNote()
            CheckAllNote()

            ReadOnlyButtons()
            GetHasSlush()
            InsertAccess()
        End If
        If hfDenied.Value = "Visible" Then
            ShowDenied()
        Else
            HideDenied()
        End If

        If hfOpen.Value = "Visible" Then
            ShowOpen()
        Else
            HideOpen()
        End If
        If hfAlert.Value = "Visible" Then
            ShowAlert()
            hfAlert.Value = ""
        Else
            HideAlert()
        End If
        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        End If

    End Sub

    'Private Sub CheckRFClaimSubmit()
    '    Dim SQL As String
    '    Dim clR As New clsDBO
    '    SQL = "select * from usersecurityinfo "
    '    SQL = SQL + "where userid = " & hfUserID.Value & " "
    '    SQL = SQL + "and teamlead <> 0 "
    '    clR.OpenDB(SQL, AppSettings("connstring"))
    '    If clR.RowCount > 0 Then
    '        SQL = "select * from veritasclaims.dbo.claim "
    '        SQL = SQL + "where not sendclaim is null "
    '        SQL = SQL + "and processclaimdate is null "
    '        clR.OpenDB(SQL, AppSettings("connstring"))
    '        If clR.RowCount > 0 Then
    '            btnRFClaimSubmit.Enabled = True
    '        End If
    '    End If
    'End Sub

    Private Sub InsertAccess()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into claimaccess (claimid, userid, accessdate) "
        SQL = SQL + "values ("
        SQL = SQL + hfClaimID.Value & ", "
        SQL = SQL + hfUserID.Value & ", "
        SQL = SQL + "'" & Now & "') "
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub

    Private Sub GetHasSlush()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and ratetypeid > 1 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            chkHasSlush.Checked = True
        End If

    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                hfReadOnly.Visible = "True"
                btnAddSC.Enabled = False
                btnChangeAssignedTo.Enabled = False
                btnSeekContract.Enabled = False
                btnSeekLossCode.Enabled = False
                btnServiceCenters.Enabled = False
                btnCalcLabor.Enabled = False
                btnCalcPart.Enabled = False
            End If
        End If

    End Sub

    Private Sub CheckNewNote()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clA As New clsDBO
        SQL = "Select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("addnew")) Then
                If clR.Fields("contractid") > 0 Then
                    SQL = "Select * from dealernote dn "
                    SQL = SQL + "inner join contract c On c.dealerid = dn.dealerid "
                    SQL = SQL + "where contractid = " & clR.Fields("contractid") & " "
                    SQL = SQL + "And dn.contractnewentry <> 0 "
                    txtAlertPopup.Text = ""
                    clA.OpenDB(SQL, AppSettings("connstring"))
                    If clA.RowCount > 0 Then
                        tsClaim.Tabs(5).Visible = True
                        tsClaim.Tabs(0).Selected = True
                        pvCustomer.Selected = True
                        txtAlertPopup.Text = ""
                        For cnt = 0 To clA.RowCount - 1
                            clA.GetRowNo(cnt)
                            'pvDealer.Selected = True
                            txtAlertPopup.Text = txtAlertPopup.Text & clA.Fields("note") & vbCrLf & vbCrLf
                        Next
                    End If
                End If
            End If
        End If

        SQL = "Select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("addnew")) Then
                If clR.Fields("contractid") > 0 Then
                    SQL = "Select * from agentsnote an "
                    SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid "
                    SQL = SQL + "inner join contract c On c.dealerid = d.dealerid "
                    SQL = SQL + "where contractid = " & clR.Fields("contractid") & " "
                    SQL = SQL + "And an.claimnewentry <> 0 "
                    txtAlertPopup.Text = ""
                    clA.OpenDB(SQL, AppSettings("connstring"))
                    If clA.RowCount > 0 Then
                        tsClaim.Tabs(5).Visible = True
                        tsClaim.Tabs(0).Selected = True
                        pvCustomer.Selected = True
                        txtAlertPopup.Text = ""
                        For cnt = 0 To clA.RowCount - 1
                            clA.GetRowNo(cnt)
                            'pvDealer.Selected = True
                            txtAlertPopup.Text = txtAlertPopup.Text & clA.Fields("note") & vbCrLf & vbCrLf
                        Next
                    End If
                End If
            End If
        End If

        SQL = "Select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("addnew")) Then
                If clR.Fields("contractid") > 0 Then
                    SQL = "Select * from subagentnote an "
                    SQL = SQL + "inner join Dealer d On d.subagentid = an.subagentid "
                    SQL = SQL + "inner join contract c On c.dealerid = d.dealerid "
                    SQL = SQL + "where contractid = " & clR.Fields("contractid") & " "
                    SQL = SQL + "And an.claimnewentry <> 0 "
                    txtAlertPopup.Text = ""
                    clA.OpenDB(SQL, AppSettings("connstring"))
                    If clA.RowCount > 0 Then
                        tsClaim.Tabs(5).Visible = True
                        tsClaim.Tabs(0).Selected = True
                        pvCustomer.Selected = True
                        txtAlertPopup.Text = ""
                        For cnt = 0 To clA.RowCount - 1
                            clA.GetRowNo(cnt)
                            'pvDealer.Selected = True
                            txtAlertPopup.Text = txtAlertPopup.Text & clA.Fields("note") & vbCrLf & vbCrLf
                        Next
                    End If
                End If
            End If
        End If

        If txtAlertPopup.Text.Length > 0 Then
            hfAlert.Value = "Visible"
        End If
    End Sub

    Private Sub CheckAllNote()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "Select * from dealernote dn "
        SQL = SQL + "inner join contract c On c.dealerid = dn.dealerid "
        SQL = SQL + "inner join claim cl On c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And dn.claimalways <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            tsClaim.Tabs(5).Visible = True
            tsClaim.Tabs(0).Selected = True
            txtAlertPopup.Text = ""
            pvCustomer.Selected = True
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                txtAlertPopup.Text = txtAlertPopup.Text & clR.Fields("note") & vbCrLf & vbCrLf
            Next
            'pvDealer.Selected = True
        End If

        SQL = "Select * from agentsnote an "
        SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid "
        SQL = SQL + "inner join contract c On d.dealerid = c.dealerid "
        SQL = SQL + "inner join claim cl On c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And an.claimalways <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            tsClaim.Tabs(5).Visible = True
            tsClaim.Tabs(0).Selected = True
            txtAlertPopup.Text = ""
            pvCustomer.Selected = True
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                txtAlertPopup.Text = txtAlertPopup.Text & clR.Fields("note") & vbCrLf & vbCrLf
            Next
            'pvDealer.Selected = True
        End If
        If txtAlertPopup.Text.Length > 0 Then
            hfAlert.Value = "Visible"
        End If

        SQL = "Select * from subagentnote an "
        SQL = SQL + "inner join Dealer d On d.subagentid = an.subagentid "
        SQL = SQL + "inner join contract c On d.dealerid = c.dealerid "
        SQL = SQL + "inner join claim cl On c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And an.claimalways <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            tsClaim.Tabs(5).Visible = True
            tsClaim.Tabs(0).Selected = True
            txtAlertPopup.Text = ""
            pvCustomer.Selected = True
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                txtAlertPopup.Text = txtAlertPopup.Text & clR.Fields("note") & vbCrLf & vbCrLf
            Next
            'pvDealer.Selected = True
        End If
        If txtAlertPopup.Text.Length > 0 Then
            hfAlert.Value = "Visible"
        End If

        SQL = "select * from contractnote cn "
        SQL = SQL + "inner join claim cl On cn.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and cn.claimalways <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                txtAlertPopup.Text = txtAlertPopup.Text & clR.Fields("note") & vbCrLf & vbCrLf
            Next
        End If

    End Sub

    Private Sub ShowLockedClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "Select userid, fname, lname from userinfo ui "
        SQL = SQL + "inner join claim cl On ui.userid = cl.lockuserid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("userid") <> hfUserID.Value Then
                hfClaimLocked.Value = True
                tbLock.Visible = True
                lblLocked.Text = "Claim Is locked by " & clR.Fields("fname") & " " & clR.Fields("lname")
            End If
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub LockClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "Set lockuserid = " & hfUserID.Value & ", "
        SQL = SQL + "lockdate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub ClearScreen()
        GetClaimNo()
        Response.Redirect("claim.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value)
    End Sub

    Private Sub GetClaimNo()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim sClaimNo As String
        Dim lClaimNo As Long = 0
MoveHere:
        sClaimNo = ""
        SQL = "select max(claimno) as claimno from claim "
        SQL = SQL + "where claimno like 'C1%' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            sClaimNo = clC.Fields("claimno")
            sClaimNo = sClaimNo.Replace("C1", "")
            If sClaimNo.Length = 0 Then
                sClaimNo = "100000000"
            End If
            lClaimNo = sClaimNo
            lClaimNo = lClaimNo + 1
            sClaimNo = Format(lClaimNo, "100000000")
            sClaimNo = "C" & sClaimNo
        End If
        If sClaimNo.Length > 0 Then
            SQL = "select * from claim "
            SQL = SQL + "where claimno = '" & sClaimNo & "' "
            clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clC.RowCount > 0 Then
                GoTo MoveHere
            Else
                clC.NewRow()
                clC.Fields("claimno") = sClaimNo
                clC.Fields("status") = "Open"
                If hfContractID.Value.Length > 0 Then
                    clC.Fields("contractid") = hfContractID.Value

                Else
                    clC.Fields("contractid") = 0
                End If
                clC.Fields("lossdate") = Today
                clC.Fields("claimactivityid") = 1
                clC.Fields("creby") = hfUserID.Value
                clC.Fields("assignedto") = hfUserID.Value
                clC.Fields("Modby") = hfUserID.Value
                clC.Fields("credate") = Today
                clC.Fields("moddate") = Today
                clC.Fields("addnew") = True
                clC.AddRow()
                clC.SaveDB()
            End If
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimno = '" & sClaimNo & "' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtClaimNo.Text = clC.Fields("claimno")
            hfClaimID.Value = clC.Fields("claimid")
        End If
        CheckClaimDuplicate()
        SQL = "insert into claimopenhistory "
        SQL = SQL + "(claimid, opendate, openby) "
        SQL = SQL + "values (" & hfClaimID.Value & ",'"
        SQL = SQL + Today & "',"
        SQL = SQL + hfUserID.Value & ")"
        clC.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub GetServerInfo()
        tcLR1.Visible = False
        tcLR2.Visible = False
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
            CheckAssignedToChange()
            If hfUserID.Value = "12" Or hfUserID.Value = "5" Or hfUserID.Value = "17" Or hfUserID.Value = "18" Or hfUserID.Value = "1" Then
                tcLR2.Visible = True
                tcLR1.Visible = True
            End If

        End If
    End Sub

    Private Sub CheckAssignedToChange()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("username").ToLower = "srhanna" Then
                btnChangeAssignedTo.Visible = True
            End If
            If clR.Fields("username").ToLower = "jfager" Then
                btnChangeAssignedTo.Visible = True
            End If
            If clR.Fields("username").ToLower = "kfulton" Then
                btnChangeAssignedTo.Visible = True
            End If
            If clR.Fields("username").ToLower = "zpeters" Then
                btnChangeAssignedTo.Visible = True
            End If
            If clR.Fields("username").ToLower = "jsteenbergen" Then
                btnChangeAssignedTo.Visible = True
            End If
            If clR.Fields("username").ToLower = "dhanley" Then
                btnChangeAssignedTo.Visible = True
            End If

        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                tsClaim.Tabs(10).Enabled = False
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub ShowDenied()
        hfDenied.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwDenied.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub ShowOpen()
        hfOpen.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwOpen.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwOpen", script, True)
        rwOpen.Visible = True
    End Sub

    Private Sub ShowAlert()
        hfAlert.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwAlert.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub HideDenied()
        hfDenied.Value = ""
        Dim script As String = "function f(){$find(""" + rwDenied.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)

    End Sub

    Private Sub HideOpen()
        hfOpen.Value = ""
        Dim script As String = "function f(){$find(""" + rwOpen.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
        rwOpen.Visible = False
    End Sub

    Private Sub HideAlert()
        hfAlert.Value = ""
        Dim script As String = "function f(){$find(""" + rwAlert.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        ClearLock()
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub FillScreen()
        Dim clCL As New clsDBO
        Dim SQL As String
        SQL = "select * from claim cl "
        SQL = SQL + "left join contract c on cl.contractid = c.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            txtClaimNo.Text = clCL.Fields("claimno")
            cboClaimStatus.SelectedValue = clCL.Fields("status")
            tsClaim.SelectedIndex = 0
            If clCL.Fields("claimactivityid") = "0" Then
                cboActivity.SelectedValue = 1
            Else
                cboActivity.SelectedValue = clCL.Fields("claimactivityid")
            End If
            txtLossDate.Text = clCL.Fields("lossdate")
            If clCL.Fields("saledate").Length > 0 Then
                If clCL.Fields("lossdate").Length > 0 Then
                    txtDaysSale.Text = DateDiff(DateInterval.Day, CDate(clCL.Fields("saledate")), CDate(clCL.Fields("lossdate")))
                Else
                    txtDaysSale.Text = 0
                End If
            Else
                txtDaysSale.Text = 0
            End If
            PlaceServiceCenter(clCL.Fields("servicecenterid"))
            txtServiceCenterContact.Text = clCL.Fields("sccontactinfo")
            hfServiceCenterID.Value = clCL.Fields("servicecenterid")
            txtShopRate.Text = clCL.Fields("shoprate")
            If clCL.Fields("rodate").Length > 0 Then
                rdpRODate.SelectedDate = CDate(clCL.Fields("rodate"))
            End If
            txtRONumber.Text = clCL.Fields("ronumber")
            If clCL.Fields("clipid") = "" Then
                If clCL.Fields("contractno").Length > 0 Then
                    If clCL.Fields("contractno").Substring(0, 2) = "VA" Then
                        txtCompanyInfo.Text = "0000976 Veritas GPS"
                    End If
                End If
                GoTo MoveHere
            End If
            If clCL.Fields("clipid") = 1 Or clCL.Fields("clipid") = 2 Then
                txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
            End If
            If clCL.Fields("clipid") = 0 Then
                If clCL.Fields("contractno").Substring(0, 2) = "VA" Then
                    txtCompanyInfo.Text = "0000976 Veritas GPS"
                End If
            End If
            If clCL.Fields("clipid") = 3 Or clCL.Fields("clipid") = 4 Or clCL.Fields("clipid") = 11 Then
                If clCL.Fields("contractno").Substring(0, 3) = "CHJ" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "DRV" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAC" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAD" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAN" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RDI" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "REP" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSA" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSD" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSW" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "VEL" Then
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
                Else
                    txtCompanyInfo.Text = "0000976 Veritas GPS"
                End If
            End If
            If clCL.Fields("inscarrierid") = 4 Then
                If isFL Then
                    txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL"
                Else
                    txtCompanyInfo.Text = "0000976 Veritas GPS"
                End If
            End If
            If clCL.Fields("inscarrierid") = 5 Then
                If isFL() Then
                    txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL"
                Else
                    txtCompanyInfo.Text = "0000976 Veritas GPS"
                End If
            End If

            If clCL.Fields("clipid") = 5 Or clCL.Fields("clipid") = 6 Or clCL.Fields("clipid") = 14 Then
                txtCompanyInfo.Text = "0000976 Veritas GPS"
            End If
            If clCL.Fields("clipid") = 7 Or clCL.Fields("clipid") = 8 Or clCL.Fields("clipid") = 13 Then
                txtCompanyInfo.Text = "0000976 Veritas GPS"
            End If
            If clCL.Fields("clipid") = 9 Or clCL.Fields("clipid") = 10 Or clCL.Fields("clipid") = 12 Then
                txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL"
            End If
            If clCL.Fields("clipid") = 11 Then
                txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield"
            End If
            If clCL.Fields("clipid") = 0 Then
                If clCL.Fields("contractno").Substring(0, 3) = "VA1" Then
                    txtCompanyInfo.Text = "0000976 Veritas GPS"
                End If
            End If
MoveHere:

            If clCL.Fields("parttax").Length = 0 Then
                clCL.Fields("parttax") = 0
            End If
            If clCL.Fields("labortax").Length = 0 Then
                clCL.Fields("labortax") = 0
            End If
            chkHCC.Checked = CBool(clCL.Fields("hcc"))
            If chkHCC.Checked Then
                tsClaim.Tabs(10).Visible = True
            Else
                tsClaim.Tabs(10).Visible = False
            End If
            txtPartTax.Text = CDbl(clCL.Fields("parttax")) * 100
            txtLaborTax.Text = CDbl(clCL.Fields("labortax")) * 100
            hfContractID.Value = clCL.Fields("contractid")
            pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value

            txtLossCodeClaim.Text = clCL.Fields("losscode")
            GetLossCodeInfo()
            GetContractInfo()
            hfAssignedTo.Value = clCL.Fields("assignedto")
            GetAssignedTo()
            FillDealerInfo()
            checksecurity
        End If
    End Sub

    Private Sub CheckSecurity()
        Dim SQL As String
        Dim clUSI As New clsDBO
        cboClaimStatus.Enabled = True
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUSI.OpenDB(SQL, AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            If cboClaimStatus.SelectedValue = "Denied" Or cboClaimStatus.SelectedValue = "Paid" Then
                If CBool(clUSI.Fields("AllowUndenyClaim")) Then
                    cboClaimStatus.Enabled = True
                Else
                    cboClaimStatus.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Function isFL() As Boolean
        isFL = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select c.State from claim cl inner join contract c on c.contractid = cl.contractid "
        SQL = SQL + "where cl.ClaimID = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("state").ToUpper = "FL" Then
                isFL = True
            End If
        End If
    End Function

    Private Sub FillDealerInfo()
        OpenContract()
        OpenDealer()
        OpenLossRatio()
        OpenAgent()
    End Sub

    Private Sub OpenLossRatio()
        Dim clR As New clsDBO
        Dim SQL As String
        txtLossRatio.Text = ""
        SQL = "select lossratio from VeritasReports.dbo.DealerRatio "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("lossratio").Length > 0 Then
                txtLossRatio.Text = Format(CDbl(clR.Fields("lossratio")), "Percent")
            End If
        End If
    End Sub

    Private Sub OpenAgent()
        If hfAgentID.Value.Length = 0 Then
            txtAgentName.Text = ""
            txtAgentNo.Text = ""
            Exit Sub
        End If

        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            txtAgentName.Text = clA.Fields("agentname")
            txtAgentNo.Text = clA.Fields("agentno")
        End If
    End Sub

    Private Sub OpenDealer()
        If hfDealerID.Value.Length = 0 Then
            txtDealerAddr1.Text = ""
            txtDealerAddr2.Text = ""
            txtDealerAddr3.Text = ""
            txtDealerName.Text = ""
            txtDealerNo.Text = ""
            txtDealerPhone.Text = ""
            Exit Sub
        End If
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            txtDealerName.Text = clD.Fields("dealername")
            txtDealerNo.Text = clD.Fields("dealerno")
            txtDealerAddr1.Text = clD.Fields("addr1")
            txtDealerAddr2.Text = clD.Fields("addr2")
            txtDealerAddr3.Text = clD.Fields("city") & ", " & clD.Fields("state") & " " & clD.Fields("zip")
            txtDealerPhone.Text = clD.Fields("phone")
        End If
    End Sub

    Private Sub OpenContract()
        Dim SQL As String
        Dim clC As New clsDBO
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "Select dealerid, agentsid from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfDealerID.Value = clC.Fields("dealerid")
            hfAgentID.Value = clC.Fields("agentsid")
        Else
            hfDealerID.Value = 0
            hfAgentID.Value = 0
        End If
    End Sub

    Private Sub GetAssignedTo()
        txtAssignedTo.Text = ""
        Dim SQL As String
        Dim clR As New clsDBO
        If hfAssignedTo.Value = "" Then
            hfAssignedTo.Value = hfUserID.Value
        End If
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfAssignedTo.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAssignedTo.Text = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
    End Sub

    Private Sub GetLossCodeInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & txtLossCodeClaim.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLossCodeDescClaim.Text = clR.Fields("losscodedesc").Trim
        End If
    End Sub

    Private Sub GetContractInfo()
        Dim SQL As String
        Dim clC As New clsDBO
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from contract c "
        SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtContractNo.Text = clC.Fields("contractno")
            txtStatus.Text = clC.Fields("status")
            If txtStatus.Text = "Cancelled" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Cancelled Before Paid" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Expired" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Pending Expired" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Invalid" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Pending Cancel" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "PendingCancel" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Quote" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Rejected" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Sale pending" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "test" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Training" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Void" Then
                txtStatus.BackColor = Color.Red
            End If

            If txtStatus.Text = "Pending" Then
                txtStatus.BackColor = Color.Yellow
            End If
            If txtStatus.Text = "Paid" Then
                txtStatus.BackColor = Color.Green
            End If
            txtPaidDate.Text = clC.Fields("datepaid")
            txtSaleDate.Text = Format(CDate(clC.Fields("saledate")), "M/d/yyyy")
            txtFName.Text = clC.Fields("fname")
            txtLName.Text = clC.Fields("lname")
            txtCoveredAddr1.Text = clC.Fields("coveredaddr1")
            txtCoveredAddr2.Text = clC.Fields("coveredaddr2")
            txtCoveredCity.Text = clC.Fields("coveredcity")
            txtInspCarrier.Text = clC.Fields("inscarriername")
            txtCoveredState.Text = clC.Fields("state")
            txtCoveredZip.Text = clC.Fields("coveredzip")
            hfPlanTypeID.Value = clC.Fields("plantypeid")

            hfProgram.Value = clC.Fields("programid")
            txtTerm.Text = clC.Fields("termmonth")
            If clC.Fields("effdate").Length > 0 Then
                txtEffDate.Text = Format(CDate(clC.Fields("effdate")), "M/d/yyyy")
            End If

            If clC.Fields("expdate").Length > 0 Then
                txtExpDate.Text = Format(CDate(clC.Fields("expdate")), "M/d/yyyy")
            End If
            GetProgram()
            GetPlanType()
            If clC.Fields("decpage").Length = 0 Then

                tcDec.Visible = False
                tcDesc2.Visible = False
            Else
                tcDec.Visible = True
                tcDesc2.Visible = True
                hlDec.NavigateUrl = clC.Fields("decpage")
                hlDec.Text = clC.Fields("decpage")
            End If
            If clC.Fields("tcpage").Length = 0 Then
                tcTC.Visible = False
                tcTC2.Visible = False
            Else
                tcTC.Visible = True
                tcTC2.Visible = True
                hlTC.NavigateUrl = clC.Fields("tcpage")
                hlTC.Text = clC.Fields("tcpage")
            End If
        End If
    End Sub

    Private Sub GetPlanType()
        Dim SQL As String
        Dim clPT As New clsDBO
        SQL = "select * from plantype "
        SQL = SQL + "where plantypeid = " & hfPlanTypeID.Value
        clPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clPT.RowCount > 0 Then
            clPT.GetRow()
            txtPlan.Text = clPT.Fields("plantype")
        End If
    End Sub

    Private Sub GetProgram()
        Dim SQL As String
        Dim clP As New clsDBO
        SQL = "select * from program "
        SQL = SQL + "where programid = " & hfProgram.Value
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            txtProgram.Text = clP.Fields("programname")
        End If
    End Sub

    Private Sub PlaceServiceCenter(xServiceCenterID As String)
        Dim clSC As New clsDBO
        Dim SQL As String
        If xServiceCenterID.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & xServiceCenterID
        clSC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSC.RowCount > 0 Then
            clSC.GetRow()
            txtServiceCenter.Text = clSC.Fields("servicecenterno")
            txtServiceInfo.Text = clSC.Fields("servicecentername") & " " & clSC.Fields("addr1") & " " & clSC.Fields("addr2") & " " & clSC.Fields("city") & " " & clSC.Fields("state") & " " & clSC.Fields("zip") & " " & clSC.Fields("phone")
            txtSCEmail.Text = clSC.Fields("email")
            txtSCFax.Text = clSC.Fields("fax")
            If clSC.Fields("zip").Length > 0 Then
                GetLaborInfo(clSC.Fields("zip"))
            End If
        End If
    End Sub

    Private Sub GetLaborInfo(xZip As String)
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clWZ As New VeritasGlobalTools.clsWISZipCode
        clWZ.ZipCode = xZip
        clWZ.RunZip()
        SQL = "select * from laborrate "
        SQL = SQL + "where zipcode = '" & xZip & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAvgRate.Text = Format(CDbl(clR.Fields("avgrate")), "#,##0.00")
        Else
            txtAvgRate.Text = Format(0, "#,##0.00")
        End If

    End Sub

    Private Sub btnSeekServiceCenter_Click(sender As Object, e As EventArgs) Handles btnSeekServiceCenter.Click
        pnlDetail.Visible = False
        pnlSeekSC.Visible = True
        rgServiceCenter.DataSourceID = "dsServiceCenter"
        rgServiceCenter.Rebind()
    End Sub

    Private Sub rgServiceCenter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgServiceCenter.SelectedIndexChanged
        pnlSeekSC.Visible = False
        pnlDetail.Visible = True
        hfServiceCenterID.Value = rgServiceCenter.SelectedValue
        PlaceServiceCenter(hfServiceCenterID.Value)
        UpdateClaimPayee()
        UpdateClaim()
        UpdateDeduct()
    End Sub

    Private Sub UpdateClaimPayee()
        Dim SQL As String
        Dim clS As New clsDBO
        Dim clR As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claimpayee "
        SQL = SQL + "where payeeno = '" & txtServiceCenter.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            SQL = "select * from servicecenter "
            SQL = SQL + "where servicecenterno = '" & txtServiceCenter.Text & "' "
            clS.OpenDB(SQL, AppSettings("connstring"))
            If clS.RowCount > 0 Then
                clS.GetRow()
                clR.NewRow()
                clR.Fields("payeeno") = clS.Fields("servicecenterno")
                clR.Fields("payeename") = clS.Fields("servicecentername")
                clR.Fields("addr1") = clS.Fields("addr1")
                clR.Fields("addr2") = clS.Fields("addr2")
                clR.Fields("city") = clS.Fields("city")
                clR.Fields("state") = clS.Fields("state")
                clR.Fields("zip") = clS.Fields("zip")
                clR.Fields("phone") = clS.Fields("phone")
                clR.AddRow()
                clR.SaveDB()
            End If

        End If
    End Sub

    Private Sub UpdateDeduct()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clCP As New clsDBO
        Dim clCD As New clsDBO
        Dim sServiceCenterNo As String = ""
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & hfServiceCenterID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from claimpayee "
            SQL = SQL + "where payeeno = '" & clR.Fields("servicecenterno") & "' "
            clCP.OpenDB(SQL, AppSettings("connstring"))
            If clCP.RowCount > 0 Then
                clCP.GetRow()
                SQL = "update claimdetail "
                SQL = SQL + "set claimpayeeid = " & clCP.Fields("claimpayeeid") & " "
                SQL = SQL + "where claimid = " & hfClaimID.Value & " "
                SQL = SQL + "and jobno = 'A01' "
                clCD.RunSQL(SQL, AppSettings("connstring"))
            End If
        End If
    End Sub

    Private Sub btnSeekContract_Click(sender As Object, e As EventArgs) Handles btnSeekContract.Click
        Dim SQL As String
        pnlDetail.Visible = False
        pnlSeekContract.Visible = True
        rgContract.DataSourceID = "dsContract"
        rgContract.Rebind()
    End Sub

    Private Function AllowCancelExpire() As Boolean
        AllowCancelExpire = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("allowCancelExpire")) Then
                AllowCancelExpire = True
            End If
        End If
    End Function

    Private Sub rgContract_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgContract.SelectedIndexChanged
        Dim SQL As String
        Dim clC As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        pnlContract.Visible = False
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & rgContract.SelectedValue
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("status") = "Paid" Then
                hfContractID.Value = rgContract.SelectedValue
                pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" & hfID.Value & "&contractid=" & hfClaimID.Value
                UpdateClaim()
                AddClaimDetail()
                FillDealerInfo()
                pnlDetail.Visible = True
                Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
                Exit Sub
            Else
                hfContractID.Value = rgContract.SelectedValue
                UpdateClaim()
                txtContractStatus2.Text = clC.Fields("status")
                pnlContractConfirm.Visible = True
                FillDealerInfo()
                AddClaimDetail()
                Exit Sub
            End If
        End If
        UpdateClaim()
        UpdateDeduct()
    End Sub

    Private Sub AddClaimDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lDeductID As Long
        Dim dDeduct As Long
        SQL = "select deductid from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lDeductID = clR.Fields("deductid")
        End If
        SQL = "select * from deductible "
        SQL = SQL + "where deductid = " & lDeductID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            dDeduct = clR.Fields("deductamt")
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and jobno = 'A01' "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
        Else
            clR.GetRow()
        End If
        GetClaimPayee()
        clR.Fields("ratetypeid") = 1
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimdetailtype") = "Deductible"
        clR.Fields("jobno") = "A01"
        clR.Fields("reqqty") = "1"
        clR.Fields("reqcost") = dDeduct * -1
        clR.Fields("Claimpayeeid") = hfClaimPayeeID.Value
        clR.Fields("claimdetailstatus") = "Requested"
        clR.Fields("reqamt") = dDeduct * -1
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("credate") = Today
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("moddate") = Today
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub GetClaimPayee()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clSC As New clsDBO
        If txtServiceCenter.Text.Length = 0 Then
            hfClaimPayeeID.Value = 0
            Exit Sub
        End If
        SQL = "select * from claimpayee "
        SQL = SQL + "where payeeno = '" & txtServiceCenter.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            SQL = "select * from servicecenter "
            SQL = SQL + "where servicecenterno = '" & txtServiceCenter.Text & "' "
            clSC.OpenDB(SQL, AppSettings("connstring"))
            If clSC.RowCount > 0 Then
                clSC.GetRow()
                clR.NewRow()
                clR.Fields("payeeno") = clSC.Fields("servicecenterno")
                clR.Fields("payeename") = clSC.Fields("servicecentername")
                clR.Fields("addr1") = clSC.Fields("addr1")
                clR.Fields("addr2") = clSC.Fields("addr2")
                clR.Fields("city") = clSC.Fields("city")
                clR.Fields("state") = clSC.Fields("state")
                clR.Fields("zip") = clSC.Fields("zip")
                clR.Fields("phone") = clSC.Fields("phone")
                clR.AddRow()
                clR.SaveDB()
                SQL = "select * from claimpayee "
                SQL = SQL + "where payeeno = '" & txtServiceCenter.Text & "' "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    clR.GetRow()
                    hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
                End If
            End If
        Else
            clR.GetRow()
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
        End If
    End Sub

    Private Sub UpdateClaim()
        Dim SQL As String
        Dim clClaim As New clsDBO
        Dim clCH As New VeritasGlobalTools.clsClaimHistory
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clClaim.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clClaim.RowCount > 0 Then
            clClaim.GetRow()
        Else
            clClaim.NewRow()
        End If
        If hfContractID.Value = "" Then
            hfContractID.Value = 0
        End If
        If clClaim.Fields("contractid") <> hfContractID.Value Then
            clCH.ClaimID = hfClaimID.Value
            clCH.FieldName = "ContractID"
            clCH.PrevFieldInfo = clClaim.Fields("contractid")
            clCH.FieldInfo = hfContractID.Value
            clCH.CreBy = hfUserID.Value
            clCH.AddClaimHistory()
        End If
        clClaim.Fields("contractid") = hfContractID.Value
        If clClaim.Fields("claimactivityid") <> cboActivity.SelectedValue Then
            clCH.ClaimID = hfClaimID.Value
            clCH.FieldName = "ClaimActivityID"
            clCH.PrevFieldInfo = clClaim.Fields("claimactivityid")
            clCH.FieldInfo = cboActivity.SelectedValue
            clCH.CreBy = hfUserID.Value
            clCH.AddClaimHistory()
        End If
        clClaim.Fields("claimactivityid") = cboActivity.SelectedValue
        If clClaim.Fields("status") <> cboClaimStatus.SelectedValue Then
            clCH.ClaimID = hfClaimID.Value
            clCH.FieldName = "Status"
            clCH.PrevFieldInfo = clClaim.Fields("status")
            clCH.FieldInfo = cboClaimStatus.SelectedValue
            clCH.CreBy = hfUserID.Value
            clCH.AddClaimHistory()
        End If
        clClaim.Fields("status") = cboClaimStatus.SelectedValue
        If hfServiceCenterID.Value = "" Then
            hfServiceCenterID.Value = 0
        End If
        If clClaim.Fields("servicecenterid") <> hfServiceCenterID.Value Then
            clCH.ClaimID = hfClaimID.Value
            clCH.FieldName = "ServiceCenterID"
            clCH.PrevFieldInfo = clClaim.Fields("servicecenterid")
            clCH.FieldInfo = hfServiceCenterID.Value
            clCH.CreBy = hfUserID.Value
            clCH.AddClaimHistory()
        End If
        clClaim.Fields("servicecenterid") = hfServiceCenterID.Value
        If clClaim.Fields("sccontactinfo") <> txtServiceCenterContact.Text Then
            clCH.ClaimID = hfClaimID.Value
            clCH.FieldName = "SCContactInfo"
            clCH.PrevFieldInfo = clClaim.Fields("sccontactinfo")
            clCH.FieldInfo = txtServiceCenterContact.Text
            clCH.CreBy = hfUserID.Value
            clCH.AddClaimHistory()
        End If

        clClaim.Fields("sccontactinfo") = txtServiceCenterContact.Text
        If clClaim.RowCount > 0 Then
            clClaim.Fields("moddate") = Today
            clClaim.Fields("modby") = hfUserID.Value
        Else
            clClaim.Fields("credate") = Today
            clClaim.Fields("creby") = hfUserID.Value
            clClaim.Fields("assignedto") = hfUserID.Value
            clClaim.Fields("moddate") = Today
            clClaim.Fields("modby") = hfUserID.Value
            clClaim.AddRow()
        End If
        clClaim.SaveDB()
        CheckClaimDuplicate()

    End Sub

    Private Sub btnProceedClaim_Click(sender As Object, e As EventArgs) Handles btnProceedClaim.Click
        UpdateClaimNote()
        UpdateContractClaim()
        pnlContractConfirm.Visible = False
        pnlDetail.Visible = True
        Response.Redirect("~/claim/claim.aspx?sid=" & hfID.Value & "&ClaimID=" & hfClaimID.Value)
    End Sub

    Private Sub btnCancelClaim_Click(sender As Object, e As EventArgs) Handles btnCancelClaim.Click
        UpdateContractClaimCancel()
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub UpdateContractClaimCancel()
        Dim clCL As New clsDBO
        Dim SQL As String
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            clCL.Fields("status") = "Void"
            clCL.Fields("contractid") = hfContractID.Value
            clCL.SaveDB()
        End If
    End Sub

    Private Sub UpdateContractClaim()
        Dim clCL As New clsDBO
        Dim SQL As String
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            clCL.Fields("contractid") = hfContractID.Value
            clCL.SaveDB()
        End If
    End Sub

    Private Sub UpdateClaimNote()
        If txtNote.Text.Length = 0 Then
            Exit Sub
        End If
        Dim clCN As New clsDBO
        Dim SQL As String
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnoteid = 0 "
        clCN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clCN.NewRow()
        clCN.Fields("claimid") = hfClaimID.Value
        clCN.Fields("claimnotetypeid") = 5
        clCN.Fields("note") = txtNote.Text
        clCN.Fields("credate") = Today
        clCN.Fields("creby") = hfUserID.Value
        clCN.AddRow()
        clCN.SaveDB()
    End Sub

    Private Sub tsClaim_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsClaim.TabClick
        If tsClaim.SelectedTab.Value = "Payment" Then
            pvPayment.Selected = True
        End If
        'If tsClaim.SelectedTab.Value = "Dealer" Then
        '    pvDealer.Selected = True
        'End If
        If tsClaim.SelectedTab.Value = "Customer" Then
            pvCustomer.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "3C" Then
            pv3C.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Documents" Then
            pvDocuments.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Insp" Then
            pvInspection.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Part" Then
            pvParts.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Notes" Then
            pvNotes.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Jobs" Then
            pvJob.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Alert" Then
            pvAlert.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "History" Then
            pvHistory.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "HCC" Then
            pvHCC.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "ContractNote" Then
            pvContractNote.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Surcharges" Then
            pvSurcharges.Selected = True
        End If
    End Sub

    Private Function CheckAlert() As Boolean
        CheckAlert = False
        If tsClaim.Tabs(5).Visible = False Then
            Exit Function
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and Alert <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckAlert = True
        End If
    End Function

    Private Sub txtServiceCenterContact_TextChanged(sender As Object, e As EventArgs) Handles txtServiceCenterContact.TextChanged
        UpdateClaim()
    End Sub

    Private Sub cboActivity_TextChanged(sender As Object, e As EventArgs) Handles cboActivity.TextChanged
        UpdateClaim()
        If cboActivity.SelectedValue = 11 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 14 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 15 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 17 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 18 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 21 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 22 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 23 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 24 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 25 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 26 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 28 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 29 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 30 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 31 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 32 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 33 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 35 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 36 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 37 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 38 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 39 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 40 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 42 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 43 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 45 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 46 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 47 Then
            CloseClaim()
        End If
        If cboActivity.SelectedValue = 48 Then
            CloseClaim()
        End If

        If cboClaimStatus.SelectedValue = "Open" Then
            If Not CheckNoteOpen() Then
                If cboActivity.SelectedValue <> 11 And cboActivity.SelectedValue <> 14 Then
                    If cboActivity.SelectedValue <> 15 And cboActivity.SelectedValue <> 17 Then
                        If cboActivity.SelectedValue <> 18 And cboActivity.SelectedValue <> 21 Then
                            If cboActivity.SelectedValue <> 22 And cboActivity.SelectedValue <> 23 Then
                                If cboActivity.SelectedValue <> 24 And cboActivity.SelectedValue <> 25 Then
                                    If cboActivity.SelectedValue <> 26 And cboActivity.SelectedValue <> 28 Then
                                        If cboActivity.SelectedValue <> 29 And cboActivity.SelectedValue <> 30 Then
                                            If cboActivity.SelectedValue <> 31 And cboActivity.SelectedValue <> 32 Then
                                                If cboActivity.SelectedValue <> 33 And cboActivity.SelectedValue <> 35 Then
                                                    If cboActivity.SelectedValue <> 36 And cboActivity.SelectedValue <> 37 Then
                                                        If cboActivity.SelectedValue <> 38 And cboActivity.SelectedValue <> 39 Then
                                                            If cboActivity.SelectedValue <> 40 And cboActivity.SelectedValue <> 42 Then
                                                                If cboActivity.SelectedValue <> 43 And cboActivity.SelectedValue <> 45 Then
                                                                    If cboActivity.SelectedValue <> 46 And cboActivity.SelectedValue <> 47 Then
                                                                        If cboActivity.SelectedValue <> 48 Then
                                                                            ShowOpen()
                                                                            Exit Sub
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub CloseClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set closedate = '" & Today & "', "
        SQL = SQL + "closeby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and closedate is null "
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "update claimopenhistory "
        SQL = SQL + "set closedate = '" & Today & "', "
        SQL = SQL + "closeby = " & hfUserID.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and closedate is null "
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub txtPartTax_TextChanged(sender As Object, e As EventArgs) Handles txtPartTax.TextChanged
        If txtPartTax.Text = "" Then
            txtPartTax.Text = 0
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claim "
        SQL = SQL + "set parttax = " & CDbl(txtPartTax.Text) / 100 & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub txtLaborTax_TextChanged(sender As Object, e As EventArgs) Handles txtLaborTax.TextChanged
        If txtLaborTax.Text = "" Then
            txtLaborTax.Text = 0
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set labortax = " & CDbl(txtLaborTax.Text) / 100 & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub ClearLock()
        Dim SQL As String
        Dim clR As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set lockuserid = null, lockdate = null "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and lockuserid = " & hfUserID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Function CheckNote() As Boolean
        CheckNote = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckNote = True
        End If
    End Function

    Private Sub btnCalcPart_Click(sender As Object, e As EventArgs) Handles btnCalcPart.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailtype = 'Part' "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)

                ProcessClaimTax(clR.Fields("claimdetailid"))
            Next
        End If
    End Sub

    Private Sub ProcessClaimTax(xClaimDetailID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailid = " & xClaimDetailID
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields(("claimdetailtype")) = "Part" Then
                If clR.Fields("authamt").Length > 0 Then
                    If CDbl(clR.Fields("authamt")) > 0 Then
                        clR.Fields("taxper") = txtPartTax.Text / 100
                        clR.Fields("taxamt") = Decimal.Round(CDbl(clR.Fields("authamt")) * (CDbl(txtPartTax.Text))) / 100
                        clR.Fields("totalamt") = CDbl(clR.Fields("authamt")) + CDbl(clR.Fields("taxamt"))
                    Else
                        clR.Fields("taxper") = txtPartTax.Text / 100
                        clR.Fields("taxamt") = 0
                        clR.Fields("totalamt") = 0
                    End If
                Else
                    clR.Fields("taxper") = txtPartTax.Text / 100
                    clR.Fields("taxamt") = 0
                    clR.Fields("totalamt") = 0
                End If
            End If
            If clR.Fields(("claimdetailtype")) = "Labor" Then
                If clR.Fields("authamt").Length > 0 Then
                    If CDbl(clR.Fields("authamt")) > 0 Then
                        clR.Fields("taxper") = txtLaborTax.Text / 100
                        clR.Fields("taxamt") = Decimal.Round(CDbl(clR.Fields("authamt")) * (CDbl(txtLaborTax.Text))) / 100
                        clR.Fields("totalamt") = CDbl(clR.Fields("authamt")) + CDbl(clR.Fields("taxamt"))
                    Else
                        clR.Fields("taxper") = txtLaborTax.Text / 100
                        clR.Fields("taxamt") = 0
                        clR.Fields("totalamt") = 0
                    End If
                Else
                    clR.Fields("taxper") = txtLaborTax.Text / 100
                    clR.Fields("taxamt") = 0
                    clR.Fields("totalamt") = 0
                End If
            End If

            clR.SaveDB()
        End If

    End Sub

    Private Sub btnCalcLabor_Click(sender As Object, e As EventArgs) Handles btnCalcLabor.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimdetailtype = 'Labor' "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                ProcessClaimTax(clR.Fields("claimdetailid"))
            Next
        End If
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        ClearLock()
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAddSC_Click(sender As Object, e As EventArgs) Handles btnAddSC.Click
        pnlAddSC.Visible = True
        pnlSeekSC.Visible = False
    End Sub

    Private Sub btnSCCancel_Click(sender As Object, e As EventArgs) Handles btnSCCancel.Click
        pnlSeekSC.Visible = True
        pnlAddSC.Visible = False
    End Sub

    Private Sub btnSCSave_Click(sender As Object, e As EventArgs) Handles btnSCSave.Click
        AddServiceCenter()
        rgServiceCenter.Rebind()
        pnlSeekSC.Visible = True
        pnlAddSC.Visible = False
    End Sub

    Private Sub AddServiceCenter()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        GetNextServiceCenterNo()
        clR.Fields("servicecenterno") = hfServiceCenterNo.Value
        clR.Fields("servicecentername") = txtServiceCenterName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("fax") = txtFax.Text
        clR.Fields("email") = txtEMail.Text
        clR.AddRow()
        clR.SaveDB()
    End Sub

    Private Sub GetNextServiceCenterNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select max(servicecenterno) as mSCN from servicecenter "
        SQL = SQL + "where servicecenterno like 'rf0%' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.GetRow()
        hfServiceCenterNo.Value = "RF" & Format(CLng(Right(clR.Fields("mscn"), 6)) + 1, "0000000")
    End Sub

    Private Sub cboClaimStatus_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles cboClaimStatus.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        tbLock.Visible = False
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If

        If cboClaimStatus.SelectedValue = "Denied" Then
            If CheckRequested() Then
                tbLock.Visible = True
                lblLocked.Text = "You must change all Jobs from requested to denied or authorized."
                cboClaimStatus.SelectedValue = "Open"
                Exit Sub
            End If
        End If

        If CheckPaidDenied() Then
            If Not CheckUndeny() Then
                tbLock.Visible = True
                lblLocked.Text = "Only Jeff, Zach, Darius or Joe can reactivate a claim"
                Exit Sub
            End If
        End If
        SQL = "update claim "
        SQL = SQL + "set status = '" & cboClaimStatus.Text & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        If cboClaimStatus.SelectedValue = "Denied" Then
            If Not CheckNoteDenied() Then
                ShowDenied()
            End If
        End If

        If cboClaimStatus.SelectedValue = "Open" Then
            If Not CheckNoteOpen() Then
                If cboActivity.SelectedValue <> 11 And cboActivity.SelectedValue <> 14 Then
                    If cboActivity.SelectedValue <> 15 And cboActivity.SelectedValue <> 17 Then
                        If cboActivity.SelectedValue <> 18 And cboActivity.SelectedValue <> 21 Then
                            If cboActivity.SelectedValue <> 22 And cboActivity.SelectedValue <> 23 Then
                                If cboActivity.SelectedValue <> 24 And cboActivity.SelectedValue <> 25 Then
                                    If cboActivity.SelectedValue <> 26 And cboActivity.SelectedValue <> 28 Then
                                        If cboActivity.SelectedValue <> 29 And cboActivity.SelectedValue <> 30 Then
                                            If cboActivity.SelectedValue <> 31 And cboActivity.SelectedValue <> 32 Then
                                                If cboActivity.SelectedValue <> 33 And cboActivity.SelectedValue <> 35 Then
                                                    If cboActivity.SelectedValue <> 36 And cboActivity.SelectedValue <> 37 Then
                                                        If cboActivity.SelectedValue <> 38 And cboActivity.SelectedValue <> 39 Then
                                                            If cboActivity.SelectedValue <> 40 And cboActivity.SelectedValue <> 42 Then
                                                                If cboActivity.SelectedValue <> 43 And cboActivity.SelectedValue <> 45 Then
                                                                    If cboActivity.SelectedValue <> 46 And cboActivity.SelectedValue <> 47 Then
                                                                        If cboActivity.SelectedValue <> 48 Then
                                                                            ShowOpen()
                                                                            Exit Sub
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function CheckUndeny()
        CheckUndeny = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and allowundenyclaim <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckUndeny = True
        End If
    End Function

    Private Function CheckRequested() As Boolean
        Dim clR As New clsDBO
        Dim SQL As String
        CheckRequested = False
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailstatus = 'Requested' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckRequested = True
        End If
    End Function

    Private Function CheckPaidDenied() As Boolean
        Dim clR As New clsDBO
        Dim SQL As String
        CheckPaidDenied = False
        SQL = "select * from claim "
        SQL = SQL + "where (status =  'Paid' "
        SQL = SQL + "or status = 'Denied') "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckPaidDenied = True
        End If
    End Function

    Private Function CheckNoteOpen() As Boolean
        CheckNoteOpen = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 6 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckNoteOpen = True
        End If
    End Function

    Private Function CheckNoteDenied() As Boolean
        CheckNoteDenied = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnotetypeid = 7 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckNoteDenied = True
        End If
    End Function

    Private Sub btnAlertOK_Click(sender As Object, e As EventArgs) Handles btnAlertOK.Click
        HideAlert()
    End Sub

    Private Sub txtRONumber_TextChanged(sender As Object, e As EventArgs) Handles txtRONumber.TextChanged
        Dim clR As New clsDBO
        Dim SQL As String
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set ronumber = '" & txtRONumber.Text & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        CheckClaimDuplicate()
    End Sub

    Private Sub rdpRODate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpRODate.SelectedDateChanged
        Dim clR As New clsDBO
        Dim SQL As String
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set rodate = '" & rdpRODate.SelectedDate & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub txtShopRate_TextChanged(sender As Object, e As EventArgs) Handles txtShopRate.TextChanged
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clCD As New clsDBO
        If txtShopRate.Text.Length = 0 Then
            Exit Sub
        End If
        SQL = "update claim "
        SQL = SQL + "set shoprate = " & txtShopRate.Text & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimdetailtype = 'Labor' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimdetail "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCD.OpenDB(SQL, AppSettings("connstring"))
                If clCD.RowCount > 0 Then
                    clCD.GetRow()
                    clCD.Fields("reqcost") = txtShopRate.Text
                    clCD.Fields("reqamt") = CDbl(txtShopRate.Text) * CDbl(clR.Fields("reqqty"))
                    clCD.SaveDB()
                End If
            Next
        End If
    End Sub

    Private Sub btnSeekLossCode_Click(sender As Object, e As EventArgs) Handles btnSeekLossCode.Click
        pnlSeekLossCode.Visible = True
        pnlDetail.Visible = False
        rgLossCode.Rebind()
        ColorLossCode()
    End Sub

    Private Sub rgLossCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgLossCode.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        txtLossCodeClaim.Text = rgLossCode.SelectedValue
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & rgLossCode.SelectedValue & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtLossCodeDescClaim.Text = clR.Fields("losscodedesc")
        End If
        pnlDetail.Visible = True
        pnlSeekLossCode.Visible = False
        SQL = "update claim "
        SQL = SQL + "set losscode = '" & rgLossCode.SelectedValue & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
    End Sub

    Private Sub btnAddLossCode_Click(sender As Object, e As EventArgs) Handles btnAddLossCode.Click
        pnlSeekLossCode.Visible = False
        pnlLossCode.Visible = True
    End Sub

    Private Sub btnCancelLossCode_Click(sender As Object, e As EventArgs) Handles btnCancelLossCode.Click
        pnlSeekLossCode.Visible = True
        pnlLossCode.Visible = False
    End Sub

    Private Sub btnSaveLossCode_Click(sender As Object, e As EventArgs) Handles btnSaveLossCode.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & txtLossCode.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("losscode") = txtLossCode.Text
            clR.Fields("losscodedesc") = txtLossCodeDesc.Text
            clR.AddRow()
            clR.SaveDB()
        End If
        rgLossCode.Rebind()
        pnlSeekLossCode.Visible = True
        pnlLossCode.Visible = False
    End Sub

    Private Sub btnChangeAssignedTo_Click(sender As Object, e As EventArgs) Handles btnChangeAssignedTo.Click
        pnlDetail.Visible = False
        pnlAssignedToChange.Visible = True
    End Sub

    Private Sub rgAssignedTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAssignedTo.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        hfAssignedTo.Value = rgAssignedTo.SelectedValue
        If hfReadOnly.Value = "True" Then
            Exit Sub
        End If
        GetAssignedTo()
        SQL = "update claim "
        SQL = SQL + "set assignedto = " & hfAssignedTo.Value & " "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        pnlAssignedToChange.Visible = False
        pnlDetail.Visible = True
    End Sub

    Private Sub txtSCEmail_TextChanged(sender As Object, e As EventArgs) Handles txtSCEmail.TextChanged
        If txtServiceCenter.Text.Length = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterno = '" & txtServiceCenter.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("email") = txtSCEmail.Text
            clR.SaveDB()
        End If
    End Sub

    Private Sub txtSCFax_TextChanged(sender As Object, e As EventArgs) Handles txtSCFax.TextChanged
        If txtServiceCenter.Text.Length = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterno = '" & txtServiceCenter.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("fax") = txtSCFax.Text
            clR.SaveDB()
        End If
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        If Not CheckNote() Then
            lblError.Text = "You need to enter in Note."
            ShowError()
            Exit Sub
        End If
        ClearLock()
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub chkHCC_CheckedChanged(sender As Object, e As EventArgs) Handles chkHCC.CheckedChanged
        Dim clR As New clsDBO
        Dim SQL As String
        If chkHCC.Checked = True Then
            SQL = "select * from claimhcc "
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount = 0 Then
                clR.NewRow()
                clR.Fields("Adjusterid") = hfUserID.Value
                clR.Fields("DateSubmitted") = Today
                clR.Fields("claimid") = hfClaimID.Value
                clR.AddRow()
                clR.SaveDB()
            End If
            tsClaim.Tabs(10).Visible = True
            SQL = "update claim "
            SQL = SQL + "set hcc = 1 "
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
        Else
            tsClaim.Tabs(10).Visible = False
            SQL = "update claim "
            SQL = SQL + "set hcc = 0 "
            SQL = SQL + "where claimid = " & hfClaimID.Value
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
    End Sub

    Private Sub btnDeniedClose_Click(sender As Object, e As EventArgs) Handles btnDeniedClose.Click
        HideDenied()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select status from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            cboClaimStatus.SelectedValue = clR.Fields("status")
        End If
        SQL = "update claim "
        SQL = SQL + "set status = '" & cboClaimStatus.Text & "', "
        SQL = SQL + "closedate = null, opendate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "insert into claimopenhistory "
        SQL = SQL + "(claimid, opendate, openby) "
        SQL = SQL + "values (" & hfClaimID.Value & ",'"
        SQL = SQL + Today & "',"
        SQL = SQL + hfUserID.Value & ") "
        clR.RunSQL(SQL, AppSettings("onnstring"))
    End Sub

    Private Sub btnDeniedSave_Click(sender As Object, e As EventArgs) Handles btnDeniedSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnoteid = 7 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimnotetypeid") = 7
        clR.Fields("note") = txtDeniedNote.Text
        clR.Fields("credate") = Now
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("notetext") = txtDeniedNote.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        SQL = "update claim "
        SQL = SQL + "set status = '" & cboClaimStatus.Text & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        HideDenied()
    End Sub

    Private Sub btnOpenClose_Click(sender As Object, e As EventArgs) Handles btnOpenClose.Click
        HideOpen()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select status, ClaimActivityID from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            cboClaimStatus.SelectedValue = clR.Fields("status")
            cboActivity.SelectedValue = clR.Fields("ClaimActivityID")
        End If
        SQL = "update claim "
        SQL = SQL + "set status = '" & cboClaimStatus.Text & "', "
        SQL = SQL + "closedate = null, opendate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "insert into claimopenhistory "
        SQL = SQL + "(claimid, opendate, openby) "
        SQL = SQL + "values (" & hfClaimID.Value & ",'"
        SQL = SQL + Today & "',"
        SQL = SQL + hfUserID.Value & ") "
        clR.RunSQL(SQL, AppSettings("onnstring"))
    End Sub

    Private Sub btnOpenSave_Click(sender As Object, e As EventArgs) Handles btnOpenSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        If txtOpenNote.Text.Length < 25 Then
            Exit Sub

        End If
        SQL = "select * from claimnote "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimnoteid = 6 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimnotetypeid") = 6
        clR.Fields("note") = txtOpenNote.Text
        clR.Fields("credate") = Now
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        clR.Fields("notetext") = txtOpenNote.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
        hfOpen.Value = ""
        SQL = "update claim "
        SQL = SQL + "set status = '" & cboClaimStatus.Text & "', "
        SQL = SQL + "closedate = null, opendate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "insert into claimopenhistory "
        SQL = SQL + "(claimid, opendate, openby) "
        SQL = SQL + "values (" & hfClaimID.Value & ",'"
        SQL = SQL + Today & "',"
        SQL = SQL + hfUserID.Value & ") "
        clR.RunSQL(SQL, AppSettings("onnstring"))
        HideOpen()
    End Sub

    Private Sub CheckClaimDuplicate()
        Dim SQL As String
        Dim clR As New clsDBO
        tbLock.Visible = False
        If hfContractID.Value.Length > 0 Then
            SQL = "select * from claim "
            SQL = SQL + "where contractid = " & hfContractID.Value & " "
            SQL = SQL + "and credate > '" & DateAdd(DateInterval.Day, -3, Today) & "' "
            clR.OpenDB(SQL, AppSettings("connstring"))
            If clR.RowCount > 1 Then
                lblLocked.Text = "There is a claim already entered within the last 3 days."
                tbLock.Visible = True
                Exit Sub
            End If
        End If
        If hfServiceCenterID.Value.Length > 0 Then
            If txtRONumber.Text.Length > 0 Then
                SQL = "select * from claim "
                SQL = SQL + "where servicecenterid = " & hfServiceCenterID.Value & " "
                SQL = SQL + "and ronumber = '" & txtRONumber.Text & "' "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 1 Then
                    lblLocked.Text = "There is a claim already have RO Number for this Service Ceneter."
                    tbLock.Visible = True
                    Exit Sub
                End If
            End If
        End If
    End Sub


    Private Sub ColorLossCode()
        Dim SQL As String
        Dim clR As New clsDBO

        rgLossCode.Columns(3).Visible = True
        For cnt2 = 0 To rgLossCode.Items.Count - 1
            If rgLossCode.Items(cnt2).Item("ACP").Text = "1" Then
                rgLossCode.Items(cnt2).Item("losscode").BackColor = Drawing.Color.Red
            End If
        Next
        rgLossCode.Columns(3).Visible = False
    End Sub


End Class