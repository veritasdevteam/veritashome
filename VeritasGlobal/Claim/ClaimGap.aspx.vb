﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager
Imports System.Drawing

Public Class ClaimGap
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsClaimStatus.ConnectionString = AppSettings("connstring")
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        End If
        If Not IsPostBack Then
            GetServerInfo()
            lblAtt.Visible = False
            tsClaim.Tabs(7).BackColor = Color.White
            GetClaim()
            tsClaim.Tabs(0).Selected = True
            pvCustomer.Selected = True
            pvPOA.ContentUrl = "~/claim/ClaimGAPPOA.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvNotes.ContentUrl = "~/claim/ClaimGAPNote.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvCoBuyer.ContentUrl = "~/claim/ClaimGAPCoBuyer.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvAttorney.ContentUrl = "~/claim/ClaimGAPAttorney.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvDocuments.ContentUrl = "~/claim/ClaimGapDocument.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvCalc.ContentUrl = "~/claim/ClaimGapCalc.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvStatus.ContentUrl = "~/claim/ClaimGapDocumentStatus.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvFinance.ContentUrl = "~/claim/ClaimGapFinance.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvPayment.ContentUrl = "~/claim/ClaimGapPayment.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            pvLoanInfo.ContentUrl = "~/claim/ClaimGapLoanInfo.aspx?sid=" & hfID.Value & "&claimid=" & hfClaimID.Value
            GetContractInfo()
            FillDealerInfo()
        End If
    End Sub

    Private Sub GetContractInfo()
        Dim SQL As String
        Dim clC As New clsDBO
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from contract c "
        SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtContractNo.Text = clC.Fields("contractno")
            txtStatus.Text = clC.Fields("status")
            If txtStatus.Text = "Cancelled" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Cancelled Before Paid" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Expired" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Pending Expired" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Invalid" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Pending Cancel" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "PendingCancel" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Quote" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Rejected" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Sale pending" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "test" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Training" Then
                txtStatus.BackColor = Color.Red
            End If
            If txtStatus.Text = "Void" Then
                txtStatus.BackColor = Color.Red
            End If

            If txtStatus.Text = "Pending" Then
                txtStatus.BackColor = Color.Yellow
            End If
            If txtStatus.Text = "Paid" Then
                txtStatus.BackColor = Color.Green
            End If
            txtPaidDate.Text = clC.Fields("datepaid")
            txtSaleDate.Text = Format(CDate(clC.Fields("saledate")), "M/d/yyyy")
            txtFName.Text = clC.Fields("fname")
            txtLName.Text = clC.Fields("lname")
            hfPlanTypeID.Value = clC.Fields("plantypeid")
            hfProgram.Value = clC.Fields("programid")
            hfDealerID.Value = clC.Fields("dealerid")
            hfAgentID.Value = clC.Fields("agentsid")
            txtTerm.Text = clC.Fields("termmonth") & "/" & Format(CLng(clC.Fields("termmile")), "#,##0")
            If clC.Fields("effdate").Length > 0 Then
                txtEffDate.Text = Format(CDate(clC.Fields("effdate")), "M/d/yyyy")
            End If

            If clC.Fields("expdate").Length > 0 Then
                txtExpDate.Text = Format(CDate(clC.Fields("expdate")), "M/d/yyyy")
            End If
            GetProgram()
            GetPlanType()
            If clC.Fields("decpage").Length = 0 Then

                tcDec.Visible = False
                tcDesc2.Visible = False
            Else
                tcDec.Visible = True
                tcDesc2.Visible = True
                hlDec.NavigateUrl = clC.Fields("decpage")
                hlDec.Text = clC.Fields("decpage")
            End If
            If clC.Fields("tcpage").Length = 0 Then
                tcTC.Visible = False
                tcTC2.Visible = False
            Else
                tcTC.Visible = True
                tcTC2.Visible = True
                hlTC.NavigateUrl = clC.Fields("tcpage")
                hlTC.Text = clC.Fields("tcpage")
            End If
        End If
    End Sub

    Private Sub FillDealerInfo()
        OpenDealer()
        OpenAgent()
    End Sub

    Private Sub OpenAgent()
        If hfAgentID.Value.Length = 0 Then
            txtAgentName.Text = ""
            txtAgentNo.Text = ""
            Exit Sub
        End If

        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            txtAgentName.Text = clA.Fields("agentname")
            txtAgentNo.Text = clA.Fields("agentno")
        End If
    End Sub

    Private Sub OpenDealer()
        If hfDealerID.Value.Length = 0 Then
            txtDealerAddr1.Text = ""
            txtDealerAddr2.Text = ""
            txtDealerAddr3.Text = ""
            txtDealerName.Text = ""
            txtDealerNo.Text = ""
            txtDealerPhone.Text = ""
            Exit Sub
        End If
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            txtDealerName.Text = clD.Fields("dealername")
            txtDealerNo.Text = clD.Fields("dealerno")
            txtDealerAddr1.Text = clD.Fields("addr1")
            txtDealerAddr2.Text = clD.Fields("addr2")
            txtDealerAddr3.Text = clD.Fields("city") & ", " & clD.Fields("state") & " " & clD.Fields("zip")
            txtDealerPhone.Text = clD.Fields("phone")
        End If
    End Sub

    Private Sub GetPlanType()
        Dim SQL As String
        Dim clPT As New clsDBO
        SQL = "select * from plantype "
        SQL = SQL + "where plantypeid = " & hfPlanTypeID.Value
        clPT.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clPT.RowCount > 0 Then
            clPT.GetRow()
            txtPlan.Text = clPT.Fields("plantype")
        End If
    End Sub

    Private Sub GetProgram()
        Dim SQL As String
        Dim clP As New clsDBO
        SQL = "select * from program "
        SQL = SQL + "where programid = " & hfProgram.Value
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            txtProgram.Text = clP.Fields("programname")
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockScreen()
        End If
    End Sub

    Private Sub LockScreen()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from userinfo ui "
        SQL = SQL + "inner join usersecurityinfo usi on ui.userid = usi.userid "
        SQL = SQL + "where ui.userid = " & hfUserID.Value

        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("readonly")) Then
                btnUpdate.Visible = False
            End If
        End If
    End Sub

    Private Sub GetClaim()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clU As New clsDBO
        SQL = "select * from claimgap "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractID.Value = clR.Fields("contractid")
            pvVehicle.ContentUrl = "~/claim/ClaimVehicle.aspx?ContractID=" & hfContractID.Value
            pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value
            txtClaimNo.Text = clR.Fields("claimno")
            cboClaimStatus.SelectedValue = clR.Fields("status")
            txtClaimDate.Text = clR.Fields("claimdate")
            txtClaimantPhone.Text = clR.Fields("claimantphone")
            txtClaimantEmail.Text = clR.Fields("claimantemail")
            txtClaimAuthorPerson.Text = clR.Fields("ClaimAuthorizedPerson")
            txtNADAatLoss.Text = clR.Fields("nadaatloss")
            txtNADAatPurchase.Text = clR.Fields("nadaatpurchase")
            txtDeductForSalvage.Text = clR.Fields("deductforsalvage")
            chkRideshareCommercial.Checked = CBool(clR.Fields("ridesharecommercial"))
            txtLossMile.Text = clR.Fields("lossmile")
            If clR.Fields("lossdate").Length > 0 Then
                rdpLossDate.SelectedDate = clR.Fields("lossdate")
            Else
                rdpLossDate.Clear()
            End If
        End If
        SQL = "select * From claimgapattorney "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            lblAtt.Visible = True
            tsClaim.Tabs(7).BackColor = Color.Red
        End If

    End Sub

    Private Sub tsClaim_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsClaim.TabClick
        If tsClaim.SelectedTab.Value = "Customer" Then
            pvCustomer.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Vehicle" Then
            pvVehicle.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Status" Then
            pvStatus.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Documents" Then
            pvDocuments.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "POA" Then
            pvPOA.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "CoBuyer" Then
            pvCoBuyer.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Attorney" Then
            pvAttorney.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Notes" Then
            pvNotes.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Finance" Then
            pvFinance.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Calc" Then
            pvCalc.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Payment" Then
            pvPayment.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "LoanInfo" Then
            pvLoanInfo.Selected = True
        End If


    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgap "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("status") = cboClaimStatus.SelectedValue
            clR.Fields("claimantphone") = txtClaimantPhone.Text
            clR.Fields("claimantemail") = txtClaimantEmail.Text
            clR.Fields("claimauthorizedperson") = txtClaimAuthorPerson.Text
            clR.Fields("nadaatpurchase") = txtNADAatPurchase.Text
            clR.Fields("nadaatloss") = txtNADAatLoss.Text
            clR.Fields("ridesharecommercial") = chkRideshareCommercial.Checked
            clR.Fields("deductforsalvage") = txtDeductForSalvage.Text
            clR.Fields("lossdate") = rdpLossDate.SelectedDate
            clR.Fields("lossmile") = txtLossMile.Text
            clR.SaveDB()
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub


    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

End Class