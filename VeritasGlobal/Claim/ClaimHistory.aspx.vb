﻿Imports System.Configuration.ConfigurationManager
Imports System
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Telerik.Web.UI

Public Class ClaimHistory1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsHistory.ConnectionString = AppSettings("connstring")
        dsHistory2.ConnectionString = AppSettings("connstring")
        dsClaimOpenHistory.ConnectionString = AppSettings("connstring")
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            GetContractNo()
        End If

    End Sub

    Private Sub GetContractNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractID.Value = clR.Fields("contractid")
            gettotclaimspaid
        End If
    End Sub

    Private Sub GetTotClaimsPaid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(paidamt) as Amt from claimdetail "
        SQL = SQL + "where claimid in (select claimid from claim where contractid = " & hfContractID.Value & ") "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("amt").Length > 0 Then
                txtTotalClaimsPaid.Text = Format(CDbl(clR.Fields("amt")), "#,##0.00")
            Else
                txtTotalClaimsPaid.Text = "0.00"
            End If
        Else
            txtTotalClaimsPaid.Text = "0.00"
        End If

    End Sub

    Private Sub rgHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgHistory.SelectedIndexChanged
    End Sub

    Private Sub rgHistory_PreRender(sender As Object, e As EventArgs) Handles rgHistory.PreRender
        If Not Page.IsPostBack Then
            'rgHistory.MasterTableView.Items(0).Expanded = True
            'rgHistory.MasterTableView.Items(0).ChildItem.NestedTableViews(0).Items(0).Expanded = True
        End If
    End Sub

    Protected Sub hierarchyLoadMode_SelectedIndexChanged(sender As Object, e As EventArgs)
        SetHierarchyLoadMode(rgHistory.MasterTableView)
        rgHistory.Rebind()
    End Sub

    Private Sub SetHierarchyLoadMode(tableView As GridTableView)
        tableView.HierarchyLoadMode = GridChildLoadMode.ServerBind

        For Each detailView As GridTableView In tableView.DetailTables
            SetHierarchyLoadMode(detailView)
        Next
    End Sub

End Class