﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ClaimGAPPOA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        dsStates.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            FillPOA()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillPOA()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgappoa "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtFName.Text = clR.Fields("poaname")
            txtAddr1.Text = clR.Fields("poaaddr1")
            txtAddr2.Text = clR.Fields("poaaddr2")
            txtCity.Text = clR.Fields("poacity")
            cboState.SelectedValue = clR.Fields("poastate")
            txtZip.Text = clR.Fields("poazip")
            txtPhone.Text = clR.Fields("poaphone")
            txtEMail.Text = clR.Fields("poaemail")
        End If
    End Sub

    Private Sub btnUpdateCustomer_Click(sender As Object, e As EventArgs) Handles btnUpdateCustomer.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgappoa "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimgapid") = hfClaimID.Value
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("credate") = Today
        Else
            clR.GetRow()
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
        End If
        clR.Fields("poaname") = txtFName.Text
        clR.Fields("poaaddr1") = txtAddr1.Text
        clR.Fields("poaaddr2") = txtAddr2.Text
        clR.Fields("poacity") = txtCity.Text
        clR.Fields("poastate") = cboState.SelectedValue
        clR.Fields("poazip") = txtZip.Text
        clR.Fields("poaphone") = txtPhone.Text
        clR.Fields("poaemail") = txtEMail.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub
End Class