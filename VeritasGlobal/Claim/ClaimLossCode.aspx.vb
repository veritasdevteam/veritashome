﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimLossCode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsLossCode.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then

            GetServerInfo()
            pnlLossCode.Visible = False
            ReadOnlyButtons()
        End If
    End Sub
    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                Response.Redirect("~/users/claimssearch.aspx?sid=" & hfID.Value)
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Date.Today
        sEndDate = DateAdd("d", 1, Date.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAddLossCode_Click(sender As Object, e As EventArgs) Handles btnAddLossCode.Click
        pnlSeekLossCode.Visible = False
        pnlLossCode.Visible = True
    End Sub

    Private Sub btnCancelLossCode_Click(sender As Object, e As EventArgs) Handles btnCancelLossCode.Click
        pnlSeekLossCode.Visible = True
        pnlLossCode.Visible = False
    End Sub

    Private Sub btnSaveLossCode_Click(sender As Object, e As EventArgs) Handles btnSaveLossCode.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimlosscode "
        SQL = SQL + "where losscode = '" & txtLossCode.Text & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("losscode") = txtLossCode.Text
            clR.Fields("losscodedesc") = txtLossCodeDesc.Text
            clR.AddRow()
            clR.SaveDB()
        End If
        rgLossCode.Rebind()
        pnlSeekLossCode.Visible = True
        pnlLossCode.Visible = False
    End Sub

End Class