﻿Imports System.Configuration.ConfigurationManager

Public Class HCC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfClaimID.Value = Request.QueryString("claimid")
            FillScreen()
        End If
    End Sub

    Private Sub FillScreen()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimhcc "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lblDateSubmitted.Text = Format(CDate(clR.Fields("datesubmitted")), "M/d/yyyy")
            If clR.Fields("adjusterid").Length > 0 Then
                lblAdjusterName.Text = CalcUserName(clR.Fields("adjusterid"))
            End If
            CalcClaimInfo()
            CalcContractInfo()
            CalcDealer()
            CalcAgent()
            CalcServiceCenter()
            CalcInsCarrier()
            lblDaysIntoContract.Text = clR.Fields("daysintocontract")
            lblMilesIntoContract.Text = clR.Fields("milesintocontract")
            lblLimitofLiability.Text = clR.Fields("LimitOfLiability")
            If clR.Fields("nadavalue").Length > 0 Then
                lblNADA.Text = Format(CDbl(clR.Fields("NADAValue")), "#,##0.00")
            End If
            txtDescribeFailure.Text = clR.Fields("DescFailure")

            If clR.Fields("claimcost").Length > 0 Then
                lblClaimCost.Text = Format(CDbl(clR.Fields("claimcost")), "#,##0.00")
            End If
            If clR.Fields("LaborCostPerHour").Length > 0 Then
                lblLaborCostPerHour.Text = Format(CDbl(clR.Fields("laborcostperhour")), "#,##0.00")
            End If
            If clR.Fields("averagecost").Length > 0 Then
                lblAverageCost.Text = Format(CDbl(clR.Fields("averagecost")), "#,###.00")
            End If
        End If
    End Sub

    Private Sub CalcInsCarrier()
        Dim clI As New clsDBO
        Dim SQL As String
        SQL = "select * from inscarrier "
        SQL = SQL + "where inscarrierid = " & hfInsCarrierID.Value
        clI.OpenDB(SQL, AppSettings("connstring"))
        If clI.RowCount > 0 Then
            clI.GetRow()
            lblInsCarrier.Text = clI.Fields("inscarriername")
        End If
    End Sub

    Private Sub CalcServiceCenter()
        Dim clSC As New clsDBO
        Dim SQL As String
        SQL = "select * from servicecenter "
        SQL = SQL + "where servicecenterid = " & hfServiceCenterID.Value
        clSC.OpenDB(SQL, AppSettings("connstring"))
        If clSC.RowCount > 0 Then
            clSC.GetRow()
            lblServiceCenterName.Text = clSC.Fields("servicecentername")
        End If
    End Sub

    Private Sub CalcAgent()
        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            lblAgentName.Text = clA.Fields("agentname")
        End If
    End Sub

    Private Sub CalcDealer()
        Dim clD As New clsDBO
        Dim SQL As String
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            lblDealerName.Text = clD.Fields("dealername")
        End If
    End Sub

    Private Sub CalcClaimInfo()
        Dim clCl As New clsDBO
        Dim SQL As String
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCl.OpenDB(SQL, AppSettings("connstring"))
        If clCl.RowCount > 0 Then
            clCl.GetRow()
            hfContractID.Value = clCl.Fields("contractid")
            lblClaimNo.Text = clCl.Fields("claimno")
            hfServiceCenterID.Value = clCl.Fields("servicecenterid")
            If clCl.Fields("lossdate").Length > 0 Then
                lblClaimDate.Text = Format(CDate(clCl.Fields("lossdate")), "M/d/yyyy")
            End If
            lblCurrentMile.Text = clCl.Fields("lossmile")
        Else
            hfContractID.Value = 0
            hfServiceCenterID.Value = 0
        End If
    End Sub

    Private Sub CalcContractInfo()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lblContractNo.Text = clR.Fields("contractno")
            lblCustomerName.Text = clR.Fields("fname") & " " & clR.Fields("lname")
            hfDealerID.Value = clR.Fields("dealerid")
            hfAgentID.Value = clR.Fields("agentsid")
            hfInsCarrierID.Value = clR.Fields("inscarrierid")
            If clR.Fields("saledate").Length > 0 Then
                lblSaleDate.Text = Format(CDate(clR.Fields("saledate")), "M/d/yyyy")
            End If
            lblMakeModel.Text = clR.Fields("Make") & "/" & clR.Fields("model")
        Else
            hfAgentID.Value = 0
            hfDealerID.Value = 0
            hfInsCarrierID.Value = 0
        End If
    End Sub

    Private Function CalcUserName(xUserID As Long) As String
        Dim SQL As String
        Dim clU As New clsDBO
        CalcUserName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clU.OpenDB(SQL, AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            CalcUserName = clU.Fields("fname") & " " & clU.Fields("lname")
        End If
    End Function

End Class