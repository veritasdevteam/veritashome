﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports Telerik.Windows.Documents.Spreadsheet.Model

Public Class ClaimNotes1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsNoteType.ConnectionString = AppSettings("connstring")
        dsNote.ConnectionString = AppSettings("connstring")
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            GetServerInfo()
            pnlDetail.Visible = False
            pnlList.Visible = True
            'FillList()
            rgNote.Rebind()
            FillNoteHeader()
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnAddNote.Enabled = False
                btnSave.Enabled = False
            End If
        End If
    End Sub

    Private Sub FillNoteHeader()
        cboHeader.Items.Add("None")
        cboHeader.Items.Add("Statement")
        cboHeader.Items.Add("Parts Sourcing / Ordered")
        cboHeader.Items.Add("TSB")
        cboHeader.Items.Add("Contract Verification")
        cboHeader.Items.Add("Approval")
        cboHeader.Items.Add("Service Center Communication ")
        cboHeader.Items.Add("Management")
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillList()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select claimnoteid, ClaimNoteTypeDesc, notetext, cn.CreDate, username from claimnote cn "
        SQL = SQL + "left join userinfo ui on ui.userid = cn.creby "
        SQL = SQL + "left join claimnotetype nt on nt.claimnotetypeid = cn.claimnotetypeid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (cn.claimnotetypeid = 4 or cn.claimnotetypeid = 5 or cn.claimnotetypeid = 6 or cn.claimnotetypeid = 7 ) "
        SQL = SQL + "order by credate desc "
        rgNote.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgNote.DataBind()
    End Sub

    Private Sub btnAddNote_Click(sender As Object, e As EventArgs) Handles btnAddNote.Click
        pnlList.Visible = False
        pnlDetail.Visible = True
        cboNoteType.ClearSelection()
        cboHeader.Text = "None"
        txtNote.Content = ""
        hfClaimNoteID.Value = 0
        btnSave.Visible = True
    End Sub

    Private Sub rgNote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgNote.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        pnlDetail.Visible = True
        pnlList.Visible = False
        hfClaimNoteID.Value = rgNote.SelectedValue
        SQL = "select * from claimnote cn "
        SQL = SQL + "inner join userinfo ui on ui.userid = cn.creby "
        SQL = SQL + "where claimnoteid = " & hfClaimNoteID.Value & " "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            cboNoteType.SelectedValue = clR.Fields("claimnotetypeid")
            txtNote.Content = clR.Fields("note")
            txtCreBy.Text = clR.Fields("username")
            txtCreDate.Text = clR.Fields("credate")
            btnSave.Visible = True
            If hfUserID.Value <> clR.Fields("creby") Then
                btnSave.Visible = False
            End If
            If DateDiff(DateInterval.Hour, CDate(txtCreDate.Text), Now) > 24 Then
                btnSave.Visible = False
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgNote.SelectedIndexes.Clear()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimnote cn "
        SQL = SQL + "where claimnoteid = " & hfClaimNoteID.Value & " "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
        End If
        clR.Fields("claimid") = hfClaimID.Value
        clR.Fields("claimnotetypeid") = cboNoteType.SelectedValue
        Dim sTemp As String
        sTemp = txtNote.Text
        'txtNote.Text = txtNote.Text.Replace(vbCrLf, vbCr)
        If cboHeader.Text = "None" Then
            clR.Fields("note") = txtNote.Content
            clR.Fields("notetext") = txtNote.Text
        Else
            clR.Fields("note") = "***** " & cboHeader.Text & " *****" & vbCrLf & vbCrLf & txtNote.Content
            clR.Fields("notetext") = "***** " & cboHeader.Text & " *****" & vbCrLf & vbCrLf & txtNote.Text
        End If
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        If clR.RowCount = 0 Then
            clR.Fields("credate") = Now
            clR.Fields("creby") = hfUserID.Value
            clR.AddRow()
        End If
        clR.SaveDB()
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgNote.Rebind()
        'FillList()
        If chkReminder.Checked Then
            SQL = "select * from usermessage "
            SQL = SQL + "where idx = 0 "
            clR.OpenDB(SQL, AppSettings("connstring"))
            clR.NewRow()
            clR.Fields("toid") = hfUserID.Value
            clR.Fields("fromid") = hfUserID.Value
            clR.Fields("header") = "Reminder from Claim: " & GetClaimNo()
            clR.Fields("message") = sTemp
            clR.Fields("messagedate") = Now
            clR.AddRow()
            clR.SaveDB()
        End If
    End Sub

    Private Sub rgNote_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgNote.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgNote.ExportSettings.ExportOnlyData = False
            rgNote.ExportSettings.IgnorePaging = True
            rgNote.ExportSettings.OpenInNewWindow = True
            rgNote.ExportSettings.UseItemStyles = True
            rgNote.ExportSettings.Excel.FileExtension = "xlsx"
            rgNote.ExportSettings.FileName = "Note"
            rgNote.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgNote.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Function GetClaimNo() As String
        GetClaimNo = ""
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetClaimNo = clR.Fields("claimno")
        End If
    End Function

End Class