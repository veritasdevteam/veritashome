﻿Public Class ContractNote
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsNotes.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            pnlNote.Visible = True
            pnlChange.Visible = False
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            'Filltable()
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnSave.Enabled = False
                btnAdd.Enabled = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub Filltable()
        'Dim SQL As String
        'Dim clC As New clsDBO
        'SQL = "select contractnoteid, note, cre.fname + ' ' + cre.lname as creby, credate, mod.fname + ' ' + mod.lname as modby, moddate  from contractnote cn "
        'SQL = SQL + "left join userinfo cre on cre.UserID = cn.CreBy "
        'SQL = SQL + "left join userinfo mod on mod.UserID = cn.ModBy "
        'SQL = SQL + "where contractid = " & hfContractID.Value
        'rgNote.DataSource = clC.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgNote.DataBind()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        txtNote.ReadOnly = False
        pnlChange.Visible = True
        pnlNote.Visible = False
        hfContractNoteID.Value = 0
        FillNote()
    End Sub

    Private Sub rgNote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgNote.SelectedIndexChanged
        pnlChange.Visible = True
        pnlNote.Visible = False
        hfContractNoteID.Value = rgNote.SelectedValue
        FillNote()
    End Sub

    Private Sub FillNote()
        Dim SQL As String
        Dim clN As New clsDBO
        txtNote.ReadOnly = False
        btnSave.Visible = True
        SQL = "select * from contractnote "
        SQL = SQL + "where contractnoteid = " & hfContractNoteID.Value
        clN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clN.RowCount > 0 Then
            clN.GetRow()
            txtNote.Text = clN.Fields("note")
            txtModDate.Text = clN.Fields("moddate")
            txtCreDate.Text = clN.Fields("credate")
            If clN.Fields("creby") <> hfUserID.Value Then
                txtNote.ReadOnly = True
                btnSave.Visible = False
            End If
            If DateDiff(DateInterval.Hour, CDate(clN.Fields("moddate")), Now) > 24 Then
                txtNote.ReadOnly = True
                btnSave.Visible = False
            End If
            txtCreBy.Text = GetUserInfo(clN.Fields("creby"))
            If clN.Fields("modby").Length > 0 Then
                txtModBy.Text = GetUserInfo(clN.Fields("modby"))
            End If
        Else
            txtNote.Text = ""
            txtModDate.Text = ""
            txtCreDate.Text = ""
            txtCreBy.Text = ""
            txtModBy.Text = ""
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlNote.Visible = True
        pnlChange.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clN As New clsDBO
        btnSave.Visible = True
        SQL = "select * from contractnote "
        SQL = SQL + "where contractnoteid = " & hfContractNoteID.Value
        clN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clN.RowCount = 0 Then
            clN.NewRow()
        Else
            clN.GetRow()
        End If
        clN.Fields("contractid") = hfContractID.Value
        clN.Fields("note") = txtNote.Text
        If clN.RowCount = 0 Then
            clN.Fields("creby") = hfUserID.Value
            clN.Fields("credate") = Now
            clN.Fields("modby") = hfUserID.Value
            clN.Fields("moddate") = Now
            clN.AddRow()
        Else
            clN.Fields("modby") = hfUserID.Value
            clN.Fields("moddate") = Now
        End If
        clN.SaveDB()
        Filltable()
        pnlChange.Visible = False
        pnlNote.Visible = True
    End Sub

    Public Function GetUserInfo(xUserID As Long) As String
        GetUserInfo = ""
        Dim SQL As String
        Dim clU As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            GetUserInfo = clU.Fields("fname") & clU.Fields("lname")
        End If
    End Function

End Class