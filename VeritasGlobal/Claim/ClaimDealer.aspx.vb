﻿Public Class ClaimDealer1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        OpenClaim()
        OpenContract()
        OpenDealer()
        openagent
    End Sub

    Private Sub OpenAgent()
        If hfAgentID.Value.Length = 0 Then
            Exit Sub
        End If

        Dim clA As New clsDBO
        Dim SQL As String
        SQL = "select * from agents "
        SQL = SQL + "where agentid = " & hfAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            txtAgentName.Text = clA.Fields("agentname")
            txtAgentNo.Text = clA.Fields("agentno")
        End If
    End Sub

    Private Sub OpenDealer()
        If hfDealerID.Value.Length = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & hfDealerID.Value
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            txtDealerName.Text = clD.Fields("dealername")
            txtDealerNo.Text = clD.Fields("dealerno")
            txtAddr1.Text = clD.Fields("addr1")
            txtAddr2.Text = clD.Fields("addr2")
            txtAddr3.Text = clD.Fields("city") & ", " & clD.Fields("state") & " " & clD.Fields("zip")
            txtPhone.Text = clD.Fields("phone")
        End If
    End Sub

    Private Sub OpenClaim()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select contractid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfContractID.Value = clC.Fields("contractid")
        Else
            hfContractID.Value = 0
        End If
    End Sub

    Private Sub OpenContract()
        Dim SQL As String
        Dim clC As New clsDBO
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "select dealerid, agentsid from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfDealerID.Value = clC.Fields("dealerid")
            hfAgentID.Value = clC.Fields("agentsid")
        Else
            hfDealerID.Value = 0
            hfAgentID.Value = 0
        End If
    End Sub
End Class