﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI.Calendar

Public Class ClaimGAPFinance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAmortization.ConnectionString = AppSettings("connstring")

        If Not IsPostBack Then
            hfClaimID.Value = Request.QueryString("ClaimID")
            GetServerInfo()
            FillFinance()
            rgClaim.Rebind()
        End If
    End Sub

    Private Sub FillFinance()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapfinance "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtTradeIn.Text = clR.Fields("tradein")
            txtTotalCashPrice.Text = clR.Fields("totalcashprice")
            txtDownPayment.Text = clR.Fields("downpayment")
            txtAmtFinance.Text = clR.Fields("amountfinance")
            txtAnnualPer.Text = clR.Fields("annualpercent")
            If clR.Fields("loanstartdate").Length > 0 Then
                rdpLoanStartDate.SelectedDate = clR.Fields("loanstartdate")
            Else
                rdpLoanStartDate.Clear()
            End If
            If clR.Fields("purchasedate").Length > 0 Then
                rdpVehiclePurchaseDate.SelectedDate = clR.Fields("purchasedate")
            Else
                rdpVehiclePurchaseDate.Clear()
            End If
            txtNoPayments.Text = clR.Fields("nopayments")
            txtFinanceCharge.Text = clR.Fields("financecharge")
            txtTotalLoan.Text = clR.Fields("totalloan")
            txtPaymentAmt.Text = clR.Fields("paymentamt")
        Else
            txtTradeIn.Text = 0
            txtTotalCashPrice.Text = 0
            txtDownPayment.Text = 0
            txtAmtFinance.Text = 0
            txtAnnualPer.Text = 0
            txtNoPayments.Text = 0
            txtFinanceCharge.Text = 0
            txtTotalLoan.Text = 0
            txtPaymentAmt.Text = 0
        End If
    End Sub

    Private Sub CalcAmountFinance()
        If txtTotalCashPrice.Text.Length > 0 Then
            txtAmtFinance.Text = txtTotalCashPrice.Text
        End If
        If txtDownPayment.Text.Length > 0 Then
            txtAmtFinance.Text = CDbl(txtAmtFinance.Text) - CDbl(txtDownPayment.Text)
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub txtTradeIn_TextChanged(sender As Object, e As EventArgs) Handles txtTradeIn.TextChanged
        txtTotalCashPrice.Focus()
    End Sub

    Private Sub btnCalLoan_Click(sender As Object, e As EventArgs) Handles btnCalLoan.Click
        Dim dRatePerMonth As Double
        Dim dDiscount1 As Double
        Dim dDiscount2 As Double
        Dim dDiscount3 As Double
        Dim dDiscount4 As Double
        Dim dDiscount5 As Double
        Dim dPaymentAmt As Double
        Dim dAmtLoan As Double
        Dim dAmtInterest As Double
        dRatePerMonth = (CDbl(txtAnnualPer.Text) / 100) / 12
        dDiscount1 = 1 + dRatePerMonth
        dDiscount2 = (dDiscount1 ^ CDbl(txtNoPayments.Text))
        dDiscount3 = dDiscount2 - 1
        dDiscount4 = dRatePerMonth * dDiscount2
        dDiscount5 = dDiscount3 / dDiscount4
        dPaymentAmt = CDbl(txtAmtFinance.Text) / dDiscount5
        dAmtLoan = dPaymentAmt * CDbl(txtNoPayments.Text)
        dAmtInterest = dAmtLoan - CDbl(txtAmtFinance.Text)

        txtFinanceCharge.Text = dAmtInterest
        txtTotalLoan.Text = dAmtLoan
        txtPaymentAmt.Text = dPaymentAmt
        Dim clBA As New VeritasGlobalTools.clsBuildAmortization
        clBA.ClaimGapID = hfClaimID.Value
        clBA.PaymentAmt = Format(dPaymentAmt, "#.00")
        clBA.RatePerMonth = dRatePerMonth
        clBA.LoanAmt = CDbl(txtAmtFinance.Text)
        clBA.LoanAmtInt = Format(dAmtLoan, "#.00")
        clBA.StartDate = rdpLoanStartDate.SelectedDate
        clBA.NoPayment = txtNoPayments.Text
        clBA.BuildIt()
        rgClaim.Rebind()
    End Sub

    Private Sub txtDownPayment_TextChanged(sender As Object, e As EventArgs) Handles txtDownPayment.TextChanged
        CalcAmountFinance()
        txtAmtFinance.Focus()
    End Sub

    Private Sub rdpLoanStartDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpLoanStartDate.SelectedDateChanged
        txtTradeIn.Focus()
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "Amortization"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapfinance "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
            clR.Fields("claimgapid") = hfClaimID.Value
        End If
        clR.Fields("tradein") = txtTradeIn.Text
        clR.Fields("totalcashprice") = txtTotalCashPrice.Text
        clR.Fields("downpayment") = txtDownPayment.Text
        clR.Fields("amountfinance") = txtAmtFinance.Text
        clR.Fields("annualpercent") = txtAnnualPer.Text
        clR.Fields("nopayments") = txtNoPayments.Text
        clR.Fields("financecharge") = txtFinanceCharge.Text
        clR.Fields("totalloan") = txtTotalLoan.Text
        clR.Fields("paymentamt") = txtPaymentAmt.Text
        clR.Fields("loanstartdate") = rdpLoanStartDate.SelectedDate
        clR.Fields("purchasedate") = rdpVehiclePurchaseDate.SelectedDate
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub txtAnnualPer_TextChanged(sender As Object, e As EventArgs) Handles txtAnnualPer.TextChanged
        txtNoPayments.Focus()
    End Sub

    Private Sub rdpVehiclePurchaseDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpVehiclePurchaseDate.SelectedDateChanged
        rdpLoanStartDate.Focus()
    End Sub

    Private Sub txtTotalCashPrice_TextChanged(sender As Object, e As EventArgs) Handles txtTotalCashPrice.TextChanged
        txtDownPayment.Focus()
    End Sub

    Private Sub txtAmtFinance_TextChanged(sender As Object, e As EventArgs) Handles txtAmtFinance.TextChanged
        txtAnnualPer.Focus()
    End Sub
End Class