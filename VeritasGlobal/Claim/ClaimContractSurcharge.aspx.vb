﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimContractSurcharge
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            GetServerInfo()
            FillSurchargeGrid()
        End If
    End Sub

    Private Sub FillSurchargeGrid()
        getcontractid
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select ContractSurchargeID, surcharge from contractSurcharge cs
                inner join Surcharge s on s.SurchargeID = cs.surchargeid "
        SQL = SQL + "where contractid = " & hfContractID.Value
        rgSurcharge.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgSurcharge.DataBind()
    End Sub

    Private Sub GetContractID()
        Dim SQL As String
        Dim clCl As New clsDBO
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCl.OpenDB(SQL, AppSettings("connstring"))
        If clCl.RowCount > 0 Then
            clCl.GetRow()
            hfContractID.Value = clCl.Fields("contractid")
        Else
            hfContractID.Value = 0
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Date.Today
        sEndDate = DateAdd("d", 1, Date.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub


End Class