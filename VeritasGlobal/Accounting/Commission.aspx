﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Commission.aspx.vb" Inherits="VeritasGlobal.Commission" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Commission</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommission" runat="server" Text="Commission"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPaylink" runat="server" Text="Paylink/Mepco"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnWex" runat="server" Text="Wex"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlCommission">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAgent" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnAgentSearch" BackColor="#1eabe2" BorderColor="#1eabe2" runat="server" Text="Search Agent" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Cycle Month
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboCycleMonth" runat="server"></asp:DropDownList>
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:CheckBox ID="chkCancel" Text="Cancelled Contracts" runat="server" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnGetCommission" BackColor="#1eabe2" BorderColor="#1eabe2" runat="server" Text="Get Commission" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <telerik:RadGrid ID="rgCommission" AutoGenerateColumns="false" AllowFilteringByColumn="false" AllowSorting="true" 
                                                Height="500" ShowFooter="true" runat="server" AllowMultiRowEdit="true">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                                <MasterTableView>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="Payee" UniqueName="Payee" HeaderText="Payee"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amount"></telerik:GridBoundColumn>
                                                        <telerik:GridCheckBoxColumn DataField="Accept" UniqueName="Accept" HeaderText="Accept"></telerik:GridCheckBoxColumn>
                                                    </Columns>
                                                    <NestedViewSettings>
                                                        <ParentTableRelation>
                                                            <telerik:GridRelationFields MasterKeyField="Payee" DetailKeyField="Payee" />
                                                        </ParentTableRelation>
                                                    </NestedViewSettings>
                                                    <NestedViewTemplate>
                                                        <telerik:RadGrid ID="rgCommission" AutoGenerateColumns="false" AllowFilteringByColumn="false" AllowSorting="true" 
                                                            Height="500" ShowFooter="true" runat="server" AllowMultiRowEdit="true">
                                                            <ClientSettings>
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                            <MasterTableView>
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="CommissionPrimeID" UniqueName="CommissionPrimeID" Visible="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Payee" UniqueName="Payee" HeaderText="Payee"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amount"></telerik:GridBoundColumn>
                                                                    <telerik:GridCheckBoxColumn DataField="Delete" UniqueName="Delete" HeaderText="Accept"></telerik:GridCheckBoxColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                        </telerik:RadGrid>
                                                    </NestedViewTemplate>

                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:Table runat="server" Width ="1200">
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Total Commisision:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtTotalAmt" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    Cycle Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadDatePicker ID="rdpCycleDate" runat="server"></telerik:RadDatePicker>
                                                                </asp:TableCell>
                                                                <asp:TableCell HorizontalAlign="Right">
                                                                    <asp:Button ID="btnAddCycle" runat="server" Text="Add To Cylce" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlAgentSearch">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgAgent" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                            AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsAgent">
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="AgentID" PageSize="10" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="AgentID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentNo" HeaderText="Agent No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsAgent"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select agentid, agentno, agentname, city, state from agents 
                                                            where agentid in (select agentid from contractcommissions where finaldate is null 
                                                            or (CancelFinalDate is null and cancelamt > 0))" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnAgentClose" runat="server" Width="100" Text="Close" BackColor="#1eabe2" BorderColor="#1eabe2" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfError" runat="server" />
                <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
