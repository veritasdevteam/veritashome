﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AgentsNote

    '''<summary>
    '''pnlControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlControl As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgAgentNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgAgentNote As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''dsAgentNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dsAgentNote As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''pnlDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNote As Global.Telerik.Web.UI.RadTextBox

    '''<summary>
    '''txtCreDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCreDate As Global.Telerik.Web.UI.RadTextBox

    '''<summary>
    '''txtCreBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCreBy As Global.Telerik.Web.UI.RadTextBox

    '''<summary>
    '''txtModDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtModDate As Global.Telerik.Web.UI.RadTextBox

    '''<summary>
    '''txtModBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtModBy As Global.Telerik.Web.UI.RadTextBox

    '''<summary>
    '''chkClaimAlways control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkClaimAlways As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkClaimEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkClaimEntry As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkContractAlways control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkContractAlways As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkContractNewEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkContractNewEntry As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkContractCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkContractCancel As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfAgentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfAgentID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfAgentNoteID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfAgentNoteID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfUserID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfUserID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfToday control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfToday As Global.System.Web.UI.WebControls.HiddenField
End Class
