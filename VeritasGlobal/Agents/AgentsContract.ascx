﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AgentsContract.ascx.vb" Inherits="VeritasGlobal.AgentsContract" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadGrid ID="rgAgentContracts" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ContractID" PageSize="10" ShowFooter="true">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ContractID" ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="SqlDataSource1"
            ProviderName="System.Data.SqlClient" 
            SelectCommand="select contractid, contractno, fname, lname, City, State from contract 
            where (Agentsid = @AgentID or agentsid in (select agentid from agents where parentagentid = @Agentid)
            or agentsid in (select agentid from agents where parentagentid in (select agentid from agents where parentagentid = @agentid)))" runat="server">
            <SelectParameters>
                <asp:ControlParameter ControlID="hfAgentID" Name="AgentID" PropertyName="Value" Type="Int32" />
            </SelectParameters>
            </asp:SqlDataSource>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfAgentID" runat="server" />

