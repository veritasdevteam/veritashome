﻿Public Class SubAgentNote
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsAgentNote.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            hfToday.Value = Format(Today, "M/d/yyyy")
            hfSubAgentID.Value = Request.QueryString("subagentID")
            pnlControl.Visible = True
            pnlDetail.Visible = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub rgSubAgentNote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgSubAgentNote.SelectedIndexChanged
        pnlControl.Visible = False
        pnlDetail.Visible = True
        hfSubAgentNoteID.Value = rgSubAgentNote.SelectedValue
        FillNote()
    End Sub

    Private Sub FillNote()
        Dim SQL As String
        Dim clRN As New clsDBO
        SQL = "select note, createdate, moddate, cre.email as CEmail, mod.email as MEMail, claimalways, claimnewentry from subagentnote rn "
        SQL = SQL + "left join userinfo cre on rn.createby = cre.userid "
        SQL = SQL + "left join userinfo mod on mod.userid = rn.modby "
        SQL = SQL + "where subagentnoteid = " & hfSubAgentNoteID.Value

        clRN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRN.RowCount > 0 Then
            clRN.GetRow()
            txtCreBy.Text = clRN.Fields("CEMail")
            txtCreDate.Text = clRN.Fields("createdate")
            txtModBy.Text = clRN.Fields("MEMail")
            txtModDate.Text = clRN.Fields("moddate")
            txtNote.Text = clRN.Fields("note")
            chkClaimAlways.Checked = CBool(clRN.Fields("claimalways"))
            chkClaimNewEntry.Checked = CBool(clRN.Fields("claimnewentry"))
        Else
            txtCreBy.Text = ""
            txtCreDate.Text = ""
            txtModBy.Text = ""
            txtModDate.Text = ""
            txtNote.Text = ""
            chkClaimNewEntry.Checked = False
            chkClaimAlways.Checked = False
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgSubAgentNote.Rebind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clRN As New clsDBO
        SQL = "select * from subAgentnote "
        SQL = SQL + "where subagentnoteid = " & hfSubAgentNoteID.Value
        clRN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clRN.RowCount = 0 Then
            clRN.NewRow()
            clRN.Fields("createby") = hfUserID.Value
            clRN.Fields("createdate") = Today
        Else
            clRN.GetRow()
            clRN.Fields("modby") = hfUserID.Value
            clRN.Fields("moddate") = Today
        End If
        clRN.Fields("note") = txtNote.Text
        clRN.Fields("subagentid") = hfSubAgentID.Value
        clRN.Fields("claimalways") = chkClaimAlways.Checked
        clRN.Fields("claimnewentry") = chkClaimNewEntry.Checked
        If clRN.RowCount = 0 Then
            clRN.AddRow()
        End If
        clRN.SaveDB()
        pnlControl.Visible = True
        pnlDetail.Visible = False
        rgSubAgentNote.Rebind()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlControl.Visible = False
        pnlDetail.Visible = True
        hfSubAgentNoteID.Value = 0
        FillNote()
    End Sub
End Class