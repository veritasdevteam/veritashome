﻿Public Class SubAgentModify
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsAgentStatus.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        hfSubAgentID.Value = Request.QueryString("subagentID")
        pnlModify.Visible = True
        pnlSearch.Visible = False
        If Not IsPostBack Then
            FillSubAgent()
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillSubAgent()
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select * from Subagents where subagentid = " & hfSubAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            txtAddr1.Text = clA.Fields("addr1")
            txtAddr2.Text = clA.Fields("addr2")
            txtSubAgentName.Text = clA.Fields("subagentname")
            txtSubAgentNo.Text = clA.Fields("subagentno")
            txtCity.Text = clA.Fields("city")
            txtEMail.Text = clA.Fields("email")
            txtPhone.Text = clA.Fields("phone")
            txtContact.Text = clA.Fields("contact")
            txtDBA.Text = clA.Fields("dba")
            txtEIN.Text = clA.Fields("ein")
            cboAgentStatus.SelectedValue = clA.Fields("statusid")
            hfAgentID.Value = clA.Fields("agentid")
            txtAgent.Text = GetAgentInfo(clA.Fields("agentid"))
            cboState.SelectedValue = clA.Fields("state")
            txtZip.Text = clA.Fields("zip")
        Else
            txtAddr1.Text = ""
            txtAddr2.Text = ""
            txtSubAgentName.Text = ""
            txtSubAgentNo.Text = ""
            txtCity.Text = ""
            txtDBA.Text = ""
            txtEIN.Text = ""
            txtContact.Text = ""
            hfAgentID.Value = 0
            txtEMail.Text = ""
            txtAgent.Text = ""
            cboState.Text = ""
            txtZip.Text = ""
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        FillSubAgent()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clA As New clsDBO

        SQL = "select * from subagents where subagentid = " & hfSubAgentID.Value
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            clA.Fields("moddate") = Today
            clA.Fields("modby") = hfUserID.Value
        Else
            clA.NewRow()
            clA.Fields("credate") = Today
            clA.Fields("creby") = hfUserID.Value
        End If
        clA.Fields("addr1") = txtAddr1.Text
        clA.Fields("addr2") = txtAddr2.Text
        clA.Fields("city") = txtCity.Text
        clA.Fields("email") = txtEMail.Text
        clA.Fields("subagentname") = txtSubAgentName.Text
        clA.Fields("subagentno") = txtSubAgentNo.Text
        clA.Fields("dba") = txtDBA.Text
        clA.Fields("ein") = txtEIN.Text
        clA.Fields("agentid") = hfAgentID.Value
        clA.Fields("statusid") = cboAgentStatus.SelectedValue
        clA.Fields("state") = cboState.SelectedValue
        clA.Fields("phone") = txtPhone.Text
        clA.Fields("contact") = txtContact.Text
        clA.Fields("zip") = txtZip.Text
        If clA.RowCount = 0 Then
            clA.AddRow()
        End If
        clA.SaveDB()
        redirectsubagent
    End Sub

    Private Sub RedirectSubAgent()
        If hfSubAgentID.Value <> "0" Then
            Response.Redirect("subagent.aspx?sid=" & hfID.Value & "&subAgentID=" & hfSubAgentID.Value)
        Else
            Response.Redirect("subagent.aspx?sid=" & hfID.Value & "&subAgentID=" & getsubagentid)
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        pnlModify.Visible = False
        pnlSearch.Visible = True
    End Sub

    Private Sub rgAgents_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgAgents.SelectedIndexChanged
        hfAgentID.Value = rgAgents.SelectedValue
        txtAgent.Text = GetAgentInfo(hfAgentID.Value)
        pnlModify.Visible = True
        pnlSearch.Visible = False
    End Sub

    Private Function GetSubAgentID() As Long
        GetSubAgentID = 0
        Dim SQL As String
        Dim clA As New clsDBO
        SQL = "select max(subagentid) as subagentid from subagents "
        SQL = SQL + "where creby = " & hfUserID.Value & " "
        SQL = SQL + ""
        clA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clA.RowCount > 0 Then
            clA.GetRow()
            GetSubAgentID = clA.Fields("subagentid")
        End If
    End Function

End Class