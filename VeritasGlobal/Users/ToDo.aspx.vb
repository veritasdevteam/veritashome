﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ToDo1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsMessage.ConnectionString = AppSettings("connstring")
        dsUserInfo.ConnectionString = AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            pnlDetail.Visible = False
            pnlList.Visible = True
            pnlSeekUser.Visible = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUserSettings.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("usersettings") = True Then
                btnUserSettings.Enabled = True
            End If
        End If
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnChangePassword_Click(sender As Object, e As EventArgs) Handles btnChangePassword.Click
        Response.Redirect("~/users/ChangePassword.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUserSettings_Click(sender As Object, e As EventArgs) Handles btnUserSettings.Click
        Response.Redirect("~/users/usersettings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnToDoCreate_Click(sender As Object, e As EventArgs) Handles btnToDoCreate.Click
        Response.Redirect("~/users/todoreader.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAddMessage_Click(sender As Object, e As EventArgs) Handles btnAddMessage.Click
        pnlDetail.Visible = True
        pnlList.Visible = False
        ClearDetail()
    End Sub

    Private Sub ClearDetail()
        hfIDX.Value = 0
        hfToID.Value = 0
        txtFrom.Text = GetUserName()
        txtTo.Text = ""
        txtHeader.Text = ""
        txtMessage.Text = ""
        txtMessageDate.Text = ""
        btnSendMessage.Visible = True
    End Sub

    Private Sub rgMessage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgMessage.SelectedIndexChanged
        pnlDetail.Visible = True
        pnlList.Visible = False
        txtFrom.Text = GetUserName()
        hfIDX.Value = rgMessage.SelectedValue
        btnSendMessage.Visible = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where idx = " & hfIDX.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfToID.Value = clR.Fields("toid")
            txtTo.Text = GetToName()
            txtHeader.Text = clR.Fields("header")
            txtMessage.Text = clR.Fields("message")
            txtMessageDate.Text = clR.Fields("messagedate")
        End If
    End Sub

    Private Sub btnSeekUser_Click(sender As Object, e As EventArgs) Handles btnSeekUser.Click
        pnlDetail.Visible = False
        pnlSeekUser.Visible = True

    End Sub

    Private Sub rgUserInfo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgUserInfo.SelectedIndexChanged
        hfToID.Value = rgUserInfo.SelectedValue
        txtTo.Text = GetToName()
        pnlDetail.Visible = True
        pnlSeekUser.Visible = False
    End Sub

    Private Sub btnCancelMessage_Click(sender As Object, e As EventArgs) Handles btnCancelMessage.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSendMessage_Click(sender As Object, e As EventArgs) Handles btnSendMessage.Click
        If hfIDX.Value <> "0" Then
            Exit Sub
        End If
        If hfToID.Value = "0" Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where idx = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("toid") = hfToID.Value
            clR.Fields("fromid") = hfUserID.Value
            clR.Fields("header") = txtHeader.Text
            clR.Fields("message") = txtMessage.Text
            clR.Fields("messagedate") = Now
            clR.AddRow()
            clR.SaveDB()
        End If
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgMessage.Rebind()
    End Sub

    Private Function GetToName()
        Dim SQL As String
        Dim clR As New clsDBO
        GetToName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfToID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetToName = clR.Fields("fname") & " " & clR.Fields("lname")
        End If
    End Function

    Private Function GetUserName() As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetUserName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            GetUserName = clR.Fields("fname") & " " & clR.Fields("lname")

        End If
    End Function

    Private Sub btnToDoReader_Click(sender As Object, e As EventArgs) Handles btnToDoReader.Click
        Response.Redirect("~/users/todoreader1.aspx?sid=" & hfID.Value)
    End Sub

End Class