﻿Public Class ContractCost
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            FillScreen()
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillScreen()
        Dim clC As New clsDBO
        Dim SQL As String
        SQL = "select moxydealercost, discountamt, customercost, markup "
        SQL = SQL + "from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("customercost").Length = 0 Then
                clC.Fields("customercost") = 0
            End If
            If clC.Fields("markup").Length = 0 Then
                clC.Fields("markup") = 0
            End If
            If clC.Fields("moxydealercost").Length = 0 Then
                clC.Fields("moxydealercost") = 0
            End If
            If clC.Fields("discountamt").Length = 0 Then
                clC.Fields("discountamt") = 0
            End If
            txtCustomerCost.Text = Format(CDbl(clC.Fields("customercost")), "#,##0.00")
            txtMarkup.Text = Format(CDbl(clC.Fields("markup")), "#,##0.00")
            txtDealerCost.Text = Format(CDbl(clC.Fields("moxydealercost")), "#,##0.00")
            txtDiscountAmt.Text = Format(CDbl(clC.Fields("discountamt")), "#,##0.00")
            txtAddMarkup.Text = Format(CDbl(clC.Fields("customercost")) - CDbl(clC.Fields("markup")) - CDbl(clC.Fields("moxydealercost")) + CDbl(clC.Fields("discountamt")), "#,##0.00")
        End If
    End Sub

End Class