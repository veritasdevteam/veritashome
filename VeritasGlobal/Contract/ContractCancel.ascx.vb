﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Windows.Documents.Spreadsheet.Expressions.Functions

Public Class ContractCancel
    Inherits System.Web.UI.UserControl
    Private dCancelFee As Double
    Private dCancelPer As Double
    Private dTermFactor As Double
    Private dCancelFactor As Double
    Private dCancelAmt As Double
    Private dDealerAmt As Double
    Private dAdminAmt As Double
    Private dClaimAmt As Double
    Private bOOB As Boolean
    Private bRegulatedState As Boolean

    Public Sub FillCancellation()
        FillCancel()
        If Not CheckCancel() Then
            CheckMoxy()
        Else
            trMoxyCancel.Visible = False
        End If
        If Not IsPostBack Then
            ReadOnlyButtons()
            lblError.Visible = False
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnActivate.Enabled = False
                btnCancel.Enabled = False
                btnInvalid.Enabled = False
                btnMoxy.Enabled = False
                btnPay.Enabled = False
                btnQuote.Enabled = False
                btnUnable.Enabled = False
                btnSaveCancel.Enabled = False
                btnUpdateNote.Enabled = False
            End If
        End If
    End Sub

    Private Function CheckCancel() As Boolean
        CheckCancel = False
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "and status = 'Cancelled' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            CheckCancel = True
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsPayType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            hfContractID.Value = Request.QueryString("contractid")
            tcSaveCancel.Visible = False
            CalcContractStatus()
            rbDealer.Checked = True
            pnlOther.Visible = False
            If CheckDealerOOB() Then
                rbOther.Checked = True
                pnlOther.Visible = True
            End If
            pnlPaylink.Visible = False
            ShowActivateCancel()
            FillCancel()
            If Not CheckCancel() Then
                CheckMoxy()
            Else
                trMoxyCancel.Visible = False
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = DateTime.Today
        sEndDate = DateAdd("d", 1, DateTime.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Function CheckDealerOOB() As Boolean
        CheckDealerOOB = False
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select c.dealerid from dealer d "
        SQL = SQL + "inner join contract c on d.dealerid = c.dealerid "
        SQL = SQL + "where c.contractid = " & hfContractID.Value & " "
        SQL = SQL + "and dealerstatusid = 5 "
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            CheckDealerOOB = True
        End If
    End Function

    Private Sub CheckMoxy()
        Dim SQL As String
        Dim clCCM As New clsDBO
        SQL = "select * from contractcancelmoxy "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "and processedcancel = 0 "
        SQL = SQL + "and cancelstatus = 'cancelled' "
        clCCM.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCCM.RowCount > 0 Then
            trMoxyCancel.Visible = True
        Else
            trMoxyCancel.Visible = False
        End If
    End Sub

    Private Sub ShowActivateCancel()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        btnActivate.Visible = False
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("Status") = "Cancelled" Or clC.Fields("status") = "Cancelled Before Paid" Or clC.Fields("status") = "Expired" Then
                If CheckActivateContract() Then
                    btnActivate.Visible = True
                End If
            End If
        End If
    End Sub

    Private Function CheckActivateContract() As Boolean
        CheckActivateContract = False
        Dim SQL As String
        Dim clUSI As New clsDBO
        SQL = "select activatecontract from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clUSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            CheckActivateContract = CBool(clUSI.Fields("activatecontract"))
        End If
    End Function

    Private Sub CalcContractStatus()
        Dim SQL As String
        Dim clC As New clsDBO
        tcCancel.Visible = True
        tcQuote.Visible = True
        tcRequest.Visible = True
        tcInvalid.Visible = True

        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("status") = "Paid" Then
                pnlCalcCancellation.Visible = True
                trCancelCalc.Visible = True
                tcInvalid.Visible = False
            Else
                tcInvalid.Visible = True
            End If
            If clC.Fields("status") = "Cancelled" Then
                tcCancel.Visible = False
                tcQuote.Visible = False
                tcRequest.Visible = False
                tcInvalid.Visible = False
            End If
            If clC.Fields("state").ToLower = "fl" Then
                trFL.Visible = True
                If clC.Fields("lienholder").ToLower = "paylink" Then
                    rbFLCustomer.Checked = False
                    rbFLAdmin.Checked = True
                Else
                    rbFLCustomer.Checked = True
                    rbFLAdmin.Checked = False
                End If
            Else
                trFL.Visible = False
            End If
            CheckSecurity()
            FillCancel()
            GetLienholder2()
        End If
    End Sub

    Private Sub CheckSecurity()
        Dim clC As New clsDBO
        Dim clUSI As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
        Else
            Exit Sub
        End If
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        tcCancel.Visible = True
        tcQuote.Visible = True
        tcRequest.Visible = True
        tcInvalid.Visible = True
        clUSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
            If clC.Fields("programid") >= 47 Then
                If clC.Fields("programid") < 61 Then
                    tcCancel.Visible = False
                    tcQuote.Visible = False
                    tcRequest.Visible = False
                    tcInvalid.Visible = False
                    If clUSI.Fields("CancelGap") = True Then
                        tcCancel.Visible = True
                        tcQuote.Visible = False
                        tcRequest.Visible = False
                        tcInvalid.Visible = False
                        txtRequestDate.ReadOnly = False
                        txtTermPercent.ReadOnly = False
                        txtCancelPercent.ReadOnly = False
                        txtTermPercent.ReadOnly = False
                        txtCancelStatus.ReadOnly = False
                        txtClaimAmt.ReadOnly = False
                        txtCancelFee.ReadOnly = False
                        txtLienholder.ReadOnly = False
                        txtQuoteDate.ReadOnly = False
                        txtCancelDate.ReadOnly = False
                        txtCustomerRefund.ReadOnly = False
                        txtFromAdmin.ReadOnly = False
                        txtFromDealer.ReadOnly = False
                        Exit Sub
                    End If
                End If
            End If
            If clUSI.Fields("cancellation") = False Then
                tcCancel.Visible = False
                tcQuote.Visible = False
                tcRequest.Visible = False
                tcInvalid.Visible = False
                tcSaveCancel.Visible = False
            Else
                If clUSI.Fields("cancelmodification") = True Then
                    If clC.Fields("status") = "Cancelled" Then
                        tcCancel.Visible = False
                        tcQuote.Visible = False
                        tcRequest.Visible = False
                        tcInvalid.Visible = False
                        txtRequestDate.ReadOnly = False
                        txtTermPercent.ReadOnly = False
                        txtCancelPercent.ReadOnly = False
                        txtTermPercent.ReadOnly = False
                        txtCancelStatus.ReadOnly = False
                        txtClaimAmt.ReadOnly = False
                        txtCancelFee.ReadOnly = False
                        txtLienholder.ReadOnly = False
                        txtQuoteDate.ReadOnly = False
                        txtCancelDate.ReadOnly = False
                        txtCustomerRefund.ReadOnly = False
                        txtFromAdmin.ReadOnly = False
                        txtFromDealer.ReadOnly = False
                        tcSaveCancel.Visible = True
                    End If
                Else
                    If clUSI.Fields("cancellation") = False Then
                        tcCancel.Visible = False
                        tcQuote.Visible = False
                        tcRequest.Visible = False
                        tcInvalid.Visible = False
                    End If
                End If
            End If
            If clUSI.Fields("cancelpay") = True Then
                tcPayment.Visible = True
            Else
                tcPayment.Visible = False
            End If
        End If
    End Sub

    Private Sub FillCancel()
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
            txtCancelStatus.Text = clCC.Fields("cancelstatus")
            If clCC.Fields("termfactor").Length > 0 Then
                txtTermPercent.Text = Format(CDbl(clCC.Fields("termfactor")), "##.000%")
            End If
            txtCancelPercent.Text = Format(CDbl(clCC.Fields("cancelfactor")), "##.000%")
            If clCC.Fields("canceleffdate").Length > 0 Then
                rdpProcessDate.SelectedDate = Format(CDate(clCC.Fields("canceleffdate")), "M/d/yyyy")
            End If
            If clCC.Fields("quotedate").Length > 0 Then
                txtQuoteDate.Text = Format(CDate(clCC.Fields("quotedate")), "M/d/yyyy")
            End If
            If clCC.Fields("requestdate").Length > 0 Then
                txtRequestDate.Text = Format(CDate(clCC.Fields("requestdate")), "M/d/yyyy")
            End If
            If clCC.Fields("canceldate").Length > 0 Then
                txtCancelDate.Text = Format(CDate(clCC.Fields("Canceldate")), "M/d/yyyy")
            End If
            cboPayType.SelectedValue = 2
            txtCancelFee.Text = Format(CDbl(clCC.Fields("cancelfeeamt")), "#,##0.00")
            txtClaimAmt.Text = Format(CDbl(clCC.Fields("ClaimAmt")), "#,##0.00")
            Dim dCustRefund As Double
            Dim clC As New clsDBO
            SQL = "select * from contract "
            SQL = SQL + "where contractid = " & clCC.Fields("contractid")
            clC.OpenDB(SQL, AppSettings("connstring"))
            If clC.RowCount > 0 Then
                clC.GetRow()
                CheckRegulatedState(clC.Fields("state"))
                GetDealerInfo(clC.Fields("dealerid"))
            End If
            If bOOB Then
                If bRegulatedState Then
                    'If CDbl(clCC.Fields("dealeramt")) > CDbl(clCC.Fields("adminamt")) Then
                    dCustRefund = CDbl(clCC.Fields("adminamt"))
                    dAdminAmt = CDbl(clCC.Fields("adminamt"))
                Else
                    dCustRefund = CDbl(clCC.Fields("adminamt"))
                    dAdminAmt = CDbl(clCC.Fields("adminamt"))
                    'End If
                End If
            Else
                dCustRefund = CDbl(clCC.Fields("dealeramt")) + CDbl(clCC.Fields("adminamt"))
                dAdminAmt = CDbl(clCC.Fields("adminamt"))
            End If

            txtCustomerRefund.Text = Format(dCustRefund, "#,##0.00")
            txtFromAdmin.Text = Format(dAdminAmt, "#,##0.00")
            txtFromDealer.Text = Format(CDbl(clCC.Fields("dealeramt")), "#,##0.00")
            txtCancelStatus.Text = clCC.Fields("cancelstatus")
            txtPaymentAmt.Text = clCC.Fields("adminamt")
            If clCC.Fields("cancelpaiddate").Length > 0 Then
                txtPayDate.SelectedDate = clCC.Fields("cancelpaiddate")
            End If
            If clCC.Fields("cancelpaidamt").Length > 0 Then
                txtPaymentAmt.Text = clCC.Fields("cancelpaidamt")
            End If
            txtPaymentInfo.Text = clCC.Fields("paymentinfo")
            If clCC.Fields("paytypeid").Length > 0 Then
                cboPayType.SelectedValue = clCC.Fields("paytypeid")
            End If
            txtNote.Text = clCC.Fields("note")
            rbOther.Checked = clCC.Fields("Other")
            rbCustomer.Checked = clCC.Fields("customer")
            rbDealer.Checked = clCC.Fields("dealer")
            txtGrossDealer.Text = Format(CDbl(clCC.Fields("grossdealer")), "#,##0.00")
            txtGrossCustomer.Text = Format(CDbl(clCC.Fields("grosscustomer")), "#,##0.00")
            txtGrossAdmin.Text = Format(CDbl(clCC.Fields("grossadmin")), "#,##0.00")
            chkDealerCancel.Checked = CBool(clCC.Fields("DealerCancel"))
            If rbOther.Checked Then
                pnlOther.Visible = True
                Dim clP As New VeritasGlobalTools.clsPayee
                clP.PayeeID = CLng(clCC.Fields("payeeid"))
                clP.OpenPayeeInfo()
                txtAddr1.Text = clP.Addr1
                txtAddr2.Text = clP.Addr2
                txtCity.Text = clP.City
                cboState.SelectedValue = clP.State
                txtPhone.Text = clP.Phone
                txtZip.Text = clP.Zip
                txtCompanyName.Text = clP.CompanyName
                txtFName.Text = clP.FName
                txtLName.Text = clP.LName
            End If
            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value
        End If
    End Sub

    Private Function CheckCancel100() As Boolean
        CheckCancel100 = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("cancel100")) Then
                CheckCancel100 = True
            End If
        End If
    End Function

    Private Sub btnQuote_Click(sender As Object, e As EventArgs) Handles btnQuote.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        If Not CheckCancel100() Then
            If CheckClaim() Then
                lblError.Visible = True
                lblError.Text = "Claim Is Open Or Paid after the Cancel Effective Date."
                Exit Sub
            Else
                lblError.Visible = False
            End If
        Else
            lblError.Visible = False
        End If
        CalcCancellation(True)
        GetLienholder2()
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If

        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("cancelstatus") = "Quote"
        clCC.Fields("termfactor") = dTermFactor
        clCC.Fields("cancelfactor") = dCancelFactor
        clCC.Fields("canceleffdate") = rdpProcessDate.SelectedDate
        clCC.Fields("quotedate") = DateTime.Today
        clCC.Fields("adminamt") = dAdminAmt
        clCC.Fields("claimamt") = txtClaimAmt.Text
        clCC.Fields("dealeramt") = dDealerAmt
        'clCC.Fields("CancelPaidAmt") = dCancelAmt
        clCC.Fields("cancelfeeamt") = dCancelFee
        clCC.Fields("dealer") = rbDealer.Checked
        clCC.Fields("customer") = rbCustomer.Checked
        clCC.Fields("Other") = rbOther.Checked
        txtQuoteDate.Text = rdpProcessDate.SelectedDate
        clCC.Fields("cancelinfo") = hfCancelInfo.Value
        clCC.Fields("Note") = txtNote.Text
        clCC.Fields("GrossCustomer") = txtGrossCustomer.Text
        clCC.Fields("grossadmin") = txtGrossAdmin.Text
        clCC.Fields("grossdealer") = txtGrossDealer.Text
        clCC.Fields("DealerCancel") = chkDealerCancel.Checked
        Dim clP As New VeritasGlobalTools.clsPayee
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = DateTime.Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = DateTime.Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
        hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value
    End Sub

    Private Sub CalcCancellation(xQuote As Boolean)
        Dim clCancel As New VeritasGlobalTools.clsCancellation
        clCancel.CancelDate = rdpProcessDate.SelectedDate
        clCancel.ContractID = hfContractID.Value
        clCancel.Quote = xQuote
        clCancel.CalculateCancellation()
        clCancel.FLAdmin = rbFLAdmin.Checked
        clCancel.FLCustomer = rbFLCustomer.Checked
        dTermFactor = clCancel.CancelTermPer
        dCancelFactor = clCancel.CancelPer
        dCancelFee = clCancel.CancelFee
        dAdminAmt = clCancel.AdminAmt
        dDealerAmt = clCancel.DealerAmt
        dCancelAmt = clCancel.DealerAmt + clCancel.AdminAmt

        txtCancelFee.Text = Format(clCancel.CancelFee, "#,##0.00")
        txtCancelPercent.Text = Format(clCancel.CancelPer, "0.###%")
        txtClaimAmt.Text = Format(clCancel.Claims, "#,##0.00")
        txtCustomerRefund.Text = Format(clCancel.CustomerCost, "#,##0.00")
        txtFromAdmin.Text = Format(clCancel.AdminAmt, "#,##0.00")
        txtFromDealer.Text = Format(clCancel.DealerAmt, "#,##0.00")
        txtTermPercent.Text = Format(clCancel.CancelTermPer, "0.###%")
        hfCancelInfo.Value = clCancel.CancelInfo
        txtGrossCustomer.Text = Format(clCancel.GrossCustomer, "#,##0.00")
        txtGrossDealer.Text = Format(clCancel.GrossDealer, "#,##0.00")
        txtGrossAdmin.Text = Format(clCancel.GrossAdmin, "#,##0.00")

    End Sub

    Private Sub CalcCancellation2()
        Dim clCancel As New VeritasGlobalTools.clsCancellation
        clCancel.CancelDate = rdpProcessDate.SelectedDate
        clCancel.ContractID = hfContractID.Value
        clCancel.CalculateCancellation()

        dTermFactor = clCancel.CancelTermPer
        dCancelFactor = clCancel.CancelPer
        dCancelFee = clCancel.CancelFee
        dAdminAmt = clCancel.AdminAmt
        dDealerAmt = clCancel.DealerAmt
        dCancelAmt = clCancel.DealerAmt + clCancel.AdminAmt

        txtCancelFee.Text = Format(clCancel.CancelFee, "#,##0.00")
        txtCancelPercent.Text = Format(clCancel.CancelPer, "0.###%")
        txtClaimAmt.Text = Format(clCancel.Claims, "#,##0.00")
        txtCustomerRefund.Text = Format(clCancel.DealerAmt + clCancel.AdminAmt, "#,##0.00")
        txtFromAdmin.Text = Format(clCancel.AdminAmt, "#,##0.00")
        txtFromDealer.Text = Format(clCancel.DealerAmt, "#,##0.00")
        txtTermPercent.Text = Format(clCancel.CancelTermPer, "0.###%")
        txtGrossAdmin.Text = Format(clCancel.GrossAdmin, "#,##0.00")
        txtGrossCustomer.Text = Format(clCancel.GrossCustomer, "#,##0.00")
        txtGrossDealer.Text = Format(clCancel.GrossDealer, "#,##0.00")
    End Sub

    Private Sub GetDealerInfo(xDealerID As Long)
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & xDealerID
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            If clD.Fields("dealerstatusid") = 5 Then
                bOOB = True
            End If
        End If
    End Sub

    Private Sub CheckRegulatedState(xState As String)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from cancelstaterules "
        SQL = SQL + "where state = '" & xState & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("regulated")) Then
                bRegulatedState = True
            End If
        End If
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        If Not CheckCancel100() Then
            If CheckClaim() Then
                lblError.Visible = True
                lblError.Text = "Claim is Open or Paid after the Cancel Effective Date."
                Exit Sub
            Else
                lblError.Visible = False
            End If
        Else
            lblError.Visible = False
        End If

        CalcCancellation(True)
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        GetLienholder2()
        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("cancelstatus") = "Cancelled"
        clCC.Fields("termfactor") = CDbl(txtTermPercent.Text.Replace("%", "")) / 100
        clCC.Fields("claimamt") = txtClaimAmt.Text
        clCC.Fields("cancelfactor") = CDbl(txtCancelPercent.Text.Replace("%", "")) / 100
        clCC.Fields("canceleffdate") = rdpProcessDate.SelectedDate
        clCC.Fields("canceldate") = DateTime.Today
        clCC.Fields("adminamt") = txtFromAdmin.Text
        clCC.Fields("dealeramt") = txtFromDealer.Text
        clCC.Fields("dealer") = rbDealer.Checked
        clCC.Fields("customer") = rbCustomer.Checked
        clCC.Fields("Other") = rbOther.Checked
        clCC.Fields("Note") = txtNote.Text
        clCC.Fields("GrossCustomer") = txtGrossCustomer.Text
        clCC.Fields("grossadmin") = txtGrossAdmin.Text
        clCC.Fields("grossdealer") = txtGrossDealer.Text
        clCC.Fields("DealerCancel") = chkDealerCancel.Checked
        Dim clP As New VeritasGlobalTools.clsPayee
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        'clCC.Fields("CancelPaidAmt") = txtCustomerRefund.Text
        clCC.Fields("cancelfeeamt") = txtCancelFee.Text
        clCC.Fields("cancelinfo") = hfCancelInfo.Value
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = DateTime.Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = DateTime.Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("datepaid").Length > 0 Then
                clC.Fields("status") = "Cancelled"
            Else
                clC.Fields("status") = "Cancelled Before Paid"
            End If
            clC.SaveDB()
        End If
        hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value
        SaveToMoxy()
        ProcessContractAmt()
        CheckPaylink()
    End Sub

    Private Sub ProcessContractAmt()
        Dim SQL As String
        Dim clCA As New clsDBO
        Dim clCA2 As New clsDBO
        SQL = "select * from contractamt "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCA.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCA.RowCount > 0 Then
            For cnt = 0 To clCA.RowCount - 1
                clCA.GetRowNo(cnt)
                SQL = "select * from contractamt "
                SQL = SQL + "where contractamtid = " & clCA.Fields("contractamtid")
                clCA2.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                If clCA2.RowCount > 0 Then
                    clCA2.GetRow()
                    clCA2.Fields("cancelamt") = CDbl(clCA2.Fields("amt")) * CDbl(txtCancelPercent.Text.Replace("%", "")) / 100
                    clCA2.SaveDB()
                End If
            Next
        End If

    End Sub

    Private Sub rbOther_CheckedChanged(sender As Object, e As EventArgs) Handles rbOther.CheckedChanged
        If rbOther.Checked Then
            pnlOther.Visible = True
            GetLienholder()
        Else
            pnlOther.Visible = False
            txtCompanyName.Text = ""
        End If
    End Sub

    Private Sub GetLienholder()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtCompanyName.Text = clC.Fields("lienholder")
            txtLienholder.Text = clC.Fields("lienholder")
        End If
    End Sub

    Private Sub GetLienholder2()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtLienholder.Text = clC.Fields("lienholder")
        End If
    End Sub

    Private Sub btnUnable_Click(sender As Object, e As EventArgs) Handles btnUnable.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        CalcCancellation(True)
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = '" & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("cancelstatus") = "Unable To Process"
        clCC.Fields("termfactor") = dTermFactor
        clCC.Fields("claimamt") = dClaimAmt
        clCC.Fields("cancelfactor") = dCancelPer
        clCC.Fields("canceleffdate") = rdpProcessDate.SelectedDate
        clCC.Fields("Requestdate") = DateTime.Today
        clCC.Fields("adminamt") = dAdminAmt
        clCC.Fields("dealeramt") = dDealerAmt
        clCC.Fields("dealer") = rbDealer.Checked
        clCC.Fields("customer") = rbCustomer.Checked
        clCC.Fields("Other") = rbOther.Checked
        clCC.Fields("DealerCancel") = chkDealerCancel.Checked
        txtCancelStatus.Text = "Unable To Process"
        Dim clP As New VeritasGlobalTools.clsPayee
        clCC.Fields("Note") = txtNote.Text
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()


            clCC.Fields("payeeid") = clP.PayeeID
        End If
        'clCC.Fields("CancelPaidAmt") = dCancelAmt
        clCC.Fields("cancelfee") = dCancelFee
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = DateTime.Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = DateTime.Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
    End Sub

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
            clCC.Fields("cancelpaiddate") = txtPayDate.SelectedDate
            clCC.Fields("cancelpaidamt") = txtPaymentAmt.Text
            clCC.Fields("PaymentInfo") = txtPaymentInfo.Text
            clCC.Fields("paytypeID") = cboPayType.SelectedValue
            clCC.Fields("CancelStatus") = "Paid"
            clCC.SaveDB()
        End If
        SQL = "update veritasMoxy.dbo.moxycontractcancel "
        SQL = SQL + "set refundstatus = 'Refund Paid' "
        SQL = SQL + "from VeritasMoxy.dbo.MoxyContractCancel mcc "
        SQL = SQL + "inner join VeritasMoxy.dbo.MoxyContract mc "
        SQL = SQL + "on mc.DealID = mcc.DealID "
        SQL = SQL + "where contractno = '" & GetContractNo() & "'"
        clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Function GetContractNo() As String
        GetContractNo = ""
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select contractno from contract "
        SQL = SQL + "where contractno = '" & hfContractID.Value & "' "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            GetContractNo = clC.Fields("contractno")
        End If
    End Function

    Private Sub btnUpdateNote_Click(sender As Object, e As EventArgs) Handles btnUpdateNote.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
            clCC.Fields("note") = txtNote.Text
            clCC.SaveDB()
        End If
    End Sub

    Private Sub CheckPaylink()
        If txtLienholder.Text.ToLower = "mepco" Or txtLienholder.Text.ToLower = "paylink" Then
            pnlCalcCancellation.Visible = False
            pnlNote.Visible = False
            pnlOther.Visible = False
            pnlPayment.Visible = False
            pnlPaylink.Visible = True
            txtPaylink.Text = "The contract has a Lien Holder of Mepco or Paylink. Please supply Mepco or Paylink with required information to cancel contract. If you have any questions contact your supervisor."
        Else
            Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        pnlCalcCancellation.Visible = True
        pnlNote.Visible = True
        If rbOther.Checked Then
            pnlOther.Visible = True
        End If
        pnlPayment.Visible = True
        pnlPaylink.Visible = False
        Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    End Sub

    Private Sub rbDealer_CheckedChanged(sender As Object, e As EventArgs) Handles rbDealer.CheckedChanged
        If rbDealer.Checked Then
            pnlOther.Visible = False
        End If
    End Sub

    Private Sub rbCustomer_CheckedChanged(sender As Object, e As EventArgs) Handles rbCustomer.CheckedChanged
        If rbCustomer.Checked Then
            pnlOther.Visible = False
        End If
    End Sub

    Private Sub btnInvalid_Click(sender As Object, e As EventArgs) Handles btnInvalid.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        Dim clC As New clsDBO
        'CalcCancellation(True)
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        GetLienholder2()
        dTermFactor = 1
        dClaimAmt = 0
        dCancelPer = 1


        SQL = "select * From contract  "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("moxydealercost").Length > 0 Then
                dAdminAmt = clC.Fields("moxydealercost")
                dDealerAmt = CDbl(clC.Fields("customercost")) - clC.Fields("moxydealercost")
            Else
                dAdminAmt = 0
                dDealerAmt = 0
            End If
        Else
            dAdminAmt = 0
            dDealerAmt = 0
        End If
        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("cancelstatus") = "Invalid"
        clCC.Fields("termfactor") = dTermFactor
        clCC.Fields("claimamt") = dClaimAmt
        clCC.Fields("cancelfactor") = dCancelPer
        clCC.Fields("canceleffdate") = rdpProcessDate.SelectedDate
        clCC.Fields("canceldate") = DateTime.Today
        clCC.Fields("adminamt") = dAdminAmt
        clCC.Fields("dealeramt") = dDealerAmt
        clCC.Fields("dealer") = rbDealer.Checked
        clCC.Fields("customer") = rbDealer.Checked
        clCC.Fields("Other") = rbOther.Checked
        clCC.Fields("Note") = txtNote.Text
        clCC.Fields("DealerCancel") = chkDealerCancel.Checked
        Dim clP As New VeritasGlobalTools.clsPayee
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        'clCC.Fields("CancelPaidAmt") = dCancelAmt
        clCC.Fields("cancelfeeamt") = dCancelFee
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = DateTime.Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = DateTime.Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()

            clC.Fields("status") = "Invalid"
            clC.SaveDB()
        End If
        hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value
        CheckPaylink()
    End Sub

    Private Sub btnSaveCancel_Click(sender As Object, e As EventArgs) Handles btnSaveCancel.Click
        Dim clCC As New clsDBO
        Dim SQL As String
        SQL = "select * from contractcancel "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount = 0 Then
            clCC.NewRow()
        Else
            clCC.GetRow()
        End If
        GetLienholder2()
        clCC.Fields("contractid") = hfContractID.Value
        clCC.Fields("cancelstatus") = "Cancelled"
        clCC.Fields("termfactor") = CDbl(txtTermPercent.Text.Replace("%", "")) / 100
        clCC.Fields("claimamt") = txtClaimAmt.Text
        clCC.Fields("cancelfactor") = CDbl(txtCancelPercent.Text.Replace("%", "")) / 100
        clCC.Fields("canceleffdate") = rdpProcessDate.SelectedDate
        clCC.Fields("canceldate") = DateTime.Today
        clCC.Fields("adminamt") = txtFromAdmin.Text
        clCC.Fields("dealeramt") = txtFromDealer.Text
        clCC.Fields("dealer") = rbDealer.Checked
        clCC.Fields("customer") = rbCustomer.Checked
        clCC.Fields("Other") = rbOther.Checked
        clCC.Fields("DealerCancel") = chkDealerCancel.Checked
        clCC.Fields("Note") = txtNote.Text
        Dim clP As New VeritasGlobalTools.clsPayee
        If rbCustomer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddCustomerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbDealer.Checked = True Then
            clP.ContractID = hfContractID.Value
            clP.AddDealerToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        If rbOther.Checked = True Then
            clP.FName = txtFName.Text
            clP.CompanyName = txtCompanyName.Text
            clP.LName = txtLName.Text
            clP.Addr1 = txtAddr1.Text
            clP.Addr2 = txtAddr2.Text
            clP.City = txtCity.Text
            clP.State = cboState.Text
            clP.Zip = txtZip.Text
            clP.Phone = txtPhone.Text
            clP.AddOtherToPayee()
            clCC.Fields("payeeid") = clP.PayeeID
        End If
        'clCC.Fields("CancelPaidAmt") = txtCustomerRefund.Text
        clCC.Fields("cancelfeeamt") = txtCancelFee.Text
        If clCC.RowCount = 0 Then
            clCC.Fields("credate") = DateTime.Today
            clCC.Fields("creby") = hfUserID.Value
            clCC.AddRow()
        Else
            clCC.Fields("moddate") = DateTime.Today
            clCC.Fields("modby") = hfUserID.Value
        End If
        clCC.SaveDB()
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            clC.Fields("status") = "Cancelled"
            clC.SaveDB()
        End If
        hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" & hfContractID.Value

        SaveToMoxy()
        CheckPaylink()
    End Sub

    Private Sub SaveToMoxy()
        Dim clMD As New VeritasGlobalTools.clsMoxyDealID
        Dim clUD As New VeritasGlobalTools.clsGetUserInfo
        Dim clCC As New clsDBO
        Dim lDealID As Long
        Dim sDBType As String
        Dim SQL As String
        clMD.ContractID = hfContractID.Value
        clMD.GetMoxyDealID()
        lDealID = clMD.DealID
        sDBType = clMD.DBType
        If lDealID = 0 Then
            Exit Sub
        End If
        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            Exit Sub
        End If
        SQL = ""
        If sDBType = "EP" Then
            SQL = "delete epContractactivate "
            SQL = SQL + "where dealid = " & lDealID
            clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            SQL = "select * from veritasmoxy.dbo.epcontractcancel "
            SQL = SQL + "where dealid = " & lDealID
        End If
        If sDBType = "Normal" Then
            SQL = "delete MoxyContractactivate "
            SQL = SQL + "where dealid = " & lDealID
            clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            SQL = "select * from veritasmoxy.dbo.moxycontractcancel "
            SQL = SQL + "where dealid = " & lDealID
        End If
        If sDBType = "Product" Then
            SQL = "select * from veritasmoxy.dbo.productcontractcancel "
            SQL = SQL + "where dealid = " & lDealID
        End If
        clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clCC.RowCount > 0 Then
            clCC.GetRow()
        Else
            clCC.NewRow()
        End If
        clCC.Fields("dealid") = lDealID
        clCC.Fields("canceldate") = rdpProcessDate.SelectedDate
        clCC.Fields("cancelpercent") = CDbl(txtCancelPercent.Text.Replace("%", "")) / 100
        clCC.Fields("cancelstatus") = "Cancelled"
        clUD.UserID = hfUserID.Value
        clUD.GetUserInfo()
        clCC.Fields("cancelbyfname") = clUD.FName
        clCC.Fields("cancelbylname") = clUD.LName
        If clCC.RowCount > 0 Then
            clCC.Fields("moddate") = Now
        Else
            clCC.Fields("credate") = Now
            clCC.AddRow()
        End If
        clCC.SaveDB()
    End Sub

    Private Sub btnActivate_Click(sender As Object, e As EventArgs) Handles btnActivate.Click
        Dim SQL As String
        Dim clCC As New clsDBO
        Dim clR As New clsDBO
        Dim lDealID As Long
        Dim sDBType As String
        Dim clMD As New VeritasGlobalTools.clsMoxyDealID
        SQL = "select status, effdate, expdate from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("status") = "Cancelled" Then
                SQL = "update contract "
                SQL = SQL + "set status = 'Paid' "
                SQL = SQL + "where contractid = " & hfContractID.Value
                clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                SQL = "update contractcancel "
                SQL = SQL + "set cancelstatus = 'Quote', "
                SQL = SQL + "canceldate = null "
                SQL = SQL + "where contractid = " & hfContractID.Value
                clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                SQL = "select * from contract "
                SQL = SQL + "where contractid = " & hfContractID.Value
                clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                If clCC.RowCount > 0 Then
                    clCC.GetRow()
                    If clCC.Fields("moxydealid").Length > 0 Then
                        lDealID = clCC.Fields("moxydealid")
                    Else
                        lDealID = 0
                    End If
                Else
                    lDealID = 0
                End If
                clMD.ContractID = hfContractID.Value
                clMD.GetMoxyDealID()
                sDBType = clMD.DBType
                If sDBType = "Normal" Then
                    SQL = SQL + "delete veritasmoxy.dbo.moxycontractcancel "
                    SQL = SQL + "where dealid = " & lDealID
                    clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                    SQL = "insert into veritasmoxy.dbo.MoxyContractActivate "
                    SQL = SQL + "(dealid, activatedate, activateby) "
                    SQL = SQL + "values (" & lDealID & ",'" & DateTime.Today & "','" & GetUserInfo(hfUserID.Value) & "') "
                    clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                End If
                If sDBType = "EP" Then
                    SQL = SQL + "delete veritasmoxy.dbo.epcontractcancel "
                    SQL = SQL + "where dealid = " & lDealID
                    clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                    SQL = "insert into veritasmoxy.dbo.epContractActivate "
                    SQL = SQL + "(dealid, activatedate, activateby) "
                    SQL = SQL + "values (" & lDealID & ",'" & DateTime.Today & "','" & GetUserInfo(hfUserID.Value) & "') "
                    clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                End If

            End If
                If clR.Fields("status") = "Cancelled Before Paid" Then
                SQL = "update contract "
                SQL = SQL + "set status = 'Pending' "
                SQL = SQL + "where contractid = " & hfContractID.Value
                clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            End If
            If clR.Fields("status") = "Expired" Then
                If CDate(clR.Fields("effdate")) < DateTime.Today Then
                    If CDate(clR.Fields("expdate")) > DateTime.Today Then
                        SQL = "update contract "
                        SQL = SQL + "set status = 'Paid' "
                        SQL = SQL + "where contractid = " & hfContractID.Value
                        clCC.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
                    End If
                End If
            End If
        End If

        Response.Redirect("contract.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    End Sub

    Private Sub btnMoxy_Click(sender As Object, e As EventArgs) Handles btnMoxy.Click
        Response.Redirect("~/contract/MoxyCancellation.aspx?sid=" & hfID.Value & "&contractid=" & hfContractID.Value)
    End Sub

    Private Function CheckClaim() As Boolean
        CheckClaim = False
        Dim SQL As String
        Dim clR As New clsDBO
        If rdpProcessDate.SelectedDate Is Nothing Then
            Exit Function
        End If
        SQL = "select * from claim "
        SQL = SQL + "where contractid = " & hfContractID.Value & " "
        SQL = SQL + "and credate > '" & rdpProcessDate.SelectedDate & "' "
        SQL = SQL + "and (status = 'Open' "
        SQL = SQL + "or status = 'Paid') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckClaim = True
        End If
    End Function
End Class