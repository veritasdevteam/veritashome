﻿Imports System.Configuration.ConfigurationManager

Public Class Cancellation
    Inherits System.Web.UI.Page
    Private lContractID As Long
    Private bOOB As Boolean
    Private bRegulatedState As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim relativePath = "~/api/export/file"
        RadClientExportManager1.PdfSettings.Fonts.Add("monospace", "")
        'the font is added to export the code viewer properly
        RadClientExportManager1.SvgSettings.ProxyURL = ResolveUrl(relativePath)
        lContractID = Request.QueryString("contractID")
        FillContractCancellation()
    End Sub

    Private Sub FillContractCancellation()
        Dim SQL As String
        Dim clCC As New clsDBO
        Dim clC As New clsDBO
        Dim clD As New clsDBO
        Dim clP As New clsDBO
        Dim lDaysElapse As Long
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            SQL = "select * from contractcancel "
            SQL = SQL + "where contractid = " & lContractID
            clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clCC.RowCount > 0 Then
                clCC.GetRow()
                lblContractNo.Text = clC.Fields("contractno")
                lblVIN.Text = clC.Fields("vin")
                If clC.Fields("contractno").Substring(0, 3) = "CHJ" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "DRV" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RAC" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RAD" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RAN" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RDI" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "REP" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RSA" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RSD" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "RSW" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "SAG" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "VEL" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                ElseIf clC.Fields("Contractno").Substring(0, 3) = "VIS" Then
                    Image1.ImageUrl = "~/images/RSAdmin.png"
                    pnlHeader.BackColor = System.Drawing.Color.White
                End If


                lblCustomer.Text = clC.Fields("fname") & " " & clC.Fields("lname")
                lblSaleDate.Text = Format(CDate(clC.Fields("saledate")), "M/d/yyyy")
                If clCC.Fields("canceleffdate").Length > 0 Then
                    lblEffDate.Text = Format(CDate(clCC.Fields("canceleffdate")), "M/d/yyyy")
                End If
                lblEffMile.Text = Format(CLng(clC.Fields("effmile")), "#,###")
                lblCancelMile.Text = Format(CLng(clCC.Fields("cancelmile")), "#,###")
                lblTotalDays.Text = Format(CDate(clC.Fields("expdate")), "M/d/yyyy")
                lblTotalMiles.Text = clC.Fields("expmile")
                lblDealerCostBase.Text = Format(CDbl(clC.Fields("moxydealercost")), "#,##0.00")
                lblCustomerCost.Text = Format(CDbl(clC.Fields("customercost")), "#,##0.00")
                lDaysElapse = DateDiff(DateInterval.Day, CDate(Format(CDate(clC.Fields("saledate")), "M/d/yyyy")), CDate(clCC.Fields("canceleffdate")))
                lblDaysElapse.Text = Format(lDaysElapse, "#,##0")
                lblMilesElapse.Text = Format(CLng(clCC.Fields("cancelmile")) - CLng(clC.Fields("effmile")), "#,##0")
                lblCancelFee.Text = Format(CDbl(clCC.Fields("cancelfeeamt")), "currency")
                lblClaims.Text = Format(CDbl(clCC.Fields("claimamt")), "currency")
                GetDealerInfo(clC.Fields("dealerid"))
                lblLienholder.Text = clC.Fields("lienholder")
                If clCC.Fields("payeeid").Length > 0 Then
                    GetPayee(clCC.Fields("payeeid"))
                End If
                lblMilePer.Text = Format(CDbl(clCC.Fields("milefactor")), "0.###%")
                lblTimePer.Text = Format(CDbl(clCC.Fields("termfactor")), "0.###%")
                lblRefundPer.Text = Format(CDbl(clCC.Fields("cancelfactor")), "0.###%")
                lblDealerCost.Text = Format(CDbl(clCC.Fields("dealeramt")), "currency")
                CheckRegulatedState(clC.Fields("state"))
                Dim dCustRefund As Double
                If bOOB Then
                    If bRegulatedState Then
                        If CDbl(clCC.Fields("dealeramt")) > CDbl(clCC.Fields("adminamt")) Then
                            lblAdminCost.Text = Format(CDbl(clCC.Fields("adminamt")), "currency")
                            dCustRefund = CDbl(clCC.Fields("dealeramt")) + CDbl(clCC.Fields("adminamt"))
                        Else
                            lblAdminCost.Text = Format(CDbl(clCC.Fields("adminamt")) - CDbl(clCC.Fields("dealeramt")), "currency")
                            dCustRefund = CDbl(clCC.Fields("adminamt"))
                        End If
                    Else
                        dCustRefund = CDbl(clCC.Fields("dealeramt")) + CDbl(clCC.Fields("adminamt"))
                        lblAdminCost.Text = Format(CDbl(clCC.Fields("adminamt")), "currency")
                    End If
                Else
                    dCustRefund = CDbl(clCC.Fields("dealeramt")) + CDbl(clCC.Fields("adminamt"))
                    lblAdminCost.Text = Format(CDbl(clCC.Fields("adminamt")), "currency")
                End If
                If bRegulatedState Then
                    lblRegulated.Text = "Yes"
                Else
                    lblRegulated.Text = "No"
                End If
                lblCustomerRefund.Text = Format(dCustRefund, "currency")
                lblCancelInfo.Text = clCC.Fields("cancelinfo")
                If clC.Fields("status") = "Cancelled" Then
                    rCancel.Visible = True
                    rCancel2.Visible = True
                    rCancel3.Visible = True
                    rQuote.Visible = False
                Else
                    rCancel.Visible = False
                    rCancel.Visible = False
                    rCancel.Visible = False
                    rQuote.Visible = True
                End If
            End If
        End If

    End Sub

    Private Sub CheckRegulatedState(xState As String)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from cancelstaterules "
        SQL = SQL + "where state = '" & xState & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If CBool(clR.Fields("regulated")) Then
                bRegulatedState = True
            End If
        End If
    End Sub

    Private Sub GetPayee(xPayeeID As Long)
        Dim clP As New clsDBO
        Dim SQL As String
        SQL = "select * from payee "
        SQL = SQL + "where payeeid = " & xPayeeID
        clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            If clP.Fields("companyname").Length > 0 Then
                lblPayee.Text = clP.Fields("companyname")
            Else
                lblPayee.Text = clP.Fields("fname") & " " & clP.Fields("lname")
            End If
            lblAddr1.Text = clP.Fields("addr1")
            If clP.Fields("addr2").Length > 0 Then
                lblAddr2.Text = clP.Fields("addr2")
                lblAddr3.Text = clP.Fields("city") & " " & clP.Fields("state") & " " & clP.Fields("zip")
            Else
                lblAddr2.Text = clP.Fields("city") & " " & clP.Fields("state") & " " & clP.Fields("zip")
            End If

        End If
    End Sub

    Private Sub GetDealerInfo(xDealerID As Long)
        Dim SQL As String
        Dim clD As New clsDBO
        SQL = "select * from dealer "
        SQL = SQL + "where dealerid = " & xDealerID
        clD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clD.RowCount > 0 Then
            clD.GetRow()
            lblDealer.Text = clD.Fields("dealername")
            If clD.Fields("dealerstatusid") = 5 Then
                bOOB = True
            End If
        End If
    End Sub


End Class