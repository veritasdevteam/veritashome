﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContractPayment.ascx.vb" Inherits="VeritasGlobal.ContractPayment" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="pnlPaymentList" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAddPayment" runat="server" Text="Add Payment" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgContractPayment" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                    AllowSorting="true" AllowPaging="true"  Width="1500" ShowFooter="true">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractPaymentID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractPaymentID"  ReadOnly="true" Visible="false" UniqueName="ContractPaymentID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Company" UniqueName="Company" HeaderText="Company"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaymentType" UniqueName="PaymentType" HeaderText="Payment Type"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CheckNo" UniqueName="CheckNo" HeaderText="Check No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlPayment" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payee: 
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPayee" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnPayeeSearch" runat="server" Text="Payee Search" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payment Type:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadComboBox ID="cboPaymentType" DataSourceID="SqlDataSource2" DataValueField="paymenttype" DataTextField="paymenttype" runat="server"></telerik:RadComboBox>
                <asp:SqlDataSource ID="SqlDataSource2"
                ProviderName="System.Data.SqlClient" SelectCommand="select paymenttype from contractpaymenttype" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Date Paid:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadDatePicker ID="rdpProcessDate" runat="server"></telerik:RadDatePicker>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Check No:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtCheckNo" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Paid Amt:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtpaidAmt" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnCancelPayment" runat="server" Text="Cancel" BackColor="#1eabe2" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnSavePayment" runat="server" Text="Save" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlPayeeSearch" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnCancelSearch" runat="server" Text="Cancel Search" BackColor="#1eabe2" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgPayee" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="DealerID" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="PayeeID"  ReadOnly="true" Visible="false" UniqueName="PayeeID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Company" FilterCheckListWebServiceMethod="LoadCompany" UniqueName="Company" HeaderText="Company" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DealerName" FilterCheckListWebServiceMethod="LoadDealerName" UniqueName="DealerName" HeaderText="Seller Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="DealerCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="AgentState" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AgentName" FilterCheckListWebServiceMethod="LoadAgentsName" UniqueName="AgentName" HeaderText="Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SubAgentName" FilterCheckListWebServiceMethod="LoadSubAgentName" UniqueName="SubAgentName" HeaderText="Sub Agent Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" SelectCommand="select dealerid, dealerno, dealername, d.city, d.state, a.agentname, 
                ay.subagentname from dealer d 
                left join agents a on d.agentsid = a.agentid 
                left join subagents ay on d.subagentid = ay.subAgentID" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow> 
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlPayeeModify" runat="server">

</asp:Panel>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfPayeeID" runat="server" />
<asp:HiddenField ID="hfContractPaymentID" runat="server" />


