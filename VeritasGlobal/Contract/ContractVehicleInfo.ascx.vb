﻿Imports System.Security.Cryptography

Public Class ContractVehicleInfo
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlModify.Visible = False
            pnlViewer.Visible = True
            hfContractID.Value = Request.QueryString("contractid")
            GetServerInfo()
            FillVehicleInfo()
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If Not CBool(clR.Fields("contractmodification")) Then
                btnEdit.Enabled = False
            End If
        End If
    End Sub
    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillVehicleInfo()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            lblVIN.Text = clC.Fields("vin")
            lblClass.Text = clC.Fields("class")
            lblYear.Text = clC.Fields("year")
            lblMake.Text = clC.Fields("make")
            lblModel.Text = clC.Fields("model")
            lblTrim.Text = clC.Fields("Trim")
            chkAWD.Checked = clC.Fields("AWD")
            chkTurbo.Checked = clC.Fields("Turbo")
            chkDiesel.Checked = clC.Fields("Diesel")
            chkHybrid.Checked = clC.Fields("Hybrid")
            chkCommercial.Checked = clC.Fields("Commercial")
            chkHydraulic.Checked = clC.Fields("Hydraulic")
            chkAirBladder.Checked = clC.Fields("AirBladder")
            chkLiftKit.Checked = clC.Fields("LiftKit")
            chkLuxury.Checked = clC.Fields("Luxury")
            chkSeals.Checked = clC.Fields("Seals")
            chkSnowPlow.Checked = clC.Fields("SnowPlow")
            chkLargerLiftKit.Checked = clC.Fields("LargerLiftKit")
            chkRideShare.Checked = clC.Fields("rideshare")

            txtVINMod.Text = clC.Fields("vin")
            txtClassMod.Text = clC.Fields("class")
            txtYearMod.Text = clC.Fields("year")
            txtMakeMod.Text = clC.Fields("make")
            txtModelMod.Text = clC.Fields("model")
            txtTrimMod.Text = clC.Fields("Trim")
            chkAwdMod.Checked = clC.Fields("AWD")
            chkTurboMod.Checked = clC.Fields("Turbo")
            chkDieselMod.Checked = clC.Fields("Diesel")
            chkHybridMod.Checked = clC.Fields("Hybrid")
            chkCommercialMod.Checked = clC.Fields("Commercial")
            chkHydraulicMod.Checked = clC.Fields("Hydraulic")
            chkAirBladderMod.Checked = clC.Fields("AirBladder")
            chkLiftKitMod.Checked = clC.Fields("LiftKit")
            chkLuxuryMod.Checked = clC.Fields("Luxury")
            chkSealsMod.Checked = clC.Fields("Seals")
            chkSnowPlowMod.Checked = clC.Fields("SnowPlow")
            chkLargerLiftKitMod.Checked = clC.Fields("LargerLiftKit")
            chkRideShareMod.Checked = clC.Fields("rideshare")

        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        pnlModify.Visible = True
        pnlViewer.Visible = False
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlModify.Visible = False
        pnlViewer.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        CheckChange()
        SaveContract()
        FillVehicleInfo()
        pnlModify.Visible = False
        pnlViewer.Visible = True
    End Sub

    Private Sub SaveContract()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            clC.Fields("vin") = txtVINMod.Text
            clC.Fields("class") = txtClassMod.Text
            clC.Fields("year") = txtYearMod.Text
            clC.Fields("make") = txtMakeMod.Text
            clC.Fields("model") = txtModelMod.Text
            clC.Fields("Trim") = txtTrimMod.Text
            clC.Fields("AWD") = chkAwdMod.Checked
            clC.Fields("Turbo") = chkTurboMod.Checked
            clC.Fields("Diesel") = chkDieselMod.Checked
            clC.Fields("Hybrid") = chkHybridMod.Checked
            clC.Fields("Commercial") = chkCommercialMod.Checked
            clC.Fields("Hydraulic") = chkHydraulicMod.Checked
            clC.Fields("AirBladder") = chkAirBladderMod.Checked
            clC.Fields("LiftKit") = chkLiftKitMod.Checked
            clC.Fields("Luxury") = chkLuxuryMod.Checked
            clC.Fields("Seals") = chkSealsMod.Checked
            clC.Fields("SnowPlow") = chkSnowPlowMod.Checked
            clC.Fields("LargerLiftKit") = chkLargerLiftKitMod.Checked
            clC.Fields("modby") = hfUserID.Value
            clC.Fields("moddate") = Today
            clC.Fields("rideshare") = chkRideShareMod.Checked
            clC.SaveDB()
        End If

    End Sub

    Private Sub CheckChange()
        Dim clC As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            If txtVINMod.Text <> clC.Fields("vin") Then
                UpdateHistory("VIN", clC.Fields("VIN"), txtVINMod.Text)
            End If
            If txtClassMod.Text <> clC.Fields("class") Then
                UpdateHistory("Class", clC.Fields("class"), txtClassMod.Text)
            End If
            If txtYearMod.Text <> clC.Fields("year") Then
                UpdateHistory("Year", clC.Fields("year"), txtYearMod.Text)
            End If
            If txtMakeMod.Text <> clC.Fields("Make") Then
                UpdateHistory("Make", clC.Fields("make"), txtMakeMod.Text)
            End If
            If txtModelMod.Text <> clC.Fields("model") Then
                UpdateHistory("Model", clC.Fields("model"), txtModelMod.Text)
            End If
            If txtTrimMod.Text <> clC.Fields("trim") Then
                UpdateHistory("Trim", clC.Fields("trim"), txtTrimMod.Text)
            End If
            If chkAwdMod.Checked <> CBool(clC.Fields("awd")) Then
                UpdateHistory("AWD", CBool(clC.Fields("awd")), chkAwdMod.Checked)
            End If
            If chkTurboMod.Checked <> CBool(clC.Fields("turbo")) Then
                UpdateHistory("Turbo", CBool(clC.Fields("turbo")), chkTurbo.Checked)
            End If
            If chkDieselMod.Checked <> CBool(clC.Fields("diesel")) Then
                UpdateHistory("Diesel", CBool(clC.Fields("diesel")), chkDieselMod.Checked)
            End If
            If chkHybridMod.Checked <> CBool(clC.Fields("hybrid")) Then
                UpdateHistory("Hybrid", CBool(clC.Fields("hybrid")), chkHybridMod.Checked)
            End If
            If chkCommercialMod.Checked <> CBool(clC.Fields("commercial")) Then
                UpdateHistory("Commercial", CBool(clC.Fields("commercial")), chkCommercial.Checked)
            End If
            If chkHydraulicMod.Checked <> CBool(clC.Fields("hydraulic")) Then
                UpdateHistory("Hydraulic", CBool(clC.Fields("hydraulic")), chkHydraulicMod.Checked)
            End If
            If chkAirBladderMod.Checked <> CBool(clC.Fields("airbladder")) Then
                UpdateHistory("Air Bladder", CBool(clC.Fields("airbaldder")), chkAirBladderMod.Checked)
            End If
            If chkLiftKitMod.Checked <> CBool(clC.Fields("LiftKit")) Then
                UpdateHistory("Lift Kit", CBool(clC.Fields("liftkit")), chkLiftKitMod.Checked)
            End If
            If chkLuxuryMod.Checked <> CBool(clC.Fields("luxury")) Then
                UpdateHistory("Luxury", CBool(clC.Fields("luxury")), chkLuxuryMod.Checked)
            End If
            If chkSealsMod.Checked <> CBool(clC.Fields("seals")) Then
                UpdateHistory("Seals", CBool(clC.Fields("seals")), chkSealsMod.Checked)
            End If
            If chkSnowPlowMod.Checked <> CBool(clC.Fields("snowplow")) Then
                UpdateHistory("Snow Plow", CBool(clC.Fields("snowplow")), chkSnowPlowMod.Checked)
            End If
            If chkLargerLiftKitMod.Checked <> CBool(clC.Fields("largerliftkit")) Then
                UpdateHistory("Larger Lift Kit", CBool(clC.Fields("largerliftkit")), chkLargerLiftKitMod.Checked)
            End If
        End If
    End Sub

    Private Sub UpdateHistory(xFieldName As String, xOldValue As String, xNewValue As String)
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select * from contracthistory "
        SQL = SQL + "where contracthistoryid =  0 "
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        clC.NewRow()
        clC.Fields("contractid") = hfContractID.Value
        clC.Fields("fieldname") = xFieldName
        clC.Fields("OldValue") = xOldValue
        clC.Fields("newvalue") = xNewValue
        clC.Fields("creby") = hfUserID.Value
        clC.Fields("credate") = Today
        clC.AddRow()
        clC.SaveDB()
    End Sub
End Class