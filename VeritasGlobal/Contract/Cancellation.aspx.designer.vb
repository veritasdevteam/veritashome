﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Cancellation

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''pnlOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOut As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlHeader As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Image1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''lblContractNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContractNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDealer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDealer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblVIN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVIN As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLienholder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLienholder As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPayee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPayee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSaleDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSaleDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddr1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddr1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDealerCostBase control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDealerCostBase As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddr2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddr2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCustomerCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomerCost As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddr3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddr3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEffDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEffDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEffMile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEffMile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCancelMile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCancelMile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMilePer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMilePer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTimePer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTimePer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalDays As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRefundPer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefundPer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalMiles control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalMiles As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDaysElapse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDaysElapse As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegulated control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegulated As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMilesElapse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMilesElapse As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDealerCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDealerCost As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAdminCost control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAdminCost As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClaims control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClaims As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCustomerRefund control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomerRefund As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCancelFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCancelFee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCancelInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCancelInfo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rQuote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rQuote As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''rCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rCancel As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''rCancel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rCancel2 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''rCancel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rCancel3 As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''RadClientExportManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RadClientExportManager1 As Global.Telerik.Web.UI.RadClientExportManager
End Class
